'use strict';

angular.module('app')
	.controller('listController',  ['$scope', '$location', '$rootScope', 'commonService', '$cookieStore', 'menuService', '$route', 'listService', 'ENV', 'NgTableParams', function ($scope, $location, $rootScope, commonService, $cookieStore, menuService, $route, listService, ENV, NgTableParams){

        var accessToken = $cookieStore.get("access_token");
        var partnerName = $cookieStore.get("partner_name");
        var filter = $route.current.$$route.filter;
        
        $scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
        };
        
    $scope.offersList = function(){
            var queryParams = {
                type : 'all'
            }
            
        var api = ENV.apiEndpoint;
        return listService.offersList(api, accessToken, queryParams)
        .then(function (data) {
          /* $scope.list = data;
          var offerlist = {};
          offerlist = data; */
          $scope.offerTableParams = new NgTableParams({}, {dataset: data});
        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
        };
        

        console.log($location.path());
        $scope.viewFile = $route.current.$$route.pageName;
        $scope.location = $location.path(); 
        $scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
        $scope.getMenu();
        $scope.offersList();
        
}]);