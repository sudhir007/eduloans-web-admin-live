'use strict';

angular.module('app')
	.controller('dashboardController', ['$scope', 'dashboardService', 'studentService', 'ENV', '$cookieStore', '$route', 'menuService', '$routeParams', 'NgTableParams', '$rootScope', '$location', function ($scope, dashboardService, studentService, ENV, $cookieStore, $route, menuService, $routeParams, NgTableParams, $rootScope, $location){
		var accessToken = $cookieStore.get("access_token");
		var partnerName = $cookieStore.get("partner_name");
		var filter = $route.current.$$route.filter;
		$scope.intakeYear = $routeParams.year;

		$scope.lead_rm_id = [];
		$scope.lead_state_id = [];
		$scope.leadStateSetting = { displayProp: 'status', idProperty:'id' };
		$scope.rmSetting = { displayProp: 'email', idProperty:'id' };

		$scope.getMenu = function(){
			var api = ENV.apiEndpoint + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
				//window.location.reload(true);
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.dashboardData = [];
		var dashboardBlocks = {};

		$scope.getDashboardData = function(){
			var api = ENV.apiEndpoint + '/dashboard';

			return dashboardService.getData(api, accessToken)
			.then(function(data){
				for(let i = 0; i < data.length; i++){
					if(data[i].current_lead_state){
						dashboardBlocks = data[i];
						switch(data[i].current_lead_state){
							case 'REFERRED':
								dashboardBlocks.display_name = 'Referred';
								break;
							case 'INTERESTED':
								dashboardBlocks.display_name = 'Interested';
								break;
							case 'POTENTIAL':
								dashboardBlocks.display_name = 'Potential';
								break;
							case 'INITIAL_OFFER':
								dashboardBlocks.display_name = 'Initial Offer';
								break;
							case 'APPLICATION_PROCESS':
								dashboardBlocks.display_name = 'Application Process';
								break;
							case 'APPLICATION_REVIEW':
								dashboardBlocks.display_name = 'Applicatio Review';
								break;
							case 'SANCTIONED':
								dashboardBlocks.display_name = 'Sanctioned';
								break;
							case 'PENDING_DOCUMENT':
								dashboardBlocks.display_name = 'Pending Document';
								break;
							case 'SUBMITTED_TO_BANK':
								dashboardBlocks.display_name = 'Submitted To Bank';
								break;
							case 'DISBURSED':
								dashboardBlocks.display_name = 'Disbursed';
								break;
							case 'DISBURSAL_DOCUMENTATION':
								dashboardBlocks.display_name = 'Disbursal Documentation';
								break;
							case 'PROVISIONAL_OFFER':
								dashboardBlocks.display_name = 'Provisional Offers';
								break;
							case 'POTENTIAL_DECLINED':
								dashboardBlocks.display_name = 'Potential Declined';
								break;
							case 'APPLICATION_REVIEW_DECLINED':
								dashboardBlocks.display_name = 'Application Review Declined';
								break;
							case 'REJECTED':
								dashboardBlocks.display_name = 'Rejected';
								break;
						}
						$scope.dashboardData.push(dashboardBlocks)
					}
				}
				/*$scope.dashboardData = data;
				$scope.dashboardData = [
					{
						"display_name": "Registered Students",
						"lead_count": 500,
						"current_lead_state": "registered_students"
					},
					{
						"display_name": "BOB Applications",
						"lead_count": 500,
						"current_lead_state": "bob_applications"
					},
					{
						"display_name": "SBI Applications",
						"lead_count": 500,
						"current_lead_state": "sbi_applications"
					},
					{
						"display_name": "Other Banks Applications",
						"lead_count": 500,
						"current_lead_state": "other_bank_applications"
					},
					{
						"display_name": "Referred",
						"lead_count": 500,
						"current_lead_state": "referred"
					},
					{
						"display_name": "Interested",
						"lead_count": 500,
						"current_lead_state": "interested"
					},
					{
						"display_name": "Potentials",
						"lead_count": 500,
						"current_lead_state": "potentials"
					},
					{
						"display_name": "Application process",
						"lead_count": 500,
						"current_lead_state": "application_process"
					},
					{
						"display_name": "Application Review",
						"lead_count": 500,
						"current_lead_state": "application_review"
					},
					{
						"display_name": "Providing Document",
						"lead_count": 500,
						"current_lead_state": "providing_document"
					},
					{
						"display_name": "Provisional Offers",
						"lead_count": 500,
						"current_lead_state": "provisional_offers"
					},
					{
						"display_name": "Disbursement",
						"lead_count": 500,
						"current_lead_state": "disbursement"
					},
					{
						"display_name": "Declined",
						"lead_count": 500,
						"current_lead_state": "declined"
					}
				];*/

				var headerWidth = "4%";
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.getDashboardCount = function(){
			var api = ENV.apiEndpoint + '/dashboard';

			return dashboardService.getData(api, accessToken)
			.then(function(data){
				//$scope.dashboardData = data;
				var dataset = [];
                data.forEach(function(tableData, key){
					var params = {};
					params.application_id = key + 1;
					params.name = "Nikhil";
                    params.email = "nikhil@issc.in";
                    params.mobile = "9012345678";
                    params.university = "MIT";
                    params.course = "AI";
                    params.degree = "MS";
                    params.status = "Potential";
                    params.bank = "BOB";
                    params.loan_type = "Collateral";
                    params.loan_amount = "5000000";
                    params.comments = "Comment-1, Comment-2, Comment-3";
					dataset.push(params);
				});
                data.forEach(function(tableData, key){
					var params = {};
					params.application_id = key + 10;
					params.name = "Gupta";
                    params.email = "gupta@issc.in";
                    params.mobile = "9123456780";
                    params.university = "US University";
                    params.course = "CS";
                    params.degree = "Master";
                    params.status = "Application Review";
                    params.bank = "SBI";
                    params.loan_type = "Non Collateral";
                    params.loan_amount = "6000000";
                    params.comments = "Comment-4, Comment-5, Comment-5";
					dataset.push(params);
				});
                $scope.tableParams = new NgTableParams({}, { dataset: dataset});
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};


		$scope.dashboardList = function(){

    		var api = ENV.apiEndpoint + '/dashboardlist';

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListCompare = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListIntakeWise = function(intake_year){

    		var api = ENV.apiEndpoint + '/dashboardlistintakewise/' + intake_year;

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListCompare = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.newdashboardListIntakeWise = function(intake_year){

    		var api = ENV.apiEndpoint + '/dashboardlistintakewisenew/' + intake_year;

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.newdashboardListCompare = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListBankOverall = function(){

    		var api = ENV.apiEndpoint + '/dashboardlistbankoverall';

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankCompare = data;
					//alert("hi");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListBankOverallIntakeWise = function(intakeYearbank){

    		var api = ENV.apiEndpoint + '/dashboardlistbankoverallintakewisenew/'+ intakeYearbank;

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankCompare = data;
					//alert("hi");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};



		$scope.dashboardListBankOverallStudent = function(){

    		var api = ENV.apiEndpoint + '/dashboardlistbankoverallstudent';

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankStudentCompare = data;
					//alert("hi");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListBankTeam = function(){

    		var api = ENV.apiEndpoint + '/dashboardlistbankteam';

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankTeamCompare = data;
					//alert("hi");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListBankTeamIntakeWise = function(intakeYearbank){

    		var api = ENV.apiEndpoint + '/dashboardlistbankteamintakewisenew/'+intakeYearbank;

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankTeamCompare = data;
					//alert("hi");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListBankTeamStudent = function(){

    		var api = ENV.apiEndpoint + '/dashboardlistbankteamstudent';

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankTeamStudentCompare = data;
					//alert("hi");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListBankTeamIndividual = function(){

    		var api = ENV.apiEndpoint + '/dashboardindividuallistbank';

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankTeamIndividualCompare = data;
					// alert("hill");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListBankTeamIndividualIntakeWise = function(intakeYearbank){

    		var api = ENV.apiEndpoint + '/dashboardindividuallistbankintakewisenew/'+intakeYearbank;

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankTeamIndividualCompare = data;
					// alert("hill");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListBankTeamIndividualStudent = function(){



    		var api = ENV.apiEndpoint + '/dashboardindividuallistbankstudent';

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListBankTeamIndividualStudentCompare = data;
					// alert("hill");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListTeam = function(){

    		var api = ENV.apiEndpoint + '/dashboardlistteam';

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListTeamCompare = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListTeamIntakeWise = function(intake_year){

    		var api = ENV.apiEndpoint + '/dashboardlistteamintakewise/' + intake_year;

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListTeamCompare = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.newdashboardListTeamIntakeWise = function(intake_year){

    		var api = ENV.apiEndpoint + '/dashboardlistteamintakewisenew/' + intake_year;

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.newdashboardListTeamCompare = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardListImperialBranchOverall = function(intake_year){

    		var api = ENV.apiEndpoint + '/imperial/branchreports/'+intake_year;

    		return dashboardService.getDashboardData(api, accessToken)
    		.then(function(data){

    			$scope.dashboardListImperialBranch = data;
					//alert("hi");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.imperialOverAllReport = function(){


				var postData = {
					"start_date" : $('#lead_start_date').val(),
					"intake_year" : $scope.intake_year_filter,
					"end_date" : $('#lead_end_date').val()
				};

    		var api = ENV.apiEndpoint + '/imperial/filter/reports';

    		return dashboardService.getDataByPost(api, postData, accessToken)
    		.then(function(data){

    			$scope.dashboardListImperialBranch = data.data;
					//alert("hi");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.dashboardAllStudentList = function(){

				var intake_year = $scope.intake_year_filter ? ENV.apiEndpoint + '/smm/getallStudent/' + $scope.intake_year_filter : ENV.apiEndpoint + '/smm/getallStudent/';
				//alert(intake_year);
    		//var api = ENV.apiEndpoint + '/smm/getallStudent';

    		return dashboardService.getDashboardData(intake_year, accessToken)
    		.then(function(data){

    			$scope.dashboardAllStudentListData = data;
    			var dataset = data;
				$scope.AllStudentListTableParams = new NgTableParams({}, {
					counts: [10, 50, 100, 1000, 5000],
					dataset: dataset
				});
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.sendSmmMail = function (appValue) {
			var api = ENV.apiEndpoint + '/smm/connectmail/' + appValue.id;
			return studentService.getData(api, accessToken)
				.then(function (data) {
					alert("Connect Mail Sent ");
					////console.log(data);
				})
				.catch(function (error) {
					alert(error.message);
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.smmUpdate = function (event,loginId,leadId,type) {

			var valCheck = "NO";

			if(event.target.checked){
				valCheck = "YES";
			}

			if(type == "SMMCONNECTMAIL"){
				valCheck = "YES";
			}

			var postData = {
				"type" : type,
				"value" : valCheck,
				"lead_id" : leadId,
				"login_id" : loginId,
			};
			var api = ENV.apiEndpoint + '/smm/smmupdate';

			return studentService.postData(accessToken, api, postData)
				.then(function (data) {
					alert("Success");
					////console.log(data);
				})
				.catch(function (error) {
					alert(error.message);
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.imperialDataModal = function (state, type, counselorId) {

			var intake_year = $routeParams.intakeYear;

			var postData = {
				"student_type" : type,
				"counselor_id" : counselorId,
				"lead_state" : state,
				"intake_year" : intake_year
			};

			var api = ENV.apiEndpoint + '/imperial/branchreportsdata';
			return studentService.postData(accessToken , api, postData)
				.then(function (data) {
					// alert("hii");
					////console.log(data);
					var dataset = data;
					$scope.tableParamsImperialData = new NgTableParams({}, {dataset: dataset});

				})
				.catch(function (error) {
					alert(error.message);
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.imperialFilterDataModal = function (state, type, counselorId) {

			var intake_year = $routeParams.intakeYear;

			var postData = {
				"student_type" : type,
				"counselor_id" : counselorId,
				"lead_state" : state,
				"intake_year" : $scope.intake_year_filter,
				"start_date" : $('#lead_start_date').val(),
				"end_date" : $('#lead_end_date').val()
			};

			var api = ENV.apiEndpoint + '/imperial/filter/branchreportsdata';
			return studentService.postData(accessToken , api, postData)
				.then(function (data) {
					// alert("hii");
					////console.log(data);
					var dataset = data;
					$scope.tableParamsImperialData = new NgTableParams({}, {
						counts: [10, 50, 100, 500, 1000],
						dataset: dataset
					});

				})
				.catch(function (error) {
					alert(error.message);
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.dashboardlistteamdata = function(partnerId,teamChecked,leadState){

			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
					"lead_state" : leadState
			};

			console.log(postParams);

				var api = ENV.apiEndpoint + '/dashboardlistteamdata';

				return studentService.postData(accessToken, api, postParams)
				.then(function(data){
					//alert("meeee");
					//$scope.applicationleadFilterData = data.data;
					var dataset = data;
					$scope.tableParamsApplication = new NgTableParams({}, {dataset: dataset});
					console.log();
				//	$scope.showTable = true;

				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.dashboardlistteamdataintakewise = function(partnerId,teamChecked,leadState){

			var intake_year = $routeParams.year;
			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
					"intake_year" : intake_year,
					"lead_state" : leadState
			};

			console.log(postParams);

				var api = ENV.apiEndpoint + '/dashboardlistteamdataintakewise';

				return studentService.postData(accessToken, api, postParams)
				.then(function(data){
					//alert("meeee");
					//$scope.applicationleadFilterData = data.data;
					var dataset = data;
					$scope.tableParamsApplication = new NgTableParams({}, {
						counts: [10, 50, 100, 500, 1000],
						dataset: dataset
					});
					//console.log();
				//	$scope.showTable = true;

				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.dashboardlistteamdatabankintakewise = function(partnerId,productId,teamChecked,leadState){

			var intake_year = $routeParams.year;
			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"product_id" : productId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
					"intake_year" : intake_year,
					"lead_state" : leadState
			};

			console.log(postParams);

				var api = ENV.apiEndpoint + '/dashboardlistteamdatabankintakewise';

				return studentService.postData(accessToken, api, postParams)
				.then(function(data){
					//alert("meeee");
					//$scope.applicationleadFilterData = data.data;
					var dataset = data;
					$scope.tableParamsApplication = new NgTableParams({}, {dataset: dataset});
					console.log();
				//	$scope.showTable = true;

				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.imperialFilterReport = function(){

			var postParams = {
					"student_type" : $scope.imperial_type,
					"agent_list" : $scope.lead_rm_id ? $scope.lead_rm_id : 0,
					"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
					"intake_year" : $scope.intake_year
			};

				var api = ENV.apiEndpoint + '/imperial/student/filter';

				return studentService.postData(accessToken, api, postParams)
				.then(function(data){
					
					var dataset = data;
					$scope.tableParams = new NgTableParams({}, {
						counts: [10, 50, 100, 500, 1000],
						dataset: dataset
					});

				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};


		$scope.intakeYearFilter = function(){
			var intake_year_filter = $scope.intake_year_filter;
			//alert();
			if(filter == 'intake-dashboard'){
				window.location.href='/dashboard/intake/'+intake_year_filter;
			} else if (filter == 'banks-intake') {
				window.location.href='/dashboard/banks/intake/'+intake_year_filter;
			} else if(filter == 'imperialData'){
				window.location.href='/dashboard/imerialdata/'+intake_year_filter;
			}

		};


        $scope.isEditable = false;
        $scope.editData = function () {
            $scope.isEditable = true;
        };
        $scope.saveData = function () {
            $scope.isEditable = false;
            console.log($scope.comment);
        };

        $scope.allStatus = [
            {
                id: 1,
                status: "REFERRED"
            },
            {
                id: 2,
                status: "INTERESTED"
            },
            {
                id:3,
                status: "POTENTIAL"
            }
        ];

        $scope.comments = [
            {
                id: 1,
                value: "Comment-1"
            },
            {
                id: 2,
                value: "Comment-2"
            }
        ];

				$scope.allStatus = [
				 {
					 id: "INTERESTED",
					 status: "Interested"
				 },
				 {
					 id: "NOT_INTERESTED",
					 status: "Not Interested"
				 },
				 {
					 id: "POTENTIAL",
					 status: "Potential"
				 },
				 {
					 id: "POTENTIAL_DECLINED",
					 status: "Potential Declined"
				 },
				 {
					 id: "APPLICATION_PROCESS",
					 status: "Application Process"
				 },
				 {
					 id: "APPLICATION_REVIEW"	,
					 status: "Application Review"
				 },
				 {
					 id: "APPLICATION_REVIEW_DECLINED"	,
					 status: "Application Review Declined"
				 },
				 {
					 id: "PROVISIONAL_OFFER"	,
					 status: "Provisional Offer"
				 },
				 {
					 id: "DISBURSAL_DOCUMENTATION"	,
					 status: "Disbursal Documentation"
				 },
				 {
					 id: "DISBURSED"	,
					 status: "Disbursed"
				 },
				 {
					 id: "PARTIALLY_DISBURSED"	,
					 status: "Partially Disbursed"
					},
				 {
					 id: "BEYOND_INTAKE"	,
					 status: "Beyond Intake"
				 }
			 ];

        $scope.changeName = function(comment){
            console.log(comment);
        };

		$scope.enable = function(currentLeadState){
			$('#'+currentLeadState).toggle();
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};


		$scope.agentsList = function(){

				var api = ENV.apiEndpoint + '/agentslist';

				return dashboardService.getdisbursedData(api, accessToken)
				.then(function(data){
					$scope.agentslist = data;

					// console.log(agentslist);

				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.basicprofilereport = function(){

			var rmId = $scope.rm_id ? $scope.rm_id : 0;
			var intakeYear = $scope.intake_year ? $scope.intake_year : 0;

			if( rmId == 0 || intakeYear == 0){
				alert("Please give Input...!!");
				return ;
			}

				var api = ENV.apiEndpoint + '/reportbasicprofile/'+intakeYear+'/'+rmId;

				return studentService.getData(api, accessToken)
				.then(function(data){
					//alert("meeee");
					$scope.basicProfileReportList = data;
					console.log(data);

				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.basicprofilereportdata = function(percentageValue,leadState){

			var rmId = $scope.rm_id ? $scope.rm_id : 0;
			var intakeYear = $scope.intake_year ? $scope.intake_year : 0;

				var postParams = {
					"percentage_value" : percentageValue,
					"lead_state" : leadState,
					"partner_id" : rmId,
					"intake_year" : intakeYear
				};

				var api = ENV.apiEndpoint + '/reportbasicprofiledata';

				return studentService.postData(accessToken, api, postParams)
				.then(function(data){

					//$scope.basicProfileReportListData = data;
					var dataset = data;
					$scope.basicProfileReportListData = new NgTableParams({}, {dataset: dataset});
					console.log();


				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.leadfollowupbydate = function(){

				var postParams = {
					"rm_id" : $scope.lead_rm_id ? $scope.lead_rm_id : 0,
					"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
					"start_date" : $('#start_date').val(),
					"end_date" : $('#end_date').val()
				};

				var api = ENV.apiEndpoint + '/leads/byfollowupdate';

				return studentService.postData(accessToken, api, postParams)
				.then(function(data){

					var dataset = data;
					$scope.tableParamsLead = new NgTableParams({}, {dataset: dataset});
					console.log();


				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};

		switch(filter){

      case 'dashboard':
          //$scope.title = "All Source List";
					$scope.dashboardList();
					$scope.dashboardListTeam();

          break;

			case 'intake-dashboard':
					var intake_year = $routeParams.year;
					$scope.newdashboardListIntakeWise(intake_year);
					$scope.newdashboardListTeamIntakeWise(intake_year);
					break;

    case 'banks':
          $scope.dashboardListBankTeam();
					$scope.dashboardListBankOverall();
					$scope.dashboardListBankTeamIndividual();
          break;

		case 'banks-intake':
					var intake_year = $routeParams.year;
			    $scope.dashboardListBankTeamIntakeWise(intake_year);
					$scope.dashboardListBankOverallIntakeWise(intake_year);
					$scope.dashboardListBankTeamIndividualIntakeWise(intake_year);
          break;

		case 'studentbanks':
			    $scope.dashboardListBankTeamStudent();
					$scope.dashboardListBankOverallStudent();
					$scope.dashboardListBankTeamIndividualStudent();
         break;

         case 'imperialData':
				 var intake_year = $routeParams.intakeYear;
					//$scope.dashboardListImperialBranchOverall(intake_year);
         break;

				 case 'imperial-student-filter':
				 $scope.agentsList();

         break;

         case 'smmAllLeads':

					$scope.dashboardAllStudentList();
         break;

				 case 'basic-profile-report':

					$scope.agentsList();
         break;

				 case 'leads-by-folloupdate':
	           $scope.agentsList();

	           break;
    };

		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$scope.viewFile = $route.current.$$route.pageName;
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();
		//$scope.getDashboardData();
		//$scope.getDashboardCount();

    }]);
