'use strict';

angular.module('app')
	.controller('forexController',  ['$scope', '$location', '$rootScope', 'commonService', 'forexService', 'dashboardService', 'assignService', '$cookieStore', 'menuService', '$route', '$routeParams', 'listService', 'ENV', 'NgTableParams', function ($scope, $location, $rootScope, commonService, forexService, dashboardService, assignService, $cookieStore, menuService, $route, $routeParams, listService, ENV, NgTableParams){

        var accessToken = $cookieStore.get("access_token");
        var partnerName = $cookieStore.get("partner_name");
        var roleId = $cookieStore.get("rId");
        var filter = $route.current.$$route.filter;

				$scope.allcitieslist = [];
		    $scope.rmcity = [];
		    $scope.citySetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };
		    $scope.rmforex = [];
		    $scope.forexSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };
		    $scope.rmstate = [];
		    $scope.stateSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };
		    $scope.parentrmid = [];
		    $scope.parentRMSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };


				$scope.disbursed_bank_id = [];
				$scope.bankSetting = { displayProp: 'value', idProperty:'id', enableSearch: true };
        $scope.role_id = roleId;
        $scope.bank_id;

        $scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
    };

    $scope.getCurrentStudent = function(){
      var apiUrl = "";
  return forexService.getData(apiUrl, accessToken)
  .then(function(data){
    $scope.menus = data;
  })
  .catch(function(error){
    if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
      window.location.href = '/';
    }
  });
  };

  $scope.getOverAllStudent = function(){
    var apiUrl = "";
return forexService.getData(apiUrl, accessToken)
.then(function(data){
  $scope.menus = data;
})
.catch(function(error){
  if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
    window.location.href = '/';
  }
});
};

$scope.forexList = function(){

		var api = ENV.apiEndpoint + '/forexList/1';

		return assignService.sourceRawList(api, accessToken)
		.then(function(data){
				$scope.forexlists = data;
			//console.log("hello",data.data);

			//console.log("errrr");

		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

$scope.stateList = function(){

		var api = ENV.apiEndpoint + '/statemaster';

		return assignService.sourceRawList(api, accessToken)
		.then(function(data){
				$scope.statelists = data;

		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

$scope.citiesByStateId = function(){

	var postParams = {
		"state_list" : $scope.rmstate ? $scope.rmstate : 0,
	};

 var api = ENV.apiEndpoint + '/citiesbystateid';

		return assignService.saveSourceData(api, postParams, accessToken)
		.then(function(data){

				$scope.allcitieslist  = data.data;

		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

$scope.forexRmList = function(){

		//var bank_id = $scope.bank_id;

		var postParams = {
			"forex_list" : $scope.rmforex
		}

		var api = ENV.apiEndpoint + '/forexrmlist';

		return assignService.saveSourceData(api, postParams, accessToken)
		.then(function(data){

			var dataset = data.data;

			$scope.tableParams = new NgTableParams({}, {dataset: dataset});

		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

$scope.forexRmById = function(rmId){

	$scope.forexList().then(function(pdata) {

		$scope.stateList().then(function(sdata) {

			var api = ENV.apiEndpoint + '/forexrmbyid/' + rmId;

			return assignService.sourceRawList(api, accessToken)
			.then(function(data){

				console.log("1");

					$scope.allcitieslist = data.city_list;
					$scope.parentrmlist = data.parent_list;

					var productArray = [];
					var stateArray = [];
					var cityArray = [];
					var parentArray = [];
					var rmdefaultCities = 'NO';

					var productlistarray = $scope.forexlists;
					var selectedProductList = data[0].products;

					//var citylistarray = $scope.allcitieslist;
					var citylistarray = data.city_list;
					var selectedCityList = data[0].city;

					var statelistarray = $scope.statelists;
					var selectedStateList = data[0].state ? data[0].state :[];

					var parentrmlistarray = data.parent_list;

					console.log("1");

					selectedProductList.forEach( function(productId) {

						productlistarray.forEach(function (productArrayId,index) {

							if(productId == productArrayId['id']){

								productArray.push(productlistarray[index]);
							}


						})

					});
					console.log("1");

					selectedStateList.forEach( function(stateId) {

						statelistarray.forEach(function (stateArrayId,indexstate) {

							if(stateId == stateArrayId['id']){

								stateArray.push(statelistarray[indexstate]);
							}

						})

					});


					selectedCityList.forEach( function(cityId) {

						if(cityId == 0){
							rmdefaultCities   = 'YES';
						 }

						citylistarray.forEach(function (citylistarrayArrayId,indexcity) {

							if(cityId == citylistarrayArrayId['id']){

								cityArray.push(citylistarray[indexcity]);
							}
						})

					});

					var parent_id = data[0].parent_id ? data[0].parent_id : 0;

					if( parent_id != 0 ){

						parentrmlistarray.forEach(function (parentArrayId,indexparent) {

							if(parent_id == parentArrayId['id']){
								parentArray.push(parentrmlistarray[indexparent]);
							}

						});

					}

					console.log("hello",cityArray);
					$scope.rmid = data[0].id;
					$scope.rmname = data[0].name;
					$scope.rmemail = data[0].email;
					$scope.rmmobile = data[0].mobile;
					$scope.rmmobile1 = data[0].mobile1;
					$scope.rmtype = data[0].contact_type;
					$scope.rmforex = productArray;//data[0].products;
					$scope.rmstate = stateArray;
					$scope.rmcity = cityArray;
					$scope.rmstatus = data[0].status;
					$scope.rmdefault = rmdefaultCities;
					$scope.parentrmid = parentArray;

			})
			.catch(function(error){
							$scope.error = {
									message: error.message
							};
			});

			});

		});

};

$scope.forexAllCities = function(){

		var api = ENV.apiEndpoint + '/allcities';

		return assignService.sourceRawList(api, accessToken)
		.then(function(data){

			var dataset = data;
			$scope.allcitieslist = data;

		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};


$scope.addForexRm = function(){

		var postParams = {
			"rm_name" : $scope.rmname,
			"rm_email" : $scope.rmemail,
			"rm_mobile" : $scope.rmmobile,
			"rm_mobile1" : $scope.rmmobile1,
			"rm_type" : $scope.rmtype,
			"rm_product" : $scope.rmforex,
			"rm_state" : $scope.rmstate,
			"rm_city" : $scope.rmcity,
			"rm_status" : $scope.rmstatus,
			"rm_default" : $scope.rmdefault,
			"rm_parent" : $scope.parentrmid
		};

		var api = ENV.apiEndpoint + '/add/forexrm';

		return assignService.saveSourceData(api, postParams, accessToken)
		.then(function(data){
			alert('Successfully ADDED....!!');
			//window.location.href = '/bank/rmlist';

		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

$scope.editForexRm = function(){

		var postParams = {
			"rm_id" : $scope.rmid,
			"rm_name" : $scope.rmname,
			"rm_email" : $scope.rmemail,
			"rm_mobile" : $scope.rmmobile,
			"rm_mobile1" : $scope.rmmobile1,
			"rm_type" : $scope.rmtype,
			"rm_product" : $scope.rmforex,
			"rm_state" : $scope.rmstate,
			"rm_city" : $scope.rmcity,
			"rm_status" : $scope.rmstatus,
			"rm_default" : $scope.rmdefault,
			"rm_parent" : $scope.parentrmid
		};

		var api = ENV.apiEndpoint + '/edit/forexrm';

		return assignService.saveSourceData(api, postParams, accessToken)
		.then(function(data){
			alert('Successfully UPDATED....!!');
			//window.location.href = '/bank/rmlist';

		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

$scope.forexRmByRoleProduct = function(){

		var postParams = {
			"rm_type" : $scope.rmtype,
			"rm_forex" : $scope.rmforex
		};

		var api = ENV.apiEndpoint + '/forexrmlist/byroleproduct';

		return assignService.saveSourceData(api, postParams, accessToken)
		.then(function(data){

			$scope.parentrmlist = data.data;

		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};


$scope.getMyAssignedLeads = function(queryParams){
	var predefinedState = $location.search().lead_state ? $location.search().lead_state : '';
	var partnerId = $location.search().pid ? $location.search().pid : '';
	if(partnerId){
		queryParams = {
			type : 'counselor',
			lead_state: 'all',
			pid: partnerId
		}
	}

	return forexService.getList(queryParams, accessToken)
	.then(function(response){
		var todayFollowup = [];
		response.forEach(function(value, key){
			if(value.followup_date){
				var currentDate = new Date();
				var followupDate = new Date(value['followup_date']);
				if(currentDate > followupDate){
					todayFollowup.push(value);
				}
			}
			//response[key]['followup_date'] = new Date(value['followup_date']);
		})
		//$scope.followlisttoday = response;
		$scope.followlisttoday = new NgTableParams({}, {dataset: todayFollowup});
		return commonService.getListData(5)
		.then(function(listData){
			response.forEach(function(leadInfo, index){
				var displayLoanType = "";
				listData.forEach(function(loanType){
					if(leadInfo['loan_type'] && leadInfo['loan_type'].indexOf(loanType['id']) !== -1){
						displayLoanType += loanType['display_name'] + ", ";
					}
				});
				response[index]['loan_type'] = displayLoanType;
				if($scope.type == 'offer'){
					response[index]['offer_name'] = '';
					$scope.offers.forEach(function(offer){
						if(offer['id'] == leadInfo['offer_id']){
							response[index]['offer_name'] = offer['display_name'];
						}
					})
				}
				//response[index]['followup_date'] = new Date(leadInfo['followup_date']);
			});
			$scope.tableParams = new NgTableParams({filter:{current_lead_state: predefinedState}}, {dataset: response});
		});
	})
	.catch(function(err){
		alert("No lead found");
		$scope.tableParams = new NgTableParams({}, {dataset: []});
	})
};


$scope.getMyLeads = function(queryParams){

	return forexService.getList(queryParams, accessToken)
	.then(function(response){
		return commonService.getListData(5)
		.then(function(listData){
			response.forEach(function(leadInfo, index){
				var displayLoanType = "";
				listData.forEach(function(loanType){
					if(leadInfo['loan_type'] && leadInfo['loan_type'].indexOf(loanType['id']) !== -1){
						displayLoanType += loanType['display_name'] + ", ";
					}
				});
				response[index]['loan_type'] = displayLoanType;
			});
			$scope.tableParams = new NgTableParams({}, {dataset: response});
			//console.log("hello",response);

		});
	})
	.catch(function(err){
		alert("No lead found");
		$scope.tableParams = new NgTableParams({}, {dataset: []});
	})
};


        switch (filter) {

          case 'forex-followuptoday-student':

						$scope.title = "Student List";
						var type = $routeParams.type ? $routeParams.type : '';
						$scope.type = $routeParams.type;

						var queryParams = {
							type : type
						}
						$scope.getMyAssignedLeads(queryParams);
            break;

          case 'forex-assigned-me-student':

            $scope.title = "Student List";
						var type = $routeParams.type ? $routeParams.type : '';
						$scope.type = $routeParams.type;

						var queryParams = {
							type : type
						}
						$scope.getMyAssignedLeads(queryParams);

            break;

         case 'forex-assigned-me-type-student':

							 $scope.title = "Student List";
							 var type = $routeParams.type ? $routeParams.type : '';
							 $scope.type = $routeParams.type;

							 var queryParams = {
								 type : type
							 }
							 $scope.getMyAssignedLeads(queryParams);

          case 'forex-my-student':
              $scope.title = "Student List";
							var queryParams = {
								type : 'originator'
							}
              $scope.getMyLeads(queryParams);
              break;

					case 'forex-rm-list':
					     $scope.title = "New Forex RM List";
					     $scope.forexList();
					     break;

			    case 'forex-rm-add':
		 	        $scope.title = "Forex RM Add";
			        $scope.forexList();
			        $scope.stateList();
					       break;

			    case 'forex-rm-edit':
					     $scope.title = "Forex RM Edit";
					     var rmId = $routeParams.rmId;
				      $scope.forexRmById(rmId);
			        break;
				};

        $scope.viewFile = $route.current.$$route.pageName;
        $scope.location = $location.path();
        $scope.partnerName = partnerName;
        $scope.header = 'views/header.html';
        $scope.menu = 'views/menu.html';
        $scope.footer = 'views/footer.html';
        $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
        $scope.getMenu();

  }]);
