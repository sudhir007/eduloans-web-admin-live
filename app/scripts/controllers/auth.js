'use strict';

angular.module('app')
	.controller('authController', ['$scope', 'authService', 'ENV', '$cookieStore', '$rootScope', function ($scope, authService, ENV, $cookieStore, $rootScope){
		//angular.element(document.body).addClass("login-page");
		if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
		}
		$scope.submitLogin = function(){
            $scope.error = {
                message: null
            };
            var postParams =  {
    			'username': $scope.username,
    			'password': $scope.password
    		};

				$('#loaderAjax').show();

    		var api = ENV.apiEndpoint + '/auth/login';

    		return authService.login(api, postParams)
    		.then(function(data){

					$('#loaderAjax').hide();

				var accessToken = data.access_token;
				data.login_object.last_name = data.login_object.last_name ? data.login_object.last_name : '';
				var partnerName = data.login_object.first_name + ' ' + data.login_object.last_name;
				var loginId = data.login_object.id;
				var roleId = data.login_object.role_id;
				var login_id = data.login_object.login_id;

        $cookieStore.put("access_token", accessToken);
				$cookieStore.put("partner_name", partnerName);
				$cookieStore.put("pId", loginId);
				$cookieStore.put("login_id", login_id);
				$cookieStore.put("rId", roleId);
				if(roleId == 10 || roleId == '10'){
					window.location.href='/banks/leads';
				} else {
					window.location.href='/dashboard/intake/2025';
				}

    		})
    		.catch(function(error){
					     $('#loaderAjax').hide();
                $scope.error = {
                    message: error.message
                };
    		});
        };

		$scope.logout = function(){
			var accessToken = $cookieStore.get("access_token");
			var postParams =  {};

    		var api = ENV.apiEndpoint + '/auth/logout';

    		return authService.logout(api, postParams, accessToken)
    		.then(function(){
				$cookieStore.remove("access_token");
    			window.location.href='/';
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentBreakIn = function(){
			var accessToken = $cookieStore.get("access_token");
			var postParams =  {
                "break_in_type" : $scope.breakin_type
            };

    		var api = ENV.apiEndpoint + '/callcenter/breakin';

    		return authService.callcenterpost(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('You Are On Break In !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentBreakOut = function(){
			var accessToken = $cookieStore.get("access_token");
			var postParams =  {};

    		var api = ENV.apiEndpoint + '/callcenter/breakout';

    		return authService.callcenterget(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('You Are On Break Out !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentWrapIn = function(){
			var accessToken = $cookieStore.get("access_token");
			var postParams =  {};

    		var api = ENV.apiEndpoint + '/callcenter/agentwrapin';

    		return authService.callcenterget(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('You Are WrapIn !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentWrapOut = function(){
			var accessToken = $cookieStore.get("access_token");
			var postParams =  {};

    		var api = ENV.apiEndpoint + '/callcenter/agentwrapout';

    		return authService.callcenterget(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('You Are WrapOut !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentReady = function(){
			var accessToken = $cookieStore.get("access_token");
			var postParams =  {};

    		var api = ENV.apiEndpoint + '/callcenter/agentready';

    		return authService.callcenterget(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('You Are Ready For call !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentNotReady = function(){
			var accessToken = $cookieStore.get("access_token");
			var postParams =  {};

    		var api = ENV.apiEndpoint + '/callcenter/agentnotready';

    		return authService.callcenterget(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('You Are Not Ready For Call !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentDialCall = function(){
			var accessToken = $cookieStore.get("access_token");
			var callNumber = $scope.phoneNumber;

			if(callNumber == ""){
				alert("Mandatory");
			}

			var postParams =  {"dial_number": callNumber };

    		var api = ENV.apiEndpoint + '/callcenter/dialcall';

    		return authService.callcenterpost(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('You Are Calling !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentSendSMS = function(){
			var accessToken = $cookieStore.get("access_token");
			var callNumber = $scope.user_number;
			var msg = $scope.sms_msg;

			if(callNumber == ""){
				alert("Mandatory");
			}

			var postParams =  {
				"mobile_number" : callNumber,
				"msg" : msg
			 };

    		var api = ENV.apiEndpoint + '/callcenter/smsagent';

    		return authService.callcenterpost(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('SMS Sent !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.agentAutoCall = function(callnumber){
			var accessToken = $cookieStore.get("access_token");
			var finalCallNummber = callnumber.replace(/ +/g, "");
 					finalCallNummber = "0" + finalCallNummber.substr(finalCallNummber.length - 10);

			var postParams =  { "dial_number" : finalCallNummber };

    		var api = ENV.apiEndpoint + '/callcenter/dialcall';

    		return authService.callcenterpost(api, postParams, accessToken)
    		.then(function(){
				//$cookieStore.remove("access_token");
    			//window.location.href='/';
					alert('You Are Calling !');
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.submitSearch = function(){
			var accessToken = $cookieStore.get("access_token");
			if(accessToken === undefined || !accessToken){
				window.location.href = '/';
			}
			else{
				$scope.value = $scope.value.replace(/\./g, "[dot]");
				window.location.href='/students/search?type=' + $scope.typename + '&value=' + $scope.value;
			}
		};
}]);
