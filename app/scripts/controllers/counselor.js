'use strict';

angular.module('app')
  .controller('counselorController', ['$scope', 'commentsService', 'assignService', 'studentService', 'ENV', '$cookieStore', 'commonService', 'bulkuploadService', '$routeParams', 'NgTableParams', '$compile', '$route', 'menuService', '$location', '$rootScope', '$q','popUpService', function ($scope, commentsService, assignService, studentService, ENV, $cookieStore, commonService, bulkuploadService, $routeParams, NgTableParams, $compile, $route, menuService, $location, $rootScope, $q, popUpService) {


    if (!$rootScope.bodylayout) {
      $rootScope.bodylayout = "login-page";
    }

    var accessToken = $cookieStore.get("access_token");
    var filter = $route.current.$$route.filter;
    $scope.test = [];

    $scope.rmList = [];
    $scope.facilitator_email = [];
    $scope.rmSetting = { displayProp: 'email', idProperty:'id', enableSearch: true, selectedToTop: true };


    $scope.getMenu = function () {
      return commonService.getMenu(accessToken)
        .then(function (data) {
          $scope.menus = data;
        })
        .catch(function (error) {
          if (error.error_code === "SESSION_EXPIRED"  || error.error_code === "HEADER_MISSING") {
            window.location.href = '/';
          }
        });
    };

    $scope.toggleMenu = function (id) {
      $('#menu-' + id).toggle();
    };
    $scope.toggleSubMenu = function (id) {
      $('#submenu-' + id).toggle();
    };

    $scope.myAssignList = function () {

      var lead_state = $routeParams.leadstate;

      var api = ENV.apiEndpoint + '/counselor/counselorcallinglist/' + lead_state;

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          //console.log(data);
          $scope.assignlist = data.data;
          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            dataset: dataset
          });

          $scope.data.forEach(function (employee) {
            employee.isEditable = false;
          });

          //console.log(assignlist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.counsellorsList = function () {

      var api = ENV.apiEndpoint + '/counselor/counselorlist';

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          $scope.counselorlist = data.data;

          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            dataset: dataset
          });

          console.log("List", counselorlist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.allcounsellorsList = function () {

      var api = ENV.apiEndpoint + '/counselor/counselorlistall';

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          $scope.counselorlist = data.data;

          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            counts: [10, 50, 100, 1000, 5000],
    				dataset: dataset
          });

          console.log("List", counselorlist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.connectFlowMail = function (counselorLoginId) {

      $('#loaderAjax').show();

      var api = ENV.apiEndpoint + '/counselor/connectmail/'+counselorLoginId;

      return assignService.assignList(api, accessToken)
        .then(function (data) {

          $('#loaderAjax').hide();

          alert("Mail Sent !!");
          //console.log("List", data);

        })
        .catch(function (error) {

          $('#loaderAjax').hide();

          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.connectIntroductionMail = function (counselorCallingId) {

      var api = ENV.apiEndpoint + '/counselor/introduction/'+counselorCallingId;

      return assignService.assignList(api, accessToken)
        .then(function (data) {

          alert("Mail Sent !!");
          //console.log("List", data);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    //$scope.isEditable = false;
    $scope.counseloreditData = function (emp) {
      emp.isEditable = true;

    //  cityArray.push(emp.facilitator_id);
      emp.facilitator_email = [{"id" : emp.facilitator_id, "email" : emp.facilitator_email}];
      //$scope.isEditable = true;
    };

    $scope.leadtransferData = function (emp) {
      emp.isEditable = true;
    };

    $scope.counselorRmUpdate = function (emp) {
      emp.isEditable = false;

      var rmIds = [];

      emp.facilitator_email.forEach(function (citylistarrayArrayId,indexcity) {

        rmIds.push(citylistarrayArrayId['id']);

      });

      var postParams = {
        "partner_id": emp.partner_id,
        "lead_assignment": emp.lead_assignment,
        "assign_old_leads": emp.assign_old_leads,
        "facilitator_id": rmIds
      };

      var api = ENV.apiEndpoint + '/counselor/facilitatoridupdate';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {

          //console.log(assignlist);
          $route.reload();

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });

    };


    //$scope.isEditable = false;
    $scope.editData = function (emp) {
      emp.isEditable = true;
      //$scope.isEditable = true;
    };

    $scope.cancelEditData = function (emp) {
      emp.isEditable = false;
      //$scope.isEditable = true;
    };

    $scope.saveData = function (emp) {
      //$scope.appkeys[$scope.editing] = $scope.newField;

      emp.isEditable = false;
      //console.log('hello', emp);
      var postParams = {
        "id": emp.id,
        "name": emp.name,
        "company_name": emp.company_name,
        "email": emp.email,
        "mobile": emp.mobile,
        "mobile1": emp.mobile1,
        "city": emp.city,
        "state": emp.state,
        "source_name": emp.source_name
      };

      var api = ENV.apiEndpoint + '/counselor/counselorcallinglistupdate';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {

          //console.log(assignlist);
          $route.reload();

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });


    };

    $scope.claculateTime = function (dt) {
      return new Date(dt).getTime();

    };

    $scope.couselorCommentById = function (type) {

      var counselorId = $routeParams.id;

      var api = ENV.apiEndpoint + '/counselor/commentlist/' + counselorId + '/' + type;

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          //console.log(data.data);
          $scope.userInfo = data.user_data;
          $scope.assignlist = data.data;
          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            dataset: dataset
          });

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.dateformat = function (D) {
      var pad = function (num) {
        var s = '0' + num;
        return s.substr(s.length - 2);
      }
      var Result = D.getFullYear() + '-' + pad((D.getMonth() + 1)) + '-' + pad(D.getDate());
      return Result;
    };

    $scope.updateCounselorCallingComment = function () {

      var callId = $routeParams.id;

      var dateString = $scope.next_FollowUp_Date ? $scope.dateformat($scope.next_FollowUp_Date) : null;
      //  console.log(dateString);
      //console.log("Time :", $scope.time_value);
      if (!($scope.time_value)) {

        alert("Time Spent Not Given !");
        return;

      }

      if (!($scope.counselor_state) && filter != 'counselor-comments') {

        alert("Counselor State Not Given !");
        return;

      }

      if (!(dateString) && filter != 'counselor-comments') {

        if ($scope.counselor_state == 'NOTINTERESTED') {
          //console.log("else");

        } else {
          alert("Next Followup Date Not Given !");
          return;
        }

      }

      var entity_type = "CALLINGCOUNSELOR";

      if(filter == 'counselor-comments') {
        entity_type = "COUNSELOR";
      }

      var postParams = {
        "entity_id": callId,
        "entity_type": entity_type,
        "time_spent": $scope.time_value,
        "comment": $scope.comments_value,
        "counselor_status": $scope.counselor_state,
        "next_followup_date": dateString
      };

      //console.log(postParams);

      var api = ENV.apiEndpoint + '/counselor/commentadd';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {
          //$scope.commentslist = data;

          //console.log(data,"hellloooooooo");
          $('#comments_value').val('');
          $('#time_value').val('');
          $('#next_FollowUp_Date').val('');

          if(filter == 'counselor-comments'){
            window.location.href = '/counselor/comments/' + callId;
          } else {
            window.location.href = '/counselor/callingcomments/' + callId;
          }

          //$('#modal-default').modal('hide');
          //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });

    };

    $scope.addPartner = function () {
      if (!$scope.title || !$scope.first_name || !$scope.last_name || !$scope.email || !$scope.country || !$scope.company_name || !$scope.company_url || !$scope.landline || !$scope.city) {
        //console.log("if", $scope.title, $scope.first_name, $scope.last_name, $scope.email, $scope.country, $scope.company_name, $scope.company_url, $scope.landline, $scope.city);
        alert("Please fill all mandatory details");
        return;
      }
      //console.log($scope);
      //console.log("else", $scope.gentitle, $scope.first_name, $scope.last_name, $scope.email, $scope.country, $scope.company_name, $scope.company_url, $scope.landline, $scope.city);
      //var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
      var counselorCallingId = $routeParams.id;
      //let utmSource = $cookieStore.get("utm_source");
      $scope.loader = true;
      var postParams = {
        partner_type: $scope.partner_type,
        title_id: $scope.gentitle,
        first_name: $scope.first_name,
        last_name: $scope.last_name,
        email: $scope.email,
        country_code: $scope.country_code,
        mobile_number: $scope.mobile,
        country_id: $scope.country,
        calling_id: counselorCallingId,
        // address: $scope.address,
        address: {
          "address": $scope.address
        },
        other_details: {
          "company_name": $scope.company_name,
          "counsellor_type" : $scope.counsellor_type,
          "company_url": $scope.company_url,
          "agreement": $scope.agreement
        },
        role_id: 12,
        landline_number: $scope.landline,
        state_id: $scope.state,
        city_id: $scope.city
        //city_name: $scope.city.title
      }
      return commonService.add(accessToken, 'register', postParams)
        .then(function (data) {
          //$cookieStore.remove("utm_source");
          $scope.loader = false;
          if( document.getElementById("logofile").files.length == 0 ){
              //console.log("no files selected");
              alert(data.result);
              window.location.href = '/counselor/list';
            }  else {

          var fileUploadParams = {
            partner_id: data.partner_id,
            document_meta_id: '13',
            entity_type: 'partner',
            userfile: $scope.file
          }
          return commonService.upload(accessToken, '/document-upload', fileUploadParams)
            .then(function (data) {
              alert("UserName and Password has been sent to Registered mail...");
              window.location.href = '/counselor/list';
            })
            .catch(function (err) {
              $scope.loader = false;
              alert(err.message);
            })
          }
          //alert(data.result);
        })
        .catch(function (err) {
          $scope.loader = false;
          alert(err.message);
        })

    };

    $scope.getAllTitles = function () {
      var listId = 3;
      commonService.getListData(listId)
        .then(function (data) {
          $scope.allTitles = data;
        });
    };

    $scope.getAllCountries = function () {
      var listId = 1;
      $scope.roleId = $cookieStore.get("role_id");
      // console.log($scope.roleId);
      commonService.getListData(listId)
        .then(function (data) {
          $scope.allCountries = data;
        });
    };

    $scope.getAllStateByCountryId = function () {
			var postParams = {
				country_id: $scope.country
			};
			var api = '/statesbycountryid';

			commonService.getdata(api, postParams)
				.then(function (data) {
					$scope.allStates = data;
				});
		};

		$scope.getAllCityByStateId = function () {
			var postParams = {
				state_id: $scope.state
			};
			var api = '/citylistbystateid';

			 commonService.getdata(api, postParams)
				.then(function (data) {
					$scope.allCities = data;
				});
		};

    $scope.getAllStateByCountryIdOnload = function (countryId) {
			var postParams = {
				country_id: countryId
			};
			var api = '/statesbycountryid';

			commonService.getdata(api, postParams)
				.then(function (data) {
					$scope.allStates = data;
				});
		};

		$scope.getAllCityByStateIdOnload = function (stateId) {
			var postParams = {
				state_id: stateId
			};
			var api = '/citylistbystateid';

			 commonService.getdata(api, postParams)
				.then(function (data) {
					$scope.allCities = data;
				});
		};

    $scope.agentsList = function () {

      var postParams = {};

      var api = ENV.apiEndpoint + '/agentslist';

      return assignService.agentList(api, postParams, accessToken)
        .then(function (data) {
          $scope.agentslist = data;

          //console.log(agentslist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.counselorCallingInfo = function (counselorCallingId) {

      var postParams = {};

      var api = ENV.apiEndpoint + '/counselor/callinginfo/'+counselorCallingId;

      return assignService.agentList(api, postParams, accessToken)
        .then(function (data) {
          $scope.gentitle = data.data.title_id;
          $scope.first_name = data.data.name;
          //$scope.last_name = data.last_name;
          $scope.email = data.data.email;
          $scope.mobile = data.data.mobile;
          $scope.company_name = data.data.company_name;
          $scope.companyurl = data.data.website_name;

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.uploadFile = function (userFile) {

      $scope.error = {
        message: null
      };

      // var sourceId = $scope.select_source;
      var sourceName = $scope.source_name;
      var AgentId = $scope.select_agent ? $scope.select_agent : 0;

      var file = $scope.userfile;
      //console.log('file is ' + file);
      console.dir(file);

      var api = ENV.apiEndpoint + '/counselor/counselorupload';

      return bulkuploadService.counselorFileUpload(api, file, sourceName, AgentId, accessToken)
        .then(function (data) {

          // window.location.href='/counselor/callinglist';
        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.getLeadStateFiltterData = function (lead_state) {

      $location.url('/counselor/callinglist/' + lead_state);

      var api = ENV.apiEndpoint + '/counselor/counselorcallinglist/' + lead_state;

      return assignService.assignList(api, accessToken)
        .then(function (data) {

          $scope.assignlist = data.data;
          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            dataset: dataset
          });

          $scope.data.forEach(function (employee) {
            employee.isEditable = false;
          });

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.createCallingData = function () {

      var postParams = {
        "name": $scope.user_name,
        "email": $scope.user_email,
        "mobile": $scope.user_mobile,
        "mobile1": $scope.user_mobile1,
        "company_name": $scope.company_name,
        "state": $scope.user_state,
        "city": $scope.user_city,
        "source_name": $scope.source_name,
        "counselor_status": "FRESH",
        "type": "counselor"
      };

      //console.log(postParams);

      var api = ENV.apiEndpoint + '/callingdata/add';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {

            window.location.href = '/counselor/callinglist/FRESH';

        })
        .catch(function (error) {
          alert(error.message);
          $scope.error = {
            message: error.message
          };
        });

      };

    $scope.allCounselorCallStatus = [{
        id: "FRESH",
        status: "FRESH"
      },
      {
        id: "NOTINTERESTED",
        status: "NOTINTERESTED"
      },
      {
        id: "INTERESTED",
        status: "INTERESTED"
      },
      {
        id: "CALLBACK",
        status: "CALLBACK"
      },
      {
        id: "BEYONDINTAKE",
        status: "BEYONDINTAKE"
      },
      {
        id: "CONVERTED",
        status: "CONVERTED"
      },
      {
        id: "RINGING",
        status: "RINGING"
      }

    ];

    $scope.allCounsellorType = [{
        id: "A",
        display_name: "A - National Level"
      },
      {
        id: "B",
        display_name: "B -  Regional Level"
      },
      {
        id: "C",
        display_name: "C - Single Office"
      },
      {
        id: "D",
        display_name: "D - Single Man & Single Office"
      },
      {
        id: "E",
        display_name: "E - Freelancer"
      }
    ];

    $scope.getPartnerData = function(id){
        var api = ENV.apiEndpoint + '/' + id;
        return commonService.getPartnerData(api, accessToken)
        .then(function(response){
            $scope.gentitle = response.title_id;
            $scope.first_name = response.first_name;
            $scope.last_name = response.last_name;
            $scope.email = response.email;
            $scope.mobile = response.mobile_number;
            $scope.pan = response.pan;
            $scope.aadhar_number = response.aadhar_number;
            $scope.gst_number = response.gst_number;
            $scope.company_logo = "";
            if(response.other_details && response.other_details.company_name){
                $scope.company_name = response.other_details.company_name;
            }
            if(response.other_details && response.other_details.counsellor_type){
                $scope.counsellor_type = response.other_details.counsellor_type;
            }
            if(response.other_details && response.other_details.company_url){
                $scope.company_url = response.other_details.company_url;
            }
            if(response.other_details && response.other_details.company_logo){
                $scope.company_logo = response.other_details.company_logo;
            }
            if(response.other_details && response.other_details.agreement){
                $scope.agreement = response.other_details.agreement;
            }
            $scope.landline = response.telephone_number;
            if(response.address && response.address.address){
                $scope.address = response.address.address;
            }
            //$scope.city = {};
            //$scope.city.description = {};
            //$scope.city.description.id = response.city_id;
            //$scope.city.title = response.city_name;
            $scope.getAllStateByCountryIdOnload(response.country_id);
            $scope.getAllCityByStateIdOnload(response.state_id);

            $scope.country = response.country_id;
            $scope.state = response.state_id;
            $scope.city = response.city_id;
            $scope.partnerId = id;
        })
        .catch(function(error){
            console.log(error);
        })
    }

    $scope.updatePartner = function (partnerId) {
      if (!$scope.title || !$scope.first_name || !$scope.last_name || !$scope.email || !$scope.country || !$scope.company_name || !$scope.company_url || !$scope.landline || !$scope.city) {
        alert("Please fill all mandatory details");
        return;
      }
      var cityId = $scope.city ? $scope.city : 0;
      $scope.loader = true;
      var putParams = {
        title_id: $scope.gentitle,
        first_name: $scope.first_name,
        last_name: $scope.last_name,
        country_id: $scope.country,
        pan: $scope.pan,
        aadhar_number: $scope.aadhar_number,
        gst_number: $scope.gst_number,
        role_id: 12,
        address: {
          "address": $scope.address
        },
        other_details: {
          "company_name": $scope.company_name,
          "counsellor_type" : $scope.counsellor_type,
          "company_url": $scope.company_url,
          "company_logo": $scope.company_logo,
          "agreement": $scope.agreement
        },
        telephone_number: $scope.landline,
        city_id: cityId
      }

      return commonService.updatePartnerData(accessToken, '/' + partnerId, putParams)
        .then(function (data) {
          $scope.loader = false;
          if( document.getElementById("logofile").files.length == 0 ){
              alert(data.result);
              window.location.href = '/counselor/list';
            }  else {

          var fileUploadParams = {
            partner_id: data.partner_id,
            document_meta_id: '13',
            entity_type: 'partner',
            userfile: $scope.file
          }
          return commonService.upload(accessToken, '/document-upload', fileUploadParams)
            .then(function (data) {
              alert("The profile has been updated successfully.");
              window.location.href = '/counselor/list';
            })
            .catch(function (err) {
              $scope.loader = false;
              alert(err.message);
            })
          }
          //alert(data.result);
        })
        .catch(function (err) {
          $scope.loader = false;
          alert(err.message);
        })

    }

    $scope.allUploadDocumentTypes = [
      {
        id:13,
        name: "Company Logo"
      },
      {
        id:14,
        name: "GST Document"
      },
      {
        id:15,
        name: "Agreement Document"
      }

    ];

    // $scope.couuDocumentMeta = 0;
    // //var partnerId = $cookieStore.get("partnerId");
    $scope.uploadCounselorFile = function(){
      //console.log($scope);
      var partnerId = $routeParams.id;

      var postData = {
        entity_type: "partner",
        partner_id: partnerId,
        document_meta_id: $scope.couuDocumentMeta,
        userfile: $scope.userfile
      }
      return assignService.uploadCounselorDocument(accessToken, postData)
      .then(function(response){
        alert("The file has been uploaded successfully");
        //window.location.href = $location.absUrl();
        $scope.getCounselorDocuments(partnerId);
      })
      .catch(function(err){
        //console.log(err);
        alert(err.data.message);
      })
    }

    $scope.allCounselorDocuments = [];
    $scope.getCounselorDocuments = function(counselorId){
      //alert(counselorId);
      return assignService.getCounsellorDocuments(accessToken, counselorId , 'partner')
      .then(function(response){
        $scope.allCounselorDocuments = response;
      })
      .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.agentcounsellorList = [];
    $scope.agentcounsellorListAll = function(){
      //alert(counselorId);

      if($scope.select_entity_type == 'AGENT' ){

        var api = ENV.apiEndpoint + '/agentslist';

      } else if ($scope.select_entity_type == 'COUNSELLOR') {

        var api = ENV.apiEndpoint + '/indianCollegeOrAgentList/2';

      }

      return assignService.assignList(api, accessToken)
      .then(function(response){

        if($scope.select_entity_type == 'AGENT' ){

          $scope.agentcounsellorList = response;

        } else if ($scope.select_entity_type == 'COUNSELLOR') {

          $scope.agentcounsellorList = response.data;

        }

      })
      .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.getCounsellorDataInfo = function () {

      var postParams = {
        "lead_type": $scope.select_lead_type,
        "entity_type": $scope.select_entity_type,
        "entity_id": $scope.select_agent_counsellor
      };

      //console.log(postParams);

      var api = ENV.apiEndpoint + '/counselor/datainfo';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {


          var dataset = data;

          $scope.tableParamsdatainfo = new NgTableParams({}, {
            dataset: dataset
          });

        })
        .catch(function (error) {
          alert(error.message);
          $scope.error = {
            message: error.message
          };
        });

      };

    $scope.leadtransferDetail = function (transferData) {

      //console.log(transferData);
      //transferData.isEditable = false;

      var fromId = transferData.id ;
      var toId = transferData.newfacilitator;
      var entityId = transferData.id;

      if(transferData.entity_type == 'AGENT'){
        fromId = $scope.select_agent_counsellor;
      }

      if(transferData.entity_type == 'COUNSELLOR'){
        entityId = $scope.select_agent_counsellor;
      }

      var postParams = {
        "lead_type": transferData.lead_type,
        "entity_type": transferData.entity_type,
        "from": fromId,
        "to": toId,
        "entity_id" : entityId
      };

      //console.log(postParams);

      var api = ENV.apiEndpoint + '/counselor/leadtransferdetail';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {

          var dataset = data.data;
					$scope.tableParamsApplication = new NgTableParams({}, {
						counts: [10, 50, 100, 500, 1000],
						dataset: dataset
					});

        })
        .catch(function (error) {
          alert(error.message);
          $scope.error = {
            message: error.message
          };
        });

      };

      $scope.updateLeadFacilitator = function (transferData) {

        //console.log(transferData);
        transferData.isEditable = false;

        var fromId = transferData.id ;
        var toId = transferData.newfacilitator;
        var entityId = transferData.entity_id;

        if($scope.select_entity_type == 'AGENT'){
          fromId = $scope.select_agent_counsellor;
        }

        if($scope.select_entity_type == 'COUNSELLOR'){
          entityId = $scope.select_agent_counsellor;
        }

        var postParams = {
          "lead_type": $scope.select_lead_type,
          "lead_state": transferData.current_lead_state,
          "entity_type": $scope.select_entity_type,
          "from": fromId,
          "to": toId,
          "entity_id" : entityId
        };

        //console.log(postParams);

        var api = ENV.apiEndpoint + '/counselor/leadtransferupdate';

        return assignService.updateCommets(api, postParams, accessToken)
          .then(function (data) {

          })
          .catch(function (error) {
            alert(error.message);
            $scope.error = {
              message: error.message
            };
          });

        };

        $scope.selectEntity = function (appId, checkedvalue) {
    			if(checkedvalue){
    				//$scope.editData(appId, "MULTIPLE");
    				$scope.test.push(appId.login_id);
    			} else {
    				/*for(var i = 0; i < $scope.test.length; i++){
    					if($scope.test[i]==appId.counselor_id){
    						$scope.test.splice(i,1);
    						//appId.isEditable = false;
    					}
    				}*/
    			}

    		};

    		//$scope.isEditable = false;
    		/*$scope.editData = function (appId, selection) {
    			//appId.isEditable = true;
    			if(selection == 'MULTIPLE'){
    				if($scope.test.indexOf(appId.counselor_id) === -1){
    					$scope.test.push(appId.counselor_id);
    				}
    			}
    			else{
    				$scope.test = [];
    				$scope.test.push(appId.counselor_id);
    			}
    		};*/

        $scope.multipleTransfer = function(){

          console.log($scope.test,"ids");

          var rmIds = [];

          $scope.facilitator_email.forEach(function (frmids,indexcity) {
          rmIds.push(frmids['id']);
          });

          var postParams = {
            "login_ids" : $scope.test,
            "facilitator_id" : rmIds
          };

          var api = ENV.apiEndpoint + '/counselor/multipleTransfer';

          return assignService.updateCommets(api, postParams, accessToken)
            .then(function (data) {
              window.location.href = "/counselor/list";
            })
            .catch(function (error) {
              alert(error.message);
              $scope.error = {
                message: error.message
              };
            });


        };

    switch (filter) {

      case 'myassign-list':
        $scope.title = "My Assign Calling List ";
        $scope.myAssignList();
        break;

      case 'my-counselor-list':
        $scope.title = "Counselor List";
        $scope.counsellorsList();
        $scope.agentsList();
        break;

     case 'all-counselor-list':
          $scope.title = "All Counselor List";
          $scope.allcounsellorsList();
          break;

      case 'counselor-comments':
        $scope.title = "Counselor Comment";
        $scope.couselorCommentById('counselor');
        break;

      case 'counselor-calling-comments':
          $scope.title = "Counselor Calling Comment";
          $scope.couselorCommentById('callingcounselor');
          break;

      case 'create':
        $scope.title = "Create Counselor";
        $scope.getAllTitles();
        $scope.getAllCountries();
        break;

        case 'createCounselor':
          $scope.title = "Create Counselor";
          var counselorCallingId = $routeParams.id;
          $scope.getAllTitles();
          $scope.getAllCountries();
          $scope.counselorCallingInfo(counselorCallingId);
          break;

      case 'upload':
        $scope.title = "File Upload";
        $scope.agentsList();
        break;

      case 'edit':
        $scope.title = "Edit Counsellor";
        $scope.getAllTitles();
        $scope.getAllCountries();
        var counselorId = $routeParams.id;
        $scope.getPartnerData(counselorId);
        $scope.getCounselorDocuments(counselorId);
        break;

      case 'leads-transfer-counsellor':
        $scope.title = "Counsellor Transfer Lead";
        $scope.agentsList();
        break;

    };



    $scope.popMenu = function(rowData){
      $(".title-name").text(rowData.partner_name);
      $scope.partnerLoginId = rowData.login_id;
        var api = ENV.apiEndpoint + '/dashboardcontent/?pid=' + rowData.login_id + '&rid=12';
        return studentService.getData(api, accessToken)
        .then(function(data){
            $scope.partner_name = rowData.partner_name;
            $scope.total = 0;
            $scope.notInterested = 0;
            $scope.interested = 0;
            $scope.potential = 0;
            $scope.potentialDeclined = 0;
            $scope.applicationProcess = 0;
            $scope.applicationReview = 0;
            $scope.applicationReviewDeclined = 0;
            $scope.provisionalOffer = 0;
            $scope.disbursalDocumentation = 0;
            $scope.applicationWithdraw = 0;
            $scope.disbursed = 0;
            $scope.applicationRejected = 0;

            if(data.stages){
                data.stages.forEach(function(value){
                    switch(value['current_lead_state']){
                        case 'NOT_INTERESTED':
                            $scope.notInterested = parseInt(value['lead_count']);
                            break;
                        case 'INTERESTED':
                            $scope.interested = parseInt(value['lead_count']);
                            break;
                        case 'POTENTIAL':
                            $scope.potential = parseInt(value['lead_count']);
                            break;
                        case 'POTENTIAL_DECLINED':
                            $scope.potentialDeclined = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_PROCESS':
                            $scope.applicationProcess = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_REVIEW':
                            $scope.applicationReview = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_REVIEW_DECLINED':
                            $scope.applicationReviewDeclined = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_DECLINED':
                            $scope.applicationRejected = parseInt(value['lead_count']);
                            break;
                        case 'PROVISIONAL_OFFER':
                            $scope.provisionalOffer = parseInt(value['lead_count']);
                            break;
                        case 'DISBURSAL_DOCUMENTATION':
                            $scope.disbursalDocumentation = parseInt(value['lead_count']);
                            break;
                        case 'DISBURSED':
                            $scope.disbursed = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_WITHDRAWN':
                            $scope.applicationWithdraw = parseInt(value['lead_count']);
                            break;
                    }
                })
            }

            $scope.total = $scope.notInterested + $scope.interested + $scope.potential + $scope.potentialDeclined + $scope.applicationProcess + $scope.applicationReview + $scope.applicationReviewDeclined + $scope.provisionalOffer + $scope.disbursalDocumentation + $scope.applicationWithdraw + $scope.disbursed + $scope.applicationRejected;
            $("#basicModal").modal('show');
        })
    }

    $scope.viewFile = $route.current.$$route.pageName;
    $scope.location = $location.path();
    $scope.header = 'views/header.html';
    $scope.menu = 'views/menu.html';
    $scope.footer = 'views/footer.html';
    $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
    $scope.searchApi = ENV.apiEndpoint + 'cities/';
    $scope.getMenu();
    //$scope.getCounselorDocuments();
  }]);
