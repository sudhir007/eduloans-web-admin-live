'use strict';

angular.module('app')
	.controller('offerController', ['$scope', 'offerService', 'ENV', '$cookieStore', '$rootScope', '$routeParams', '$route', '$location', 'commonService', 'studentService', 'NgTableParams', function ($scope, offerService, ENV, $cookieStore, $rootScope, $routeParams, $route, $location, commonService, studentService, NgTableParams){
        var accessToken = $cookieStore.get("access_token");
		var partnerName = $cookieStore.get("partner_name");
        var leadId = $routeParams.lead_id;

        $scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

        $scope.offers = function(){
			$scope.tableParams = {};
			return offerService.userOffer(accessToken, leadId)
			.then(function(data){
				if(data.length){
					data.forEach(function(tableData, key){
						$scope.tableParams[tableData['name']] = {};
						if(tableData.Collateral.length){
							$scope.tableParams[tableData['name']]['Collateral'] = new NgTableParams({}, {dataset: tableData.Collateral});
						}
						if(tableData.NonCollateral.length){
							$scope.tableParams[tableData['name']]['Non Collateral'] = new NgTableParams({}, {dataset: tableData.NonCollateral});
						}
						if(tableData.USCosigner.length){
							$scope.tableParams[tableData['name']]['US Cosigner'] = new NgTableParams({}, {dataset: tableData.USCosigner});
						}
						if(tableData.NonCosignor.length){
              $scope.tableParams[tableData['name']]['Non Cosigner'] = new NgTableParams({}, {dataset: tableData.NonCosignor});
                        }
						if(tableData.forex_offer.length){
              $scope.tableParams[tableData['name']]['Forex Offer'] = new NgTableParams({}, {dataset: tableData.forex_offer});
						 }
					});
				}
				else{
					alert("There is no offer for selected student. Please check all the required details are filled.");
				}
				$scope.offersdata = data;
				$scope.getAppliedProducts(data[0].lead_id);
			})
			.catch(function(error){
	            if(error.error_code == 'SESSION_EXPIRED'){
	                $cookieStore.put("access_token", "");
	                window.location.href = "/";
	            }
			});
		}

		$scope.finishSignup = function(){
			window.location.href='student/' + leadId + '/product/0';
		}

		$scope.applyProduct = function(productId, universityId, courseId, loanType, leadId, countryId){
			var accessToken = $cookieStore.get("access_token");

			var postParams = {
				"lead_id": leadId,
				"product_id": productId,
				"university_id": universityId,
				"course_id": courseId,
				"loan_type": loanType,
				"country_id": countryId
			};

			return offerService.applyProduct(postParams, accessToken)
			.then(function(data){
				alert("Congratulation! You have successfully applied.");
				data.data.forEach(function(value){
					$scope.appliedProduct.push(value.product_id + '|' + value.university_id + '|' + value.course_id + '|' + value.country_id + '|' + value.loan_type);
				});
			})
			.catch(function(error){
				$scope.error = {
					message: error.message
				};
				$scope.nextButtonDisabled = true;
			});

		};

		$scope.applyForexProduct = function(forexProductId, leadId, forexConversionAmount, universityId, courseId, countryId){
			var accessToken = $cookieStore.get("access_token");

			var postParams = {
				"lead_id": leadId,
				"forex_id": forexProductId,
				"forex_conversion_amount": forexConversionAmount,
				"university_id": universityId,
				"course_id": courseId,
				"country_id": countryId
			};

			return offerService.applyForexProduct(postParams, accessToken)
			.then(function(data){
				alert("Congratulation! You have successfully applied.");
				data.data.forEach(function(value){
					$scope.appliedForexProduct.push(value.forex_id);
				});
			})
			.catch(function(error){
				$scope.error = {
					message: error.message
				};
				$scope.nextButtonDisabled = true;
			});

		};

		$scope.appliedProduct = [];
		$scope.appliedForexProduct = [];
		$scope.getAppliedProducts = function(leadId){
			return studentService.getAppliedProducts(accessToken, leadId)
			.then(function(response){
				response.applied_products.forEach(function(value){
					$scope.appliedProduct.push(value.product_id + '|' + value.university_id + '|' + value.course_id + '|' + value.country_id + '|' + value.loan_type);
				});

				response.applied_forex.forEach(function(value){
					$scope.appliedForexProduct.push(value.forex_id);
				});

			});
		};

		$scope.offers();

        $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();
}]);
