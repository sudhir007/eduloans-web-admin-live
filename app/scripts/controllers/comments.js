'use strict';

angular.module('app')
.filter('num', function() {
    return function(input) {
      return parseInt(input, 10);
    };
})
.controller('commentsController', ['$scope', 'commentsService', 'ENV', '$cookieStore',  '$route',  'menuService', 'NgTableParams', '$rootScope', '$location', '$routeParams', function ($scope, commentsService, ENV, $cookieStore,  $route, menuService, NgTableParams,  $rootScope, $location, $routeParams){
		//angular.element(document.body).addClass("login-page");
		if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
		}
        $scope.loanApplicationId = $routeParams.loanappid;
        $scope.forexApplicationId = $routeParams.forexappid;
        $scope.product_type_name = '';
        $scope.forex_type_name = '';
        $scope.product_type = [];
        $scope.forex_type = [];
        $scope.productNameSetting = { displayProp: 'display_name', idProperty:'product_id' };
        $scope.forexNameSetting = { displayProp: 'display_name', idProperty:'forex_id' };
        $scope.product_type_id = '';
        $scope.forex_type_id = '';
        $scope.product_id = '';

        var accessToken = $cookieStore.get("access_token");
        var filter = $route.current.$$route.filter;

        $scope.$back = function() {
            window.history.back();
        };

  		$scope.claculateTime = function(dt) {
  	         return new Date(dt).getTime();
  	    };

        $scope.getMenu = function(){
			var api = ENV.apiEndpoint + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
            .catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

        $scope.productsList = function(){

    		var api = ENV.apiEndpoint + '/productList/1';

    		return commentsService.productsList(api, accessToken)
    		.then(function(data){
    			$scope.productslist = data;

          //console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

        $scope.appliedProductsList = function(leadId){

    		var api = ENV.apiEndpoint + '/appliedProductList/'+leadId;

    		return commentsService.productsList(api, accessToken)
    		.then(function(data){
    			$scope.appliedproductslist = data;

                if(data){
                    $scope.product_id = data.data[0]['product_id'];
                }

                if($scope.loanApplicationId){
                    data.data.forEach(function(value){
                        if(value.id == $scope.loanApplicationId){
                            $scope.product_type_id = value.product_id;
                            $scope.product_type_name = value.display_name;
                            $scope.product_id = value.product_id;
                        }
                    })
                }

                console.log($scope.loanApplicationId, $scope.product_type_name, "loan");

                if($scope.product_id){
                    $scope.getBasicDetailCompleteness($scope.product_id);
                }

          //console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.appliedForexList = function(leadId){

    var api = ENV.apiEndpoint + '/appliedForexList/'+leadId;

    return commentsService.productsList(api, accessToken)
    .then(function(data){
      $scope.appliedforexlist = data;

            if($scope.forexApplicationId){
                data.data.forEach(function(value){
                    if(value.id == $scope.forexApplicationId){
                        $scope.forex_type_id = value.forex_id;
                        $scope.forex_type_name = value.display_name;
                    }
                })
            }

            console.log($scope.appliedforexlist,$scope.forexApplicationId, $scope.forex_type_name, "forex");

    })
    .catch(function(error){
            $scope.error = {
                message: error.message
            };
    });
};

    $scope.leadCommentsById = function(leadId, loanAppId, forexAppId){
      //alert(callId);
      $('#loaderAjax').show();

      $scope.commentslist = "";

      var api = ENV.apiEndpoint + '/leadcomments/'+leadId+'/'+loanAppId+'/'+forexAppId;

      return commentsService.commentsById(api, accessToken)
      .then(function(data){
        $('#loaderAjax').hide();
        $scope.commentslist = data.data;
        $scope.userInfo = data.user_data;

        var dataset = data.data;

        $scope.tableParams = new NgTableParams({}, {dataset: dataset});

        //console.log(commentslist,callingIdComments,"hellloooooooo");
        //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

      })
      .catch(function(error){
        $('#loaderAjax').hide();
          //console.log(error)
              $scope.error = {
                  message: error.message
              };
      });
    };

    $scope.callingCommentsById = function(leadId){
      //alert(callId);

      $('#loaderAjax').show();

      $scope.commentslist = "";

      var api = ENV.apiEndpoint + '/callingcomments/'+leadId;

      return commentsService.commentsById(api, accessToken)
      .then(function(data){
        $('#loaderAjax').hide();
        $scope.commentslist = data.data;
        $scope.userInfo = data.user_data;

        var dataset = data.data;

        $scope.tableParams = new NgTableParams({}, {dataset: dataset});

        //console.log(commentslist,callingIdComments,"hellloooooooo");
        //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

      })
      .catch(function(error){
        $('#loaderAjax').hide();
              $scope.error = {
                  message: error.message
              };
      });
    };

    $scope.dateformat = function(D){
			 var pad = function(num) { var s = '0' + num; return s.substr(s.length - 2); }
			 var Result = D.getFullYear() + '-' + pad((D.getMonth() + 1)) + '-' + pad(D.getDate());
			 return Result; };

       $scope.updateCallingComment = function(){

         $('#loaderAjax').show();

         var callId = $routeParams.id;
         var commentsTime = new Date();
             commentsTime = Date.parse(commentsTime);

             var dateString = $scope.next_FollowUp_Date ? $scope.dateformat($scope.next_FollowUp_Date) : null;
           //  console.log(dateString);

           if(!($scope.time_value)){

             alert("Time Spent Not Given !");
             return ;

           }

           if(!($scope.lead_state_calling) ){

             alert("Lead State Not Given !");
             return ;

           }

           if(!(dateString) ){

             if( $scope.lead_state_calling == 'NOTINTERESTED' ){
               console.log("else");

                }
                else{
                  alert("Next Followup Date Not Given !");
                  return ;
                }

           }
           $scope.product_type = [];
           $scope.product_type.push($scope.product_type_id);
         var productTypeCount = $scope.product_type.length;
         if(productTypeCount > 0){
             //$scope.time_value = parseInt($scope.time_value / productTypeCount);
             $scope.time_value = $scope.time_value / productTypeCount;
         }
         var postParams = {
             "entity_id" : callId,
             "comment_type" : $scope.comment_type,
             "product_type" : $scope.product_type,
             "time_spent" : $scope.time_value,
             "comment" : $scope.comments_value,
             "leadstate" : $scope.lead_state_calling,
             "last_followup_date" : 't'+commentsTime,
             "next_followup_date" : dateString
         };

         //console.log(postParams);

         var api = ENV.apiEndpoint + '/callingcomments/add';

         return commentsService.updateCommets(api, postParams, accessToken, $scope.appliedproductslist)
         .then(function(data){
           $('#loaderAjax').hide();
           //$scope.commentslist = data;
           window.location.href = '/comments/calling/'+callId;
           //console.log(data,"hellloooooooo");
           $('#comments_value').val('');
           $('#time_value').val('');
           $('#next_FollowUp_Date').val('');
           //$('#modal-default').modal('hide');
           //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

         })
         .catch(function(error){
           $('#loaderAjax').hide();
                 $scope.error = {
                     message: error.message
                 };
         });

       };

       $scope.updateLeadComment = function(){

         $('#loaderAjax').show();

         var product_id = '';
         var leadId = $routeParams.id;
         var appId = $routeParams.loanappid;
         var forexAppId = $routeParams.forexappid;

         var dateString = $scope.next_FollowUp_Date ? $scope.dateformat($scope.next_FollowUp_Date) : null;

         if(!($scope.time_value)){

           alert("Time Spent Not Given !");
           return ;

         }

         if(!(dateString)){

           if( $scope.lead_state == 'NOT_INTERESTED' || $scope.lead_state == 'REJECTED' || $scope.lead_state == 'POTENTIAL_DECLINED' || $scope.lead_state == 'WITHDRAWN' || $scope.lead_state == 'APPLICATION_WITHDRAW' || $scope.lead_state == 'APPLICATION_REJECTED'
           || $scope.lead_state == 'PROVISIONAL_OFFER_NOT_ACCEPTED' || $scope.lead_state == 'APPLICATION_REVIEW_DECLINED' || $scope.lead_state == 'DISBURSED' || $scope.lead_state == 'BEYOND_INTAKE' ) {
             console.log("else");

              }
              else{
                alert("Next Followup Date Not Given !");
                return ;
              }
         }

         if(($scope.lead_state) && $scope.lead_state != 'NOT_INTERESTED'){

           if(!($scope.product_type)){

             alert("Application (Product Name)  Not Given !");
             return ;
           }

         }

         if(!$scope.product_type.length){
             $scope.product_type.push($scope.product_type_id);

         }
         var productTypeCount = $scope.product_type.length;
         if(productTypeCount > 0){
//             $scope.time_value = parseInt($scope.time_value / productTypeCount);
                $scope.time_value = $scope.time_value / productTypeCount;
         }

         var postParams = {
             "entity_id" : leadId,
             "application_id" : appId,
             "comment_type" : $scope.comment_type,
             "product_type" : $scope.product_type,
             "time_spent" : $scope.time_value,
             "comment" : $scope.comments_value,
             "leadstate" : $scope.lead_state,
             "decline_reason" : $scope.reason,
             "decline_reason_other" : $scope.reasontext,
             "next_followup_date" : dateString,
             "customer_name" : $scope.userInfo[0].customerName,
             "comment_for" : "EDUCATION_LOAN"
         };


         if($scope.lead_state == 'DISBURSED' && $scope.basicDetailCompleted < 100){
             alert("Please fill all the basic detail first before move to disbursed");
             return;
         }

         var api = ENV.apiEndpoint + '/leadcomments/add';

         return commentsService.updateCommets(api, postParams, accessToken, $scope.appliedproductslist)
         .then(function(data){
           $('#loaderAjax').hide();
           //$scope.commentslist = data;
           window.location.href = '/comments/lead/'+leadId+'/'+appId+'/'+forexAppId;
           //console.log(data,"hellloooooooo");
           $('#comments_value').val('');
           $('#time_value').val('');
           $('#next_FollowUp_Date').val('');
           //$('#modal-default').modal('hide');
           //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

         })
         .catch(function(error){
           $('#loaderAjax').hide();
                 $scope.error = {
                     message: error.message
                 };
         });

       };

       $scope.nextFollowupDateCount = function(){

         var dateString = $scope.next_FollowUp_Date ? $scope.dateformat($scope.next_FollowUp_Date) : null;

         var postParams = {
             "entity_type" : "lead",
             "next_followup_date" : dateString
         };

         var apiLink = '/nextfollowupdate/count';

         return commentsService.getDataByPost(apiLink, accessToken, postParams)
         .then(function(data){
           $('#loaderAjax').hide();

           console.log(data);

           var count = data.follow_up_count;

           alert("You Have Follow-Up count on this date - " + dateString + " Count - "+ count);

         })
         .catch(function(error){
           $('#loaderAjax').hide();
                 $scope.error = {
                     message: error.message
                 };
         });

       };

       $scope.updateForexComment = function(){

         $('#loaderAjax').show();

         var leadId = $routeParams.id;
         var appId = $routeParams.loanappid;
         var forexAppId = $routeParams.forexappid;

         var dateString = $scope.forex_next_FollowUp_Date ? $scope.dateformat($scope.forex_next_FollowUp_Date) : null;

         if(!($scope.forex_time_value)){

           alert("Time Spent Not Given !");
           return ;

         }

         if(!(dateString)){

           if( $scope.forex_lead_state == 'NOT_INTERESTED' || $scope.forex_lead_state == 'REJECTED' || $scope.forex_lead_state == 'POTENTIAL_DECLINED' || $scope.forex_lead_state == 'WITHDRAWN' || $scope.forex_lead_state == 'APPLICATION_WITHDRAW' || $scope.forex_lead_state == 'APPLICATION_REJECTED'
           || $scope.forex_lead_state == 'PROVISIONAL_OFFER_NOT_ACCEPTED' || $scope.forex_lead_state == 'APPLICATION_REVIEW_DECLINED' || $scope.forex_lead_state == 'DISBURSED' || $scope.forex_lead_state == 'BEYOND_INTAKE' ) {
             console.log("else");

              }
              else{
                alert("Next Followup Date Not Given !");
                return ;
              }
         }

         if(($scope.forex_lead_state) && $scope.forex_lead_state != 'NOT_INTERESTED'){

           if(!($scope.forex_type)){

             alert("Application (Forex Name)  Not Given !");
             return ;
           }

         }

         if(!$scope.forex_type.length){
             $scope.forex_type.push($scope.forex_type_id);
         }
         var forexTypeCount = $scope.forex_type_id.length;
         if(forexTypeCount > 0){
//             $scope.time_value = parseInt($scope.time_value / productTypeCount);
                $scope.forex_time_value = $scope.forex_time_value / forexTypeCount;
         }

         var postParams = {
             "entity_id" : leadId,
             "application_id" : forexAppId,
             "comment_type" : $scope.forex_comment_type,
             "product_type" : $scope.forex_type,
             "time_spent" : $scope.forex_time_value,
             "comment" : $scope.forex_comments_value,
             "leadstate" : $scope.forex_lead_state,
             "decline_reason" : $scope.forex_reason,
             "decline_reason_other" : $scope.forex_reasontext,
             "next_followup_date" : dateString,
             "customer_name" : $scope.userInfo[0].customerName,
             "comment_for" : "FOREX"
         };

         console.log(postParams);

         var api = ENV.apiEndpoint + '/leadcomments/add';

         return commentsService.updateForexCommets(api, postParams, accessToken, $scope.appliedforexlist)
         .then(function(data){
           $('#loaderAjax').hide();
           //$scope.commentslist = data;
           window.location.href = '/comments/lead/'+leadId+'/'+appId+'/'+forexAppId;
           //console.log(data,"hellloooooooo");
           $('#comments_value').val('');
           $('#time_value').val('');
           $('#next_FollowUp_Date').val('');
           //$('#modal-default').modal('hide');
           //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

         })
         .catch(function(error){
           $('#loaderAjax').hide();
                 $scope.error = {
                     message: error.message
                 };
         });

       };

       $scope.updateLeadState = function(){

            if(document.getElementById('lead_state').value == "APPLICATION_WITHDRAW")
            {
              document.getElementById("reason").disabled = false;

              $scope.allStatusDeclineReason = [
                {
                    id: "SELF_FINANCED",
                    status: "Self Financed"
                },
              {
                  id: "GOT_LOAN_FROM_OTHER_BANK",
                  status: "Got loan from Other Bank"
              },
              {
                  id: "NOTINTERESTED_IN_STUDYING_ABROAD",
                  status: "Not interested in studying abroad"
              },
              {
                  id:"APPLICATION_PROCESS_TOO_LONG",
                  status: "Application process too long"
              },
              {
                  id:"DO_NOT_HAVE_RELEVANT_DOCUMENTS",
                  status: "Do not have relevant documents"
              },
              {
                  id:"POSTPONED_PLAN_TO_NEXT_INTAKE",
                  status: "Postponed plan to next intake"
              },
              {
                  id:"NOT_FOLLOWED_UP_OR_DELAYED",
                  status: "Not followed up / Delayed"
              },
              {
                  id:"OTHER",
                  status: "OTHER"
              }

            ];
          } else if(document.getElementById('lead_state').value == "APPLICATION_REVIEW_DECLINED")
            {
              document.getElementById("reason").disabled = false;
              $scope.allStatusDeclineReason = [
                {
                    id: "SELF_FINANCED",
                    status: "Self Financed"
                },
              {
                  id: "NOT_AGREEING_WITH_TERMS_AND_CONDITIONS",
                  status: "Not agreeing with terms and Conditions"
              },
              {
                  id: "NOT_HAVING_COMPLETE_DOCUMENTS",
                  status: "Not having complete documents"
              },
              {
                  id:"LEGAL_VERIFICATION_NOT_SUCCESSFUL",
                  status: "Legal verification not successful"
              },
              {
                  id:"PROPERTY_VALUATION_NOT_SUCCESSFUL",
                  status: "Property valuation not successful"
              },
              {
                  id:"COLLEGE_NOT_ACCREDITED",
                  status: "College not accredited"
              },
              {
                  id:"COURSE_NOT_ACCREDITED",
                  status: "Course not accreditied"
              },
              {
                  id:"CIBIL_RATING_LOW_OF_GUARANTOR",
                  status: "Cibil rating low of guarantor"
              },
              {
                  id:"OTHER",
                  status: "OTHER"
              }

            ];
          } else if(document.getElementById('lead_state').value == "PROVISIONAL_OFFER_NOT_ACCEPTED")
            {
              document.getElementById("reason").disabled = false;
              $scope.allStatusDeclineReason = [
                {
                    id: "SELF_FINANCED",
                    status: "Self Financed"
                },
              {
                  id: "CHANGE_IN_COLLEGE",
                  status: "Change in college"
              },
              {
                  id: "BETTER_TERMS_FROM_OTHER_BANK",
                  status: "Better terms from other bank"
              },
              {
                  id:"HIGHER_INTEREST_RATE",
                  status: "Higher Interest rate"
              },
              {
                  id:"HIGHER_VALUE_OF_COLLATERAL_REQUIREMENT",
                  status: "Higher value of collateral requirement"
              },
              {
                  id:"HIGHER_VALUE_OF_MARGIN_MONEY",
                  status: "Higher value of Margin Money"
              },
              {
                  id:"DELAY_IN_PROCESSING",
                  status: "Delay in processing"
              },
              {
                  id:"CHANGE_IN_PLAN",
                  status: "Change in plan"
              },
              {
                  id:"OTHER",
                  status: "OTHER"
              }

            ];
          } else if(document.getElementById('lead_state').value == "NOT_INTERESTED"){
            document.getElementById("reason").disabled = false;
            $scope.allStatusDeclineReason = [
              {
                  id: "SELF_FINANCED",
                  status: "Self Financed"
              },
            {
                id: "STUDENT_NOT_RESPONDING",
                status: "Student not responding"
            },
            {
                id: "STUDENT_NOT_DOABLE",
                status: "Student Not Doable"
            },
            {
                id:"TEST_ENQUIRY",
                status: "Test Enquiry"
            },
            {
                id:"STUDENT_ALREADY_GOT_LOAN",
                status: "Student Already Got Loan"
            },
            {
                id:"CHANGE_IN_PLAN",
                status: "Change in plan"
            },
            {
                id:"OTHER",
                status: "OTHER"
            }

          ];
        } else if(document.getElementById('lead_state').value == "POTENTIAL_DECLINED"){
          document.getElementById("reason").disabled = false;
          $scope.allStatusDeclineReason = [
            {
                id: "SELF_FINANCED",
                status: "Self Financed"
            },
          {
              id: "GOT_LOAN_FROM_OTHER_BANK",
              status: "Got Loan From Other Bank"
          },
          {
              id: "NOT_INTERESTED_IN_STUDYING_ABROAD",
              status: "Not interested in studying abroad"
          },
          {
              id:"GOT_BETTER_OFFER_FROM_OTHER_SOURCES",
              status: "Got Better Offer From Other Sources"
          },
          {
              id:"DO_NOT_HAVE_RELEVANT_DOCUMENTS",
              status: "Do not have relevant documents"
          },
          {
              id:"POSTPONED_PLAN_TO_NEXT_INTAKE",
              status: "Postponed plan to next intake"
          },
          {
              id:"NOT_FOLLOWED_UP_DELAYED_BY_BANK_RM",
              status: "Not followed up/Delayed by Bank RM"
          },
          {
              id:"LOAN_NOT_SUITABLE_WITH_TERMS_AND_CONDITIONS",
              status: "Loan not Suitable with terms and Conditions"
          },
          {
              id:"OC_CC_NOT_AVAILABLE",
              status: "OC/CC  Not available"
          },
          {
              id:"GUARANTOR_PROFILE_WEAK_IT_RETURNS",
              status: "Guarantor profile weak (IT returns)"
          },
          {
              id:"COURSE_NOT_ACCREDITED",
              status: "Course not accredited"
          },
          {
              id:"CIBIL_RATING_LOW_OF_GUARANTOR",
              status: "Cibil rating low of guarantor"
          },
          {
              id:"OTHER",
              status: "OTHER"
          }

        ];
      } else if(document.getElementById('lead_state').value == "DISBURSAL_DOCUMENTATION_WITHDRAWN"){
          document.getElementById("reason").disabled = false;
          $scope.allStatusDeclineReason = [
            {
                id: "SELF_FINANCED",
                status: "Self Financed"
            },
          {
              id: "DOCUMENTS_NOT_FOUND",
              status: "Documents not found"
          },
          {
              id: "VERY_LENGTHY",
              status: "Very Lengthy"
          },
          {
              id:"LEGAL_VERIFICATION_NOT_SUCCESSFUL",
              status: "Legal verification not successful"
          },
          {
              id:"PROPERTY_VALUATION_NOT_SUCCESSFUL",
              status: "Property valuation not successful"
          },
          {
              id:"OTHER",
              status: "OTHER"
          }

        ];
      }
      else if(document.getElementById('lead_state').value == "NOT_TAKEN_DISBURSEMENT"){
          document.getElementById("reason").disabled = false;
          $scope.allStatusDeclineReason = [
          {
              id: "GOT_SCHOLARSHIP",
              status: "Got scholarship"
          },
          {
              id: "SELF_FINANCED",
              status: "Self Financed"
          },
          {
              id:"CHANGE_OF_PLAN",
              status: "Change of plan"
          },
          {
              id:"OTHER",
              status: "OTHER"
          }

        ];
      }
      else {

          // document.getElementById("reason").val = "";
          document.getElementById("reason").disabled = true;
          document.getElementById("reasontext").disabled = true;
        }
      }


        $scope.reasonChanged = function(){
            // alert($('#reason :selected').text());
            if(document.getElementById('reason').value == "OTHER")
            {
              //alert("other clicked");
              //$("#reasonTextBox").css("display", "block");
              document.getElementById("reasontext").disabled = false;

            }else{
              // document.getElementById("reasontext").val = "";
              // $('.reasontext').attr("value", "");
              document.getElementById("reasontext").disabled = true;

            }

       };

       $scope.allForexStatus = [

         {
    				id: "INTERESTED",
    				status: "Interested"
    			},
    			{
    				id: "NOT_INTERESTED",
    				status: "Not Interested"
    			},
    			{
    				id: "POTENTIAL",
    				status: "Potential"
    			},
    			{
    				id: "POTENTIAL_DECLINED",
    				status: "Potential Declined"
    			},
    			{
    				id: "APPLICATION_PROCESS",
    				status: "Application Process"
    			},
    			{
    				id: "APPLICATION_REVIEW",
    				status: "Application Review"
    			},
    			{
    				id: "APPLICATION_REVIEW_DECLINED",
    				status: "Application Review Declined"
    			},
    			{
    				id: "PROVISIONAL_OFFER",
    				status: "Provisional Offer"
    			},
    			{
    				id: "DISBURSED",
    				status: "Disbursed"
    			},
          {
     				id: "BEYOND_INTAKE",
     				status: "Beyond Intake"
     			}

       ];

       $scope.allStatus = [
   			{
   				id: "INTERESTED",
   				status: "Interested"
   			},
   			{
   				id: "NOT_INTERESTED",
   				status: "Not Interested"
   			},
   			{
   				id: "POTENTIAL",
   				status: "Potential"
   			},
   			{
   				id: "POTENTIAL_DECLINED",
   				status: "Potential Declined"
   			},
   			{
   				id: "APPLICATION_PROCESS",
   				status: "Application Process"
   			},
        {
          id: "APPLICATION_WITHDRAW",
          status: "Application Withdraw"
        },
   			{
   				id: "APPLICATION_REVIEW",
   				status: "Application Review"
   			},
   			{
   				id: "APPLICATION_REVIEW_DECLINED",
   				status: "Application Review Declined"
   			},
   			/*{
   				id: "INITIAL_OFFER"	,
   				status: "Initial Offer"
   			},*/
   			{
   				id: "PROVISIONAL_OFFER",
   				status: "Provisional Offer"
   			},
        {
          id: "PROVISIONAL_OFFER_NOT_ACCEPTED",
          status: "Provisional Offer Not Accepted"
        },
   			{
   				id: "DISBURSAL_DOCUMENTATION",
   				status: "Disbursal Documentation"
   			},
        {
          id: "DISBURSAL_DOCUMENTATION_WITHDRAWN",
          status: "Disbursal Documentation Withdrawn"
        },
   			{
   				id: "DISBURSED",
   				status: "Disbursed"
   			},

        {
  				id: "PARTIALLY_DISBURSED",
  				status: "Partially Disbursed"
  			 },
         {
          id: "NOT_TAKEN_DISBURSEMENT",
          status: "Not Taken Disbursement"
        },
   			// {
   			// 	id: "WITHDRAWN"	,
   			// 	status: "Withdrawn"
   			// },
   			// {
   			// 	id: "REJECTED"	,
   			// 	status: "Rejected"
   			// },
   			{
   				id: "BEYOND_INTAKE",
   				status: "Beyond Intake"
   			}
   		];

      $scope.allCallStatus = [
        {
            id: "FRESH",
            status: "FRESH"
        },
          {
              id: "NOTINTERESTED",
              status: "NOTINTERESTED"
          },
          {
              id:"INTERESTED",
              status: "INTERESTED"
          },
          {
              id:"CALLBACK",
              status: "CALLBACK"
          },
          {
              id:"BEYONDINTAKE",
              status: "BEYONDINTAKE"
          },
          {
              id:"RINGING",
              status: "RINGING"
          },
          {
              id:"LEADCONVERTED",
              status: "LEADCONVERTED"
          }

      ];

      $scope.commentTypeStatus = [
        {
            id: "EDUCATION_LOAN",
            status: "EDUCATION_LOAN"
        },
          {
              id: "FOREX",
              status: "FOREX"
          }

      ];



       $scope.toggleMenu = function(id){
   			$('#menu-'+id).toggle();
   		};
   		$scope.toggleSubMenu = function(id){
   			$('#submenu-'+id).toggle();
   		};

       switch(filter){

       case 'calling-comments':
             $scope.title = "Comments";
             var callId = $routeParams.id;
             $scope.callingCommentsById(callId);
             $scope.productsList();
             break;

       case 'lead-comments':
             $scope.title = "Comments";
             var leadId = $routeParams.id;
             $scope.leadCommentsById(leadId, "0", "0");
             $scope.appliedProductsList(leadId);
             $scope.appliedForexList(leadId);
             break;

      case 'lead-app-comments':
            $scope.title = "Comments";
            var leadId = $routeParams.id;
            var loanAppId = $routeParams.loanappid;
            var forexAppId = $routeParams.forexappid;
            $scope.leadCommentsById(leadId, loanAppId, forexAppId);
            $scope.appliedProductsList(leadId);
            $scope.appliedForexList(leadId);
            break;
       };

       $scope.basicDetailCompleted = 0;

       $scope.getBasicDetailCompleteness = function (productId){
           var leadId = $routeParams.id;
           return commentsService.getBasicDetailCompleteness(accessToken, leadId, productId)
           .then(function(response){
               var profileCompleteness = response.profile_completeness.slice(0, -1);
               $scope.basicDetailCompleted = profileCompleteness;
           })
           .catch(function(err){
               console.log(err);
           })
       }

       //$scope.title = "My Assign Calling List";
      //$scope.updateLeadState();
      $scope.viewFile = $route.current.$$route.pageName;
   		$scope.location = $location.path();
   		$scope.header = 'views/header.html';
   		$scope.menu = 'views/menu.html';
   		$scope.footer = 'views/footer.html';
   		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
   		$scope.getMenu();


       //$scope.commentsById();

   }])

   .filter('strip', function(){
       return function(str) {
         var finaldate = str.substring(1, str.length);
         finaldate = Number(finaldate)
         var fdate = new Date(finaldate);
         var pdate = fdate.toString('yyyy-MM-dd HH:mm:ss');
             //finaldate = Date.parse(finaldate);
                           console.log(finaldate,fdate,pdate);
         return pdate ;
       };
     });
