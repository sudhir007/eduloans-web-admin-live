'use strict';

angular.module('app')
    .controller('approvalController',  ['$scope', '$location', '$rootScope', 'commonService', '$cookieStore', 'menuService', '$route', 'approvalService', 'ENV', '$routeParams', 'NgTableParams', function ($scope, $location, $rootScope, commonService, $cookieStore, menuService, $route, approvalService, ENV, $routeParams, NgTableParams){

        var accessToken = $cookieStore.get("access_token");
        var partnerName = $cookieStore.get("partner_name");

        $scope.university = true;
        $scope.option = "";

        $scope.map_id = [];
    		$scope.mapSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true };

        var filter = $route.current.$$route.filter;

    $scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
        };

        $scope.getApprovals = function(){
            return approvalService.getApprovals(accessToken)
            .then(function(response){
                $scope.universities = new NgTableParams({}, {dataset: response.universities_approvals});
                $scope.courses = new NgTableParams({}, {dataset: response.courses_approvals});
            })
        }

        $scope.toggleData = function(type){
            $scope.university = true;
            if(type == 'course'){
                $scope.university = false;
            }
        }

        $scope.approve = function(approvalId, type){
            var postData = {
                "type": type,
                "status": 1
            }
            return approvalService.updateApproval(approvalId, postData, accessToken)
            .then(function(response){
                alert("Success");
                $scope.getApprovals();
            })
            .catch(function(error){
                alert("Some error occured");
                console.log(error);
            })
        };

        $scope.reject = function(approvalId, type){
            var postData = {
                "type": type,
                "status": -1
            }
            return approvalService.updateApproval(approvalId, postData, accessToken)
            .then(function(response){
                alert("Success");
                $scope.getApprovals();
            })
            .catch(function(error){
                alert("Some error occured");
                console.log(error);
            })
        };

        $scope.getUniversityApprovalsInfoById = function(universityApprovalId){

            var apiEndpoint = "/university/approvalinfo/"+universityApprovalId;

            return approvalService.getdatabyget(apiEndpoint, accessToken)
            .then(function(response){

              $scope.university_name = response.approval_info[0].university_name;
              $scope.country_name = response.approval_info[0].country_name;
              $scope.list_id = response.approval_info[0].list_id;

              $scope.university_list = response.list_data;

            })
            .catch(function(error){

                alert("Some error occured");
                console.log(error);

            })
        };

        $scope.postUniversityApprovalSubmit = function(updateId){

          $('#loaderAjax').show();
          var approvalId = $routeParams.id;

            var postParams = {
              "university_name" : $scope.university_name,
              "country_name" : $scope.country_name,
              "list_id" : $scope.list_id,
              "map_id" : $scope.map_id,
              "approval_id" : approvalId,
              "option" : $scope.option
            };
            var apiEndpoint = "/university/approvalsubmit";

            return approvalService.getdatabypost(apiEndpoint, postParams, accessToken)
            .then(function(response){

              console.log(response);
              alert("University Successfully Updated. Now You can close this window. Thanks");
              $('#loaderAjax').hide();

            })
            .catch(function(error){
                $('#loaderAjax').hide();
                alert("Some error occured");
                console.log(error);

            })
        };

        $scope.getCourseApprovalsInfoById = function(courseApprovalId){

            var apiEndpoint = "/course/approvalinfo/"+courseApprovalId;

            return approvalService.getdatabyget(apiEndpoint, accessToken)
            .then(function(response){

              $scope.course_name = response.approval_info[0].course_name;

              $scope.course_list = response.list_data;

            })
            .catch(function(error){

                alert("Some error occured");
                console.log(error);

            })
        };

        $scope.postCourseApprovalSubmit = function(updateId){

          $('#loaderAjax').show();

          var approvalId = $routeParams.id;

            var postParams = {
              "course_name" : $scope.course_name,
              "map_id" : $scope.map_id,
              "approval_id" : approvalId,
              "option" : $scope.option
            };

            var apiEndpoint = "/course/approvalsubmit";

            return approvalService.getdatabypost(apiEndpoint, postParams, accessToken)
            .then(function(response){

              console.log(response);
              alert("SUCCESS");

              $('#loaderAjax').hide();

            })
            .catch(function(error){

              $('#loaderAjax').hide();

                alert("Some error occured");
                console.log(error);

            })
        };

        //approved university & courses
        $scope.getUniversityApprovedInfoById = function(universityApprovalId){

            var apiEndpoint = "/university/approvedinfo/"+universityApprovalId;

            return approvalService.getdatabyget(apiEndpoint, accessToken)
            .then(function(response){

              $scope.university_name = response.approval_info[0].university_name;
              $scope.country_name = response.approval_info[0].country_name;
              $scope.list_id = response.approval_info[0].list_id;
              $scope.university_id = response.approval_info[0].id;

              $scope.university_list = response.list_data;

            })
            .catch(function(error){

                alert("Some error occured");
                console.log(error);

            })
        };

        $scope.postUniversityApprovedSubmit = function(updateId){

          $('#loaderAjax').show();
          var associatedId = $routeParams.id;

            var postParams = {
              "list_id" : $scope.list_id,
              "map_id" : $scope.map_id,
              "university_id" : $scope.university_id,
              "associated_id" : associatedId
            };

            var apiEndpoint = "/university/approvedsubmit";

            return approvalService.getdatabypost(apiEndpoint, postParams, accessToken)
            .then(function(response){

              console.log(response);
              alert("SUCCESS");
              $('#loaderAjax').hide();

            })
            .catch(function(error){
                $('#loaderAjax').hide();
                alert("Some error occured");
                console.log(error);

            })
        };

        $scope.getCourseApprovedInfoById = function(courseApprovalId){

            var apiEndpoint = "/course/approvedinfo/"+courseApprovalId;

            return approvalService.getdatabyget(apiEndpoint, accessToken)
            .then(function(response){

              $scope.course_name = response.approval_info[0].course_name;

              $scope.course_list = response.list_data;

            })
            .catch(function(error){

                alert("Some error occured");
                console.log(error);

            })
        };

        $scope.postCourseApprovedSubmit = function(updateId){

          $('#loaderAjax').show();

          var courseId = $routeParams.id;
          var associatedId = $routeParams.aid;

            var postParams = {
              "list_id" : $scope.list_id,
              "map_id" : $scope.map_id,
              "course_id" : courseId,
              "associated_id" : associatedId
            };

            var apiEndpoint = "/course/approvedsubmit";

            return approvalService.getdatabypost(apiEndpoint, postParams, accessToken)
            .then(function(response){

              console.log(response);
              alert("SUCCESS");

              $('#loaderAjax').hide();

            })
            .catch(function(error){

              $('#loaderAjax').hide();

                alert("Some error occured");
                console.log(error);

            })
        };

        $scope.mapList = function(){
          if($scope.option == "MAP"){
            $('#mapDiv').show();
            //alert("display");
          } else {
            $('#mapDiv').hide();
            //alert("don't display");
          }
        };

        // category Code

        $scope.getCategoryCourseList = function(){

            var apiEndpoint = "/course/category";

            return approvalService.getdatabyget(apiEndpoint, accessToken)
            .then(function(response){

              $scope.category_course_list = response.category_course_list;

              $scope.course_list = response.course_list;

            })
            .catch(function(error){

                alert("Some error occured");
                console.log(error);

            })
        };


        $scope.getcourseSubCategoryList = function(){

            var categoryId = $scope.category_id;
            var apiEndpoint = "/course/subcategory/"+categoryId;

            return approvalService.getdatabyget(apiEndpoint, accessToken)
            .then(function(response){

              $scope.sub_category_course_list = response.sub_category_course_list;

            })
            .catch(function(error){

                alert("Some error occured");
                console.log(error);

            })
        };

        $scope.postCourseCategorySubmit = function(){

          $('#loaderAjax').show();

            var postParams = {
              "course_id" : $scope.map_id,
              "sub_category_course_id" : $scope.category_sub_id
            };

            var apiEndpoint = "/course/categorysubmit";

            return approvalService.getdatabypost(apiEndpoint, postParams, accessToken)
            .then(function(response){

              console.log(response);
              alert("SUCCESS");

              $('#loaderAjax').hide();

            })
            .catch(function(error){

              $('#loaderAjax').hide();

                alert("Some error occured");
                console.log(error);

            })
        };


        switch (filter) {

        case 'approval-list':

            $scope.getApprovals();

            break;

        case 'approval-page-university':

            $scope.title = "University Approval";
            var universityApprovalId = $routeParams.id;
            $scope.getUniversityApprovalsInfoById(universityApprovalId);

            break;

        case 'approval-page-course':

            $scope.title = "Course Approval";
            var courseApprovalId = $routeParams.id;
            $scope.getCourseApprovalsInfoById(courseApprovalId);

          break;

        case 'approved-page-university':

            $scope.title = "Approved University Approval";
            var universityApprovalId = $routeParams.id;
            $scope.getUniversityApprovedInfoById(universityApprovalId);

            break;

      case 'approved-page-course':

            $scope.title = "Approved Course Approval";
            var courseApprovalId = $routeParams.id;
            $scope.getCourseApprovedInfoById(courseApprovalId);

                break;

      case 'category-course':

            $scope.title = "Category Course";
            $scope.getCategoryCourseList();

            break;
          }

        $scope.viewFile = $route.current.$$route.pageName;
        $scope.location = $location.path();
        $scope.partnerName = partnerName;
		    $scope.header = 'views/header.html';
		    $scope.menu = 'views/menu.html';
		    $scope.footer = 'views/footer.html';
		    $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
        $scope.getMenu();


}]);
