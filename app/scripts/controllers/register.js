'use strict';

angular.module('app')
	.controller('registerController', ['$scope', 'searchService', '$cookieStore', '$route', '$location', '$rootScope', 'commonService', 'NgTableParams', 'assignService', 'ENV', function ($scope, searchService, $cookieStore, $route, $location, $rootScope, commonService, NgTableParams, assignService, ENV){
		$scope.accordion = {
	      	current: null
	    };

		var accessToken = $cookieStore.get("access_token") !== undefined ? $cookieStore.get("access_token") : '';
        var loginId = $cookieStore.get("login_id") !== undefined ? $cookieStore.get("login_id") : '';
        
        $scope.register = {};

		$scope.address = '';
		$scope.intake_month = '';
		$scope.intake_year = '';
		$scope.company_name = '';
        $scope.landline = '';

		$scope.present = new Date().getFullYear();
		$scope.next = new Date().getFullYear() + 1;
		$scope.future = new Date().getFullYear() + 2;

		$scope.addPartner = function(register){
            console.log(register,'---------');
			var postParams = {
				role_id: register.partner_type,
				title_id: register.title,
				first_name: register.first_name,
				last_name: register.last_name,
				email: register.email,
				mobile_number: register.mobile,
				country_id: register.country,
				address: {"address":register.address},
				// intake_month: register.intake_month,
				// intake_year: register.intake_year,
				other_details: { "company_name": register.company_name },
                added_by: loginId, 
				landline_number: register.landline
            }
            // if(register.country) {
            //     postParams
            // }

            console.log(postParams, '---------------');
			return searchService.add(accessToken, '/register', postParams)
			.then(function(data){
				alert(data);
			})
			.catch(function(err){
				alert(err.message);
				console.log(err);
			})

			console.log(postParams);
		};

		$scope.getAllTitles = function(){
			var listId = 3;
			searchService.getListData(listId)
			.then(function(data){
				$scope.allTitles = data;
			});
		};

		$scope.getAllCountries = function(){
			var listId = 1;
			searchService.getListData(listId)
			.then(function(data){
				$scope.allCountries = data;
			});
		};

		$scope.getIntakeMonths = function(){
			var listId = 364;
			searchService.getListData(listId)
			.then(function(data){
				$scope.intakeMonths = data;
			});
		};
        $scope.getRoles = function(){
			return searchService.getRoles(accessToken)
			.then(function(data){
				console.log(data);
				 $scope.roles = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
        };
        $scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};

		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};
        $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();
		$scope.getAllTitles();
		$scope.getAllCountries();
        $scope.getIntakeMonths();
        $scope.getRoles();
}]);
