'use strict';

angular.module('app')
  .controller('universityController', ['$scope', 'commentsService', 'assignService', 'studentService', 'ENV', '$cookieStore', 'commonService', 'bulkuploadService', '$routeParams', 'NgTableParams', '$compile', '$route', 'menuService', '$location', '$rootScope', '$q','popUpService', function ($scope, commentsService, assignService, studentService, ENV, $cookieStore, commonService, bulkuploadService, $routeParams, NgTableParams, $compile, $route, menuService, $location, $rootScope, $q, popUpService) {


    if (!$rootScope.bodylayout) {
      $rootScope.bodylayout = "login-page";
    }

    var accessToken = $cookieStore.get("access_token");
    var filter = $route.current.$$route.filter;


    $scope.getMenu = function () {
      return commonService.getMenu(accessToken)
        .then(function (data) {
          $scope.menus = data;
        })
        .catch(function (error) {
          if (error.error_code === "SESSION_EXPIRED"  || error.error_code === "HEADER_MISSING") {
            window.location.href = '/';
          }
        });
    };

    $scope.toggleMenu = function (id) {
      $('#menu-' + id).toggle();
    };
    $scope.toggleSubMenu = function (id) {
      $('#submenu-' + id).toggle();
    };

    $scope.myAssignList = function () {

      var lead_state = $routeParams.leadstate;

      var api = ENV.apiEndpoint + '/university/universitycallinglist/' + lead_state;

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          //console.log(data);
          $scope.assignlist = data.data;
          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            dataset: dataset
          });

          $scope.data.forEach(function (employee) {
            employee.isEditable = false;
          });

          //console.log(assignlist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.counsellorsList = function () {

      var api = ENV.apiEndpoint + '/university/universitylist';

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          $scope.counselorlist = data.data;

          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            dataset: dataset
          });

          console.log("List", counselorlist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.allcounsellorsList = function () {

      var api = ENV.apiEndpoint + '/university/universitylistall';

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          $scope.counselorlist = data.data;

          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            counts: [10, 50, 100, 1000, 5000],
            dataset: dataset
          });

          console.log("List", counselorlist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.connectFlowMail = function (counselorLoginId) {

      var api = ENV.apiEndpoint + '/university/connectmail/'+counselorLoginId;

      return assignService.assignList(api, accessToken)
        .then(function (data) {

          alert("Mail Sent !!");
          //console.log("List", data);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    //$scope.isEditable = false;
    $scope.editData = function (emp) {
      emp.isEditable = true;
      //$scope.isEditable = true;
    };

    $scope.cancelEditData = function (emp) {
      emp.isEditable = false;
      //$scope.isEditable = true;
    };

    $scope.saveData = function (emp) {
      //$scope.appkeys[$scope.editing] = $scope.newField;

      emp.isEditable = false;
      //console.log('hello', emp);
      var postParams = {
        "id": emp.id,
        "name": emp.name,
        "university_name": emp.university_name,
        "email": emp.email,
        "mobile": emp.mobile,
        "mobile1": emp.mobile1,
        "city": emp.city,
        "state": emp.state,
        "source_name": emp.source_name
      };

      var api = ENV.apiEndpoint + '/university/universitycallinglistupdate';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {

          //console.log(assignlist);
          $route.reload();

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });


    };

    $scope.claculateTime = function (dt) {
      return new Date(dt).getTime();

    };

    $scope.universityCommentById = function (type) {

      var universityId = $routeParams.id;

      var api = ENV.apiEndpoint + '/university/commentlist/' + universityId + '/' + type;

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          //console.log(data.data);
          $scope.userInfo = data.user_data;
          $scope.assignlist = data.data;
          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            dataset: dataset
          });

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.dateformat = function (D) {
      var pad = function (num) {
        var s = '0' + num;
        return s.substr(s.length - 2);
      }
      var Result = D.getFullYear() + '-' + pad((D.getMonth() + 1)) + '-' + pad(D.getDate());
      return Result;
    };

    $scope.updateUniversityCallingComment = function () {

      var callId = $routeParams.id;

      var dateString = $scope.next_FollowUp_Date ? $scope.dateformat($scope.next_FollowUp_Date) : null;
      //  console.log(dateString);
      //console.log("Time :", $scope.time_value);
      if (!($scope.time_value)) {

        alert("Time Spent Not Given !");
        return;

      }

      if (!($scope.counselor_state) && filter != 'university-comments') {

        alert("Counselor State Not Given !");
        return;

      }

      if (!(dateString) && filter != 'university-comments') {

        if ($scope.counselor_state == 'NOTINTERESTED') {
          //console.log("else");

        } else {
          alert("Next Followup Date Not Given !");
          return;
        }

      }

      var entity_type = "CALLINGUNIVERSITY";

      if(filter == 'university-comments') {
        entity_type = "UNIVERSITY";
      }

      var postParams = {
        "entity_id": callId,
        "entity_type": entity_type,
        "time_spent": $scope.time_value,
        "comment": $scope.comments_value,
        "counselor_status": $scope.counselor_state,
        "next_followup_date": dateString
      };

      //console.log(postParams);

      var api = ENV.apiEndpoint + '/university/commentadd';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {
          //$scope.commentslist = data;
          //console.log(data,"hellloooooooo");
          $('#comments_value').val('');
          $('#time_value').val('');
          $('#next_FollowUp_Date').val('');

          if(filter == 'university-comments'){
            window.location.href = '/university/comments/' + callId;
          } else {
            window.location.href = '/university/callingcomments/' + callId;
          }


          //$('#modal-default').modal('hide');
          //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });

    };

    $scope.addPartner = function () {
      if (!$scope.title || !$scope.first_name || !$scope.last_name || !$scope.email || !$scope.country || !$scope.company_name || !$scope.company_url || !$scope.landline || !$scope.city) {
        //console.log("if", $scope.title, $scope.first_name, $scope.last_name, $scope.email, $scope.country, $scope.company_name, $scope.company_url, $scope.landline, $scope.city);
        alert("Please fill all mandatory details");
        return;
      }
      //console.log($scope);
      //console.log("else", $scope.gentitle, $scope.first_name, $scope.last_name, $scope.email, $scope.country, $scope.company_name, $scope.company_url, $scope.landline, $scope.city);
      var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
      var universityCallingId = $routeParams.id;
      //let utmSource = $cookieStore.get("utm_source");
      $scope.loader = true;
      var postParams = {
        partner_type: $scope.partner_type,
        title_id: $scope.gentitle,
        first_name: $scope.first_name,
        last_name: $scope.last_name,
        email: $scope.email,
        mobile_number: $scope.mobile,
        country_id: $scope.country,
        calling_id: universityCallingId,
        // address: $scope.address,
        address: {
          "address": $scope.address
        },
        other_details: {
          "company_name": $scope.company_name,
          "company_url": $scope.company_url,
          "students_college": $scope.students_college,
          "students_final_year": $scope.students_final_year,
          "students_going_abroad": $scope.students_going_abroad,
          "designation": $scope.designation,
          "agreement": $scope.agreement
        },
        role_id: 15,
        landline_number: $scope.landline,
        //utm_source: utmSource,
        city_id: cityId,
        city_name: $scope.city.title
      }
      return commonService.add(accessToken, 'register', postParams)
        .then(function (data) {
          //$cookieStore.remove("utm_source");
          $scope.loader = false;
          if( document.getElementById("logofile").files.length == 0 ){
              //console.log("no files selected");
              alert(data.result);
              window.location.href = '/university/list';
            }  else {

          var fileUploadParams = {
            partner_id: data.partner_id,
            document_meta_id: '13',
            entity_type: 'partner',
            userfile: $scope.file
          }
          return commonService.upload(accessToken, '/document-upload', fileUploadParams)
            .then(function (data) {
              alert("UserName and Password has been sent to Registered mail...");
              window.location.href = '/university/list';
            })
            .catch(function (err) {
              $scope.loader = false;
              alert(err.message);
            })
          }
          //alert(data.result);
        })
        .catch(function (err) {
          $scope.loader = false;
          alert(err.message);
        })

    };

    $scope.addMember = function () {
      if (!$scope.member_title || !$scope.member_first_name || !$scope.member_last_name || !$scope.member_email) {
        alert("Please fill all mandatory details");
        return;
      }

      var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;

      $scope.loader = true;
      var postParams = {
        partner_type: $scope.partner_type,
        title_id: $scope.member_title,
        first_name: $scope.member_first_name,
        last_name: $scope.member_last_name,
        email: $scope.member_email,
        mobile_number: $scope.member_mobile,
        country_id: $scope.country,
        // address: $scope.address,
        address: {
          "address": $scope.address
        },
        other_details: {
          "company_name": $scope.company_name,
          "company_url": $scope.company_url,
          "students_college": $scope.students_college,
          "students_final_year": $scope.students_final_year,
          "students_going_abroad": $scope.students_going_abroad,
          "designation": $scope.designation,
          "agreement": $scope.agreement
        },
        role_id: 17,
        landline_number: $scope.landline,
        //utm_source: utmSource,
        city_id: cityId,
        city_name: $scope.city.title,
        parent_id: $scope.universityLoginId
      }
      return commonService.add(accessToken, 'register', postParams)
        .then(function (data) {
            alert(data.result);
            $scope.loader = false;
            window.location.href = '/university/' + $scope.universityId + '/member';
        })
        .catch(function (err) {
          $scope.loader = false;
          alert(err.message);
        })

    };

    $scope.updateMember = function (partnerId) {
      if (!$scope.gentitle || !$scope.first_name || !$scope.last_name) {
        alert("Please fill all mandatory details");
        return;
      }
      var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
      $scope.loader = true;
      var putParams = {
        title_id: $scope.gentitle,
        first_name: $scope.first_name,
        last_name: $scope.last_name,
        other_details: {
          "designation": $scope.designation
        }
      }

      return commonService.updatePartnerData(accessToken, '/' + partnerId, putParams)
        .then(function (data) {
          $scope.loader = false;
          alert(data.result);
          window.location.href = '/university/' + $scope.universityId + '/member';
        })
        .catch(function (err) {
          $scope.loader = false;
          alert(err.message);
        })

    }

    $scope.getAllTitles = function () {
      var listId = 3;
      commonService.getListData(listId)
        .then(function (data) {
          $scope.allTitles = data;
        });
    };

    $scope.getAllCountries = function () {
      var listId = 1;
      $scope.roleId = $cookieStore.get("role_id");
      // console.log($scope.roleId);
      commonService.getListData(listId)
        .then(function (data) {
          $scope.allCountries = data;
        });
    };

    $scope.agentsList = function () {

      var postParams = {};

      var api = ENV.apiEndpoint + '/agentslist';

      return assignService.agentList(api, postParams, accessToken)
        .then(function (data) {
          $scope.agentslist = data;

          //console.log(agentslist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.universityCallingInfo = function (universityCallingId) {

      var postParams = {};

      var api = ENV.apiEndpoint + '/university/callinginfo/' + universityCallingId;

      return assignService.agentList(api, postParams, accessToken)
        .then(function (data) {
          $scope.universityCallingInfoObj = data;

          $scope.gentitle = data.data.title_id;
          $scope.first_name = data.data.name;
          //$scope.last_name = data.last_name;
          $scope.email = data.data.email;
          $scope.mobile = data.data.mobile;
          $scope.company_name = data.data.university_name;
          $scope.companyurl = data.data.website_name;

          //console.log(data.data.name);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.uploadFile = function (userFile) {

      $scope.error = {
        message: null
      };

      // var sourceId = $scope.select_source;
      var sourceName = $scope.source_name;
      var AgentId = $scope.select_agent ? $scope.select_agent : 0;

      var file = $scope.userfile;
      //console.log('file is ' + file);
      console.dir(file);

      var api = ENV.apiEndpoint + '/university/universityupload';

      return bulkuploadService.counselorFileUpload(api, file, sourceName, AgentId, accessToken)
        .then(function (data) {

          // window.location.href='/counselor/callinglist';
        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.getLeadStateFiltterData = function (lead_state) {

      $location.url('/university/callinglist/' + lead_state);

      var api = ENV.apiEndpoint + '/university/universitycallinglist/' + lead_state;

      return assignService.assignList(api, accessToken)
        .then(function (data) {

          $scope.assignlist = data.data;
          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
            dataset: dataset
          });

          $scope.data.forEach(function (employee) {
            employee.isEditable = false;
          });

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

    $scope.createCallingData = function () {

      var postParams = {
        "name": $scope.user_name,
        "email": $scope.user_email,
        "mobile": $scope.user_mobile,
        "mobile1": $scope.user_mobile1,
        "university_name": $scope.university_name,
        "website_name": $scope.website_name,
        "state": $scope.user_state,
        "city": $scope.user_city,
        "source_name": $scope.source_name,
        "university_status": "FRESH",
        "type": "university"
      };

      //console.log(postParams);

      var api = ENV.apiEndpoint + '/callingdata/add';

      return assignService.updateCommets(api, postParams, accessToken)
        .then(function (data) {

            window.location.href = '/university/callinglist/FRESH';

        })
        .catch(function (error) {
          alert(error.message);
          $scope.error = {
            message: error.message
          };
        });

      };

      $scope.allUploadDocumentTypes = [
        {
          id:13,
          name: "Company Logo"
        },
        {
          id:14,
          name: "GST Document"
        },
        {
          id:15,
          name: "Agreement Document"
        }

      ];

      // $scope.couuDocumentMeta = 0;
      // //var partnerId = $cookieStore.get("partnerId");
      $scope.uploadCounselorFile = function(){
        //console.log($scope);
        var universityId = $routeParams.id;

        var postData = {
          entity_type: "partner",
          partner_id: universityId,
          document_meta_id: $scope.couuDocumentMeta,
          userfile: $scope.userfile
        }
        return assignService.uploadCounselorDocument(accessToken, postData)
        .then(function(response){
          alert("The file has been uploaded successfully");
          //window.location.href = $location.absUrl();
          $scope.getCounselorDocuments(universityId);
        })
        .catch(function(err){
          //console.log(err);
          alert(err.data.message);
        })
      }

      $scope.allCounselorDocuments = [];
      $scope.getCounselorDocuments = function(counselorId){
        //alert(counselorId);
        return assignService.getCounsellorDocuments(accessToken, counselorId , 'partner')
        .then(function(response){
          $scope.allCounselorDocuments = response;
        })
        .catch(function (error) {
            $scope.error = {
              message: error.message
            };
          });
      };


    $scope.allCounselorCallStatus = [{
        id: "FRESH",
        status: "FRESH"
      },
      {
        id: "NOTINTERESTED",
        status: "NOTINTERESTED"
      },
      {
        id: "INTERESTED",
        status: "INTERESTED"
      },
      {
        id: "CALLBACK",
        status: "CALLBACK"
      },
      {
        id: "BEYONDINTAKE",
        status: "BEYONDINTAKE"
      },
      {
        id: "CONVERTED",
        status: "CONVERTED"
      },
      {
        id: "RINGING",
        status: "RINGING"
      }

    ];

    $scope.getPartnerData = function(id){
        var api = ENV.apiEndpoint + '/' + id;
        return commonService.getPartnerData(api, accessToken)
        .then(function(response){
            $scope.gentitle = response.title_id;
            $scope.first_name = response.first_name;
            $scope.last_name = response.last_name;
            $scope.email = response.email;
            $scope.mobile = response.mobile_number;
            $scope.company_logo = "";
            if(response.other_details && response.other_details.company_name){
                $scope.company_name = response.other_details.company_name;
            }
            if(response.other_details && response.other_details.company_url){
                $scope.company_url = response.other_details.company_url;
            }
            if(response.other_details && response.other_details.company_logo){
                $scope.company_logo = response.other_details.company_logo;
            }
            $scope.landline = response.telephone_number;
            if(response.address && response.address.address){
                $scope.address = response.address.address;
            }
            if(response.other_details && response.other_details.students_college){
                $scope.students_college = response.other_details.students_college;
            }
            if(response.other_details && response.other_details.students_final_year){
                $scope.students_final_year = response.other_details.students_final_year;
            }
            if(response.other_details && response.other_details.students_going_abroad){
                $scope.students_going_abroad = response.other_details.students_going_abroad;
            }
            if(response.other_details && response.other_details.designation){
                $scope.designation = response.other_details.designation;
            }
            if(response.other_details && response.other_details.agreement){
                $scope.agreement = response.other_details.agreement;
            }
            $scope.city = {};
            $scope.city.description = {};
            $scope.city.description.id = response.city_id;
            $scope.city.title = response.city_name;
            $scope.country = response.country_id;
            $scope.partnerId = id;
            $scope.universityLoginId = response.login_id;
        })
        .catch(function(error){
            console.log(error);
        })
    }

    $scope.updatePartner = function (partnerId) {
      if (!$scope.title || !$scope.first_name || !$scope.last_name || !$scope.email || !$scope.country || !$scope.company_name || !$scope.company_url || !$scope.landline || !$scope.city) {
        alert("Please fill all mandatory details");
        return;
      }
      var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
      $scope.loader = true;
      var putParams = {
        title_id: $scope.gentitle,
        first_name: $scope.first_name,
        last_name: $scope.last_name,
        country_id: $scope.country,
        role_id: 15,
        address: {
          "address": $scope.address
        },
        other_details: {
          "company_name": $scope.company_name,
          "company_url": $scope.company_url,
          "students_college": $scope.students_college,
          "students_final_year": $scope.students_final_year,
          "students_going_abroad": $scope.students_going_abroad,
          "designation": $scope.designation,
          "agreement": $scope.agreement
        },
        telephone_number: $scope.landline,
        city_id: cityId
      }

      return commonService.updatePartnerData(accessToken, '/' + partnerId, putParams)
        .then(function (data) {
          $scope.loader = false;
          if( document.getElementById("logofile").files.length == 0 ){
              alert(data.result);
              window.location.href = '/university/list';
            }  else {

          var fileUploadParams = {
            partner_id: data.partner_id,
            document_meta_id: '13',
            entity_type: 'partner',
            userfile: $scope.file
          }
          return commonService.upload(accessToken, '/document-upload', fileUploadParams)
            .then(function (data) {
              alert("The profile has been updated successfully.");
              window.location.href = '/counselor/list';
            })
            .catch(function (err) {
              $scope.loader = false;
              alert(err.message);
            })
          }
          //alert(data.result);
        })
        .catch(function (err) {
          $scope.loader = false;
          alert(err.message);
        })

    }

    $scope.getAgentsList = function(universityId){
		commonService.getPartnerListData(accessToken, universityId)
		.then(function (data) {
            data.forEach(function(value, key){
                data[key]['partner_id'] = universityId;
            })
			$scope.tableParams = new NgTableParams({}, { dataset: data });
            //console.log(data);
		});
	}

    switch (filter) {

      case 'myassign-list':
        $scope.title = "My Assign Calling List ";
        $scope.myAssignList();
        break;

      case 'all-university-list':
        $scope.title = "All University List";
        $scope.allcounsellorsList();
        break;

        case 'my-university-list':
          $scope.title = "university List";
          $scope.counsellorsList();
          break;

      case 'university-comments':
        $scope.title = "university Comment";

        $scope.universityCommentById('university');
        break;

    case 'university-calling-comments':
          $scope.title = "university Calling Comment";
          $scope.universityCommentById('callinguniversity');
          break;

      case 'create':
        $scope.title = "Create University";
        $scope.getAllTitles();
        $scope.getAllCountries();
        break;

    case 'callingUniversity':
        $scope.title = "Create University";
        var universityCallingId = $routeParams.id;
        $scope.getAllTitles();
        $scope.getAllCountries();
        $scope.universityCallingInfo(universityCallingId);
        break;

      case 'upload':
        $scope.title = "File Upload";
        $scope.agentsList();
        break;

      case 'edit':
        $scope.title = "My Assign Calling Source List";
        $scope.getAllTitles();
        $scope.getAllCountries();
        var universityId = $routeParams.id;
        $scope.universityId = universityId;
        $scope.getPartnerData(universityId);
        $scope.getCounselorDocuments(universityId);
        break;

      case 'member':
          var universityId = $routeParams.id;
          $scope.universityId = universityId;
          $scope.getAgentsList(universityId);
          break;
      case 'member-create':
        $scope.getAllTitles();
          var universityId = $routeParams.id;
          $scope.universityId = universityId;
          $scope.getPartnerData(universityId);
          break;
      case 'member-edit':
            $scope.getAllTitles();
          var universityId = $routeParams.id;
          $scope.universityId = universityId;
          var memberId = $routeParams.member_id;
          $scope.getPartnerData(memberId);
          break;
    };



    $scope.popMenu = function(rowData){
      $(".title-name").text(rowData.partner_name);
      $scope.partnerLoginId = rowData.login_id;
        var api = ENV.apiEndpoint + '/dashboardcontent/?pid=' + rowData.login_id + '&rid=15';
        return studentService.getData(api, accessToken)
        .then(function(data){
            $scope.partner_name = rowData.partner_name;
            $scope.total = 0;
            $scope.notInterested = 0;
            $scope.interested = 0;
            $scope.potential = 0;
            $scope.potentialDeclined = 0;
            $scope.applicationProcess = 0;
            $scope.applicationReview = 0;
            $scope.applicationReviewDeclined = 0;
            $scope.provisionalOffer = 0;
            $scope.disbursalDocumentation = 0;
            $scope.applicationWithdraw = 0;
            $scope.disbursed = 0;
            $scope.applicationRejected = 0;

            if(data.stages){
                data.stages.forEach(function(value){
                    switch(value['current_lead_state']){
                        case 'NOT_INTERESTED':
                            $scope.notInterested = parseInt(value['lead_count']);
                            break;
                        case 'INTERESTED':
                            $scope.interested = parseInt(value['lead_count']);
                            break;
                        case 'POTENTIAL':
                            $scope.potential = parseInt(value['lead_count']);
                            break;
                        case 'POTENTIAL_DECLINED':
                            $scope.potentialDeclined = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_PROCESS':
                            $scope.applicationProcess = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_REVIEW':
                            $scope.applicationReview = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_REVIEW_DECLINED':
                            $scope.applicationReviewDeclined = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_DECLINED':
                            $scope.applicationRejected = parseInt(value['lead_count']);
                            break;
                        case 'PROVISIONAL_OFFER':
                            $scope.provisionalOffer = parseInt(value['lead_count']);
                            break;
                        case 'DISBURSAL_DOCUMENTATION':
                            $scope.disbursalDocumentation = parseInt(value['lead_count']);
                            break;
                        case 'DISBURSED':
                            $scope.disbursed = parseInt(value['lead_count']);
                            break;
                        case 'APPLICATION_WITHDRAWN':
                            $scope.applicationWithdraw = parseInt(value['lead_count']);
                            break;
                    }
                })
            }

            $scope.total = $scope.notInterested + $scope.interested + $scope.potential + $scope.potentialDeclined + $scope.applicationProcess + $scope.applicationReview + $scope.applicationReviewDeclined + $scope.provisionalOffer + $scope.disbursalDocumentation + $scope.applicationWithdraw + $scope.disbursed + $scope.applicationRejected;
            $("#basicModal").modal('show');
        })
    }

    $scope.viewFile = $route.current.$$route.pageName;
    $scope.location = $location.path();
    $scope.header = 'views/header.html';
    $scope.menu = 'views/menu.html';
    $scope.footer = 'views/footer.html';
    $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
    $scope.searchApi = ENV.apiEndpoint + 'cities/';
    $scope.getMenu();

  }]);
