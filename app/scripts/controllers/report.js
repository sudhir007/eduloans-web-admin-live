'use strict';

angular.module('app')
	.controller('reportController', ['$scope', 'reportService', 'ENV', '$cookieStore', '$route', 'menuService', 'assignService', 'NgTableParams', '$rootScope', '$location', function ($scope, reportService, ENV, $cookieStore, $route, menuService, assignService, NgTableParams, $rootScope, $location){

		$scope.product_id = [];
		$scope.lead_state_id = [];
		$scope.calling_lead_state_id = [];
		$scope.lead_state_bankwise_id = [];
		$scope.lead_rm_id = [];
		$scope.entity_list_id = [];
		$scope.rm_report_type = [];
		$scope.leadStateSetting = { displayProp: 'status', idProperty:'id' };
		$scope.callingleadStateSetting = { displayProp: 'lead_state', idProperty:'id' };
		$scope.productSetting = { displayProp: 'display_name', idProperty:'id' };
		$scope.rmSetting = { displayProp: 'email', idProperty:'id' };
		$scope.entityListSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true };
		$scope.rmReportTypeSetting = { displayProp: 'value', idProperty:'id' };

		$scope.source_name_value = [];
		$scope.sourceNameSetting = { displayProp: 'name', idProperty:'id', enableSearch: true };

		$scope.showTable = false;
		$scope.showTableApplication = false;

		//calling data
		$scope.lead_state_calling = [];
		$scope.leadStateSettingCalling = { displayProp: 'lead_state', idProperty:'id' };

		var accessToken = $cookieStore.get("access_token");
		var filter = $route.current.$$route.filter;

		$scope.getMenu = function(){
			var api = ENV.apiEndpoint + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.productsList = function(){
			console.log("hello");

    		var api = ENV.apiEndpoint + '/productList/1';

    		return reportService.productsList(api, accessToken)
    		.then(function(data){
						$scope.productlists = data;
					//console.log("hello",data.data);

          //console.log("errrr");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.getLeadList = function(){
			var api = ENV.apiEndpoint + '/lead/list';
			var params = {
				currentLeadState: "'REFERED'"
			};
			return reportService.getData(api, accessToken, params)
			.then(function(data){
				$scope.leadList = data;
				var dataset = [];
				data.forEach(function(tableData, key){
					var params = {};
					params.no = key;
					params.name = tableData.first_name;
					dataset.push(params);
				});
				$scope.tableParams = new NgTableParams({}, { dataset: dataset});
			});
		};

		$scope.dateformat = function(D){
			 var pad = function(num) { var s = '0' + num; return s.substr(s.length - 2); }
			 var Result = D.getFullYear() + '-' + pad((D.getMonth() + 1)) + '-' + pad(D.getDate());
			 return Result; };

		$scope.reportsData = function(){

			console.log($('#start_date').val(),"end dateeeee",$('#partner_id').val());

			//var start_date = $scope.start_date ? $scope.dateformat($('#start_date').val()) : $scope.dateformat(new Date());
			//var end_date = $scope.end_date ? $scope.dateformat($('#end_date').val()) : $scope.dateformat(new Date());

			var partnerId = $('#partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			 var postParams = {
				 "partner_id" : partnerId,
				 "start_date" : $('#start_date').val(),
				 "end_date" : $('#end_date').val()
			 };
				var api = ENV.apiEndpoint + '/reports/partners';
				//console.log(postParams,"hello");

				return reportService.reportsData(api, postParams, accessToken)
				.then(function(data){
					//console.log("helllllllllllllllll1111111",data);
					$scope.reportsDataList = data;
					$scope.showTable = true;

					//console.log("hekkk",$scope.reportsDataList);

				//	if(csvdownload == 1){
			/*	var anchor = angular.element('<a/>');
	     anchor.attr({
	         href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data),
	         target: '_blank',
	         download: 'filename.csv'
	     })[0].click();*/

				})
				.catch(function(error){
					$scope.showTable = false;
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.dayReportsData = function(){

			//console.log($('#start_date').val(),"end dateeeee",$('#partner_id').val());
			$('#loaderAjax').show();
			//var start_date = $scope.start_date ? $scope.dateformat($('#start_date').val()) : $scope.dateformat(new Date());
			//var end_date = $scope.end_date ? $scope.dateformat($('#end_date').val()) : $scope.dateformat(new Date());

			var partnerId = $('#partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			 var postParams = {
				 "report_type" : $scope.rm_report_type ? $scope.rm_report_type : 0, // $('#select_type').val(),
				 "partner_id" : partnerId, //$scope.lead_rm_id ? $scope.lead_rm_id : 0,
				 "start_date" : $('#start_date').val(),
				 "end_date" : $('#end_date').val()
			 };
				var api = ENV.apiEndpoint + '/reports/partners/days';
				//console.log(postParams,"hello");

				return reportService.reportsData(api, postParams, accessToken)
				.then(function(data){
					$('#loaderAjax').hide();
					console.log("helllllllllllllllll1111111",data);
					$scope.reportsDataList = data;
					$scope.showTable = true;

					console.log("hekkk",$scope.reportsDataList);

				//	if(csvdownload == 1){
			/*	var anchor = angular.element('<a/>');
	     anchor.attr({
	         href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data),
	         target: '_blank',
	         download: 'filename.csv'
	     })[0].click();*/

				})
				.catch(function(error){
					$('#loaderAjax').hide();
					$scope.showTable = false;
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.dayReportsDataDownload = function(){

			$('#loaderAjax').show();


			var partnerId = $('#partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			 var postParams = {
				 "report_type" : $scope.rm_report_type ? $scope.rm_report_type : 0,
				 "partner_id" : partnerId,
				 "start_date" : $('#start_date').val(),
				 "end_date" : $('#end_date').val()
			 };
				var api = ENV.apiEndpoint + '/leads/comment/reports';
				//console.log(postParams,"hello");

				return reportService.reportsData(api, postParams, accessToken)
				.then(function(data){

					$('#loaderAjax').hide();

				var anchor = angular.element('<a/>');
		     anchor.attr({
		         href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data),
		         target: '_blank',
		         download: partnerId+'_filename.csv'
		     })[0].click();

				})
				.catch(function(error){
					$('#loaderAjax').hide();
					$scope.showTable = false;
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.loginStatData = function(){

			//console.log($('#start_date').val(),"end dateeeee",$('#partner_id').val());
			$('#loaderAjax').show();
			//var start_date = $scope.start_date ? $scope.dateformat($('#start_date').val()) : $scope.dateformat(new Date());
			//var end_date = $scope.end_date ? $scope.dateformat($('#end_date').val()) : $scope.dateformat(new Date());

			var partnerId = $('#partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			 var postParams = {
				 "partner_id" : partnerId, //$scope.lead_rm_id ? $scope.lead_rm_id : 0,
				 "start_date" : $('#start_date').val(),
				 "end_date" : $('#end_date').val()
			 };
				var api = ENV.apiEndpoint + '/loginstat';
				//console.log(postParams,"hello");

				return reportService.reportsData(api, postParams, accessToken)
				.then(function(data){
					$('#loaderAjax').hide();
					console.log("helllllllllllllllll1111111",data);
					$scope.loginStatDataList = data.data;
					$scope.showTable = true;

				})
				.catch(function(error){
					$('#loaderAjax').hide();
					$scope.showTable = false;
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.businessStatData = function(){

			$('#loaderAjax').show();

			 var postParams = {
				 "lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
				 "calling_lead_state" : $scope.calling_lead_state_id ? $scope.calling_lead_state_id : 0,
				 "login_id" : $scope.lead_rm_id ? $scope.lead_rm_id : 0,
				 "intake_year" : $('#intake_year_business').val(), //$scope.intake_year_business,
				 "date_one" : $('#date_one').val(),
				 "date_two" : $('#date_two').val(),
				 "date_three" : $('#date_three').val(),
				 "date_four" : $('#date_four').val(),
				 "date_five" : $('#date_five').val(),
				 "date_six" : $('#date_six').val(),
				 "date_seven" : $('#date_seven').val(),
				 "date_eight" : $('#date_eight').val()
			 };
				var api = ENV.apiEndpoint + '/businessStatData';

				return reportService.reportsData(api, postParams, accessToken)
				.then(function(data){

					$('#loaderAjax').hide();

					$scope.businessStatDataList = data.data;
					$scope.dateRange = data.dates;
					$scope.showTable = true;

				})
				.catch(function(error){
					$('#loaderAjax').hide();
					$scope.showTable = false;
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.onloadReportsData = function(){

			//console.log($scope.end_date,"end dateeeee");

			//var start_date = $scope.start_date ? $scope.dateformat($scope.start_date) : $scope.dateformat(new Date());
			//var end_date = $scope.end_date ? $scope.dateformat($scope.end_date) : $scope.dateformat(new Date());

			 var postParams = {
				 "partner_id" : "26",
				 "start_date" : "2018-07-02",
				 "end_date" : "2018-07-05"
			 };
				var api = ENV.apiEndpoint + '/reports/partners';
				//console.log(postParams,"hello");

				return reportService.reportsData(api, postParams, accessToken)
				.then(function(data){
					console.log("helllllllllllllllll",data);
					$scope.reportsDataList = data;
					$scope.showTable = true;

					console.log("hekkk",$scope.reportsDataList);

				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});
		};

		$scope.agentsList = function(){

			var postParams =  {};

    		var api = ENV.apiEndpoint + '/agentslist';

    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){
    			$scope.agentslist = data;

          console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.employeeList = function(){

			var postParams =  {};

    		var api = ENV.apiEndpoint + '/employeelist';

    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){
    			$scope.employeelist = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.overallreports = function(){

			var postParams =  {};

    		var api = ENV.apiEndpoint + '/reports/partners/overall';

    		return reportService.overallreports(api, postParams, accessToken)
    		.then(function(data){

					//$scope.assignlist = data;
          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

          //console.log(assignlist);


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.filterLeadReports = function(){

			var partnerId = $('#lead_partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			var teamChecked = false;

			if(document.getElementById('lead_team').checked) {
			    teamChecked = true;
			} else {
			    teamChecked = false;
			}

			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#lead_team').val(),//$scope.lead_team,
					"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
					"start_date" : $('#lead_start_date').val(),
					"end_date" : $('#lead_end_date').val()
			};

			//console.log(postParams);

    		var api = ENV.apiEndpoint + '/filterslistbankteam';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){
					//alert("meeee");
					$scope.leadAppFilter = data.data;
				//	var dataset = data.data;
				//	console.log(dataset);
				//	$scope.leadAppFilterData = new NgTableParams({}, {dataset: dataset});
					//$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.leadFilterInfoData = function(leadstate){

			var partnerId = $('#lead_partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			var teamChecked = false;

			if(document.getElementById('lead_team').checked) {
			    teamChecked = true;
			} else {
			    teamChecked = false;
			}

			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#lead_team').val(),//$scope.lead_team,
					"lead_state" : leadstate,
					"start_date" : $('#lead_start_date').val(),
					"end_date" : $('#lead_end_date').val()
			};

			//console.log(postParams);

    		var api = ENV.apiEndpoint + '/leadfilterdata';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){
					//alert("meeee");
					$scope.leadFilterData = data.data;
					var dataset = data.data;
					$scope.leadTableParams = new NgTableParams({}, {dataset: dataset});
					//$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.filterbankReports = function(){

			var partnerId = $('#application_partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			var teamChecked = false;

			if(document.getElementById('application_team').checked) {
			    teamChecked = true;
			} else {
			    teamChecked = false;
			}

			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
					"lead_state" : $scope.lead_state_bankwise_id ? $scope.lead_state_bankwise_id : 0,
					"product_id" : $scope.product_id ? $scope.product_id : 0,
					"start_date" : $('#application_start_date').val(),
					"end_date" : $('#application_end_date').val()
			};

			//console.log(postParams);

    		var api = ENV.apiEndpoint + '/applicationfilterslistbankteam';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){

					//alert("meeee");
					$scope.applicationLeadFilter = data.data;
					//$scope.showTableApplication = true;
					var dataset = data.data;
					$scope.applicationTableParams = new NgTableParams({}, {
						counts: [10, 30, 50, 100, 1000],
						dataset: dataset
					});


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.callingReports = function(){

			var partnerId = $('#application_partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			var teamChecked = false;

			if(document.getElementById('application_team').checked) {
			    teamChecked = true;
			} else {
			    teamChecked = false;
			}

			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
					"lead_state" : $scope.lead_state_calling ? $scope.lead_state_calling : 0,
					"start_date" : $('#application_start_date').val(),
					"end_date" : $('#application_end_date').val()
			};

			//console.log(postParams);

    		var api = ENV.apiEndpoint + '/callingfilterreport';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){

					//alert("meeee");
					$scope.applicationLeadFilter = data.data;
					//$scope.showTableApplication = true;
					var dataset = data.data;
					$scope.callingTableParams = new NgTableParams({}, {dataset: dataset});


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.applicationleadFilterInfoData = function(leadstate,productid){

			var partnerId = $('#application_partner_id').val();
			partnerId = partnerId.substring(7, partnerId.length);

			var teamChecked = false;

			if(document.getElementById('application_team').checked) {
			    teamChecked = true;
			} else {
			    teamChecked = false;
			}

			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
					"lead_state" : leadstate,
					"product_id" : productid,
					"start_date" : $('#application_start_date').val(),
					"end_date" : $('#application_end_date').val()
			};

			console.log(postParams);

    		var api = ENV.apiEndpoint + '/applicationleadfilterdata';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){
					//alert("meeee");
					$scope.applicationleadFilterData = data.data;
					var dataset = data.data;

					$scope.tableParamsApplication = new NgTableParams({}, {
						counts: [10, 30, 50, 100, 1000],
						dataset: dataset
					});

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.applicationCallingFilterInfoData = function(leadstate){

			 var partnerId = $('#application_partner_id').val();
			 partnerId = partnerId.substring(7, partnerId.length);

			var teamChecked = false;

			if(document.getElementById('application_team').checked) {
			    teamChecked = true;
			} else {
			    teamChecked = false;
			}

			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
					"lead_state" : leadstate,
					"start_date" : $('#application_start_date').val(),
					"end_date" : $('#application_end_date').val()
			};

			console.log(postParams);

    		var api = ENV.apiEndpoint + '/callingfilterreportdata';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){
					//alert("meeee");
					$scope.applicationleadFilterData = data.data;
					var dataset = data.data;
					$scope.tableParamsCallingLead = new NgTableParams({}, {dataset: dataset});
					$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};



		$scope.callcenterReports = function(){

			 var partnerId = $('#application_partner_id').val();
			 partnerId = partnerId.substring(7, partnerId.length);

			var teamChecked = false;

			if(document.getElementById('application_team').checked) {
			    teamChecked = true;
			} else {
			    teamChecked = false;
			}

			var postParams = {
					"partner_id" : partnerId,//$scope.lead_partner_id,
					"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
					"start_date" : $('#application_start_date').val(),
					"end_date" : $('#application_end_date').val()
			};

			console.log(postParams);

    		var api = ENV.apiEndpoint + '/callcenter/report';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){
					//alert("meeee");
					//$scope.applicationleadFilterData = data.data;
					var dataset = data.data;
					$scope.callcenterTableParams = new NgTableParams({}, {dataset: dataset});
					$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};


$scope.applicationCallcenterReportData = function(){

	 var partnerId = $('#application_partner_id').val();
	 partnerId = partnerId.substring(7, partnerId.length);

	var teamChecked = false;

	if(document.getElementById('application_team').checked) {
			teamChecked = true;
	} else {
			teamChecked = false;
	}

	var postParams = {
			"partner_id" : partnerId,//$scope.lead_partner_id,
			"team_type" : teamChecked,//$('#application_team').val(),//$scope.lead_team,
			"start_date" : $('#application_start_date').val(),
			"end_date" : $('#application_end_date').val()
	};

	console.log(postParams);

		var api = ENV.apiEndpoint + '/callcenter/reportdata';

		return reportService.reportsData(api, postParams, accessToken)
		.then(function(data){
			//alert("meeee");
			//$scope.applicationleadFilterData = data.data;
			var dataset = data.data;
			$scope.tableParamscallcenternumber = new NgTableParams({}, {dataset: dataset});
			$scope.showTable = true;


		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

	$scope.counsellorsList = function () {
      console.log("Here");

      var api = ENV.apiEndpoint + '/counselor/counselorlist';

      return assignService.assignList(api, accessToken)
        .then(function (data) {
          $scope.counselorlist = data.data;

          console.log("List", counselorlist);

        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
    };

		$scope.hearAboutUsSelect = function(){
          	// alert("hi");
          	if(document.getElementById('hear_About_Us').value == "311" || document.getElementById('hear_About_Us').value == "314" || document.getElementById('hear_About_Us').value == "8" || document.getElementById('hear_About_Us').value == "411" || document.getElementById('hear_About_Us').value == "431" )
            {
            	// alert("hiiii");
            	$("#counselorDropDown").css("display", "none");
            }
            else{
							$("#counselorDropDown").css("display", "block");
							$scope.hearAboutUsOptionsData(document.getElementById('hear_About_Us').value);
            }
        };

				$scope.hearAboutUsOptionsData = function(hearId){

						var api = ENV.apiEndpoint + '/indianCollegeOrAgentList/'+hearId;

					return assignService.assignList(api, accessToken)
					.then(function(data){
						$scope.hearAboutUsListData = data.data;

					})
					.catch(function(error){
									$scope.error = {
											message: error.message
									};
					});

				};

				$scope.countryList = function(){

						var api =  ENV.apiEndpoint + '/indianCollegeOrAgentList/country';

					return assignService.assignList(api, accessToken)
					.then(function(data){
						$scope.countryListData = data.data;

					})
					.catch(function(error){
									$scope.error = {
											message: error.message
									};
					});

				};

        $scope.hearAboutUsReports = function(){



			var postParams = {
					"hear_about_us_id" : $scope.hear_About_Us ? $scope.hear_About_Us : 0,
					"rm_id" : $scope.lead_rm_id ? $scope.lead_rm_id : 0,
					"entity_id" : $scope.entity_list_id ? $scope.entity_list_id : 0,
					"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
					"product_list" : $scope.product_id ? $scope.product_id : 0,
					"intake_year_filter" : $scope.intake_year_filter ? $scope.intake_year_filter : "ALL",
					"start_date" : $('#hearaboutus_start_date').val(),
					"end_date" : $('#hearaboutus_end_date').val()
			};

			console.log(postParams);

    		var api = ENV.apiEndpoint + '/aboutusfilter';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){

					// $scope.leadAppFilter = data.data;
			var dataset = data.data;
			$scope.tableParamsHearAboutUs = new NgTableParams({}, {dataset: dataset});
			$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.hearAboutUsReportsAllLeads = function(){

	var postParams = {
			"hear_about_us_id" : $scope.hear_About_Us ? $scope.hear_About_Us : 0,
			"rm_id" : $scope.lead_rm_id ? $scope.lead_rm_id : 0,
			"entity_id" : $scope.entity_list_id ? $scope.entity_list_id : 0,
			"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
			"product_list" : $scope.product_id ? $scope.product_id : 0,
			"intake_year_filter" : $scope.intake_year_filter ? $scope.intake_year_filter : "ALL",
			"start_date" : $('#hearaboutus_start_date').val(),
			"end_date" : $('#hearaboutus_end_date').val(),
			"show_all_leads" : "YES"
	};

	console.log(postParams);

		var api = ENV.apiEndpoint + '/aboutusfilter';

		return reportService.reportsData(api, postParams, accessToken)
		.then(function(data){

			$scope.hearAboutUsFilterDataall = data.data;
			var dataset = data.data;
			$scope.hearAboutUsTableParamsall = new NgTableParams({}, {
				counts: [10, 50, 100, 1000, 5000],
				dataset: dataset
			});


		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

$scope.oneGoReports = function(){

var postParams = {
	"hear_about_us_id" : $scope.hear_About_Us ? $scope.hear_About_Us : 0,
	"rm_id" : $scope.lead_rm_id ? $scope.lead_rm_id : 0,
	"entity_id" : $scope.entity_list_id ? $scope.entity_list_id : 0,
	"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
	"product_list" : $scope.product_id ? $scope.product_id : 0,
	"intake_year_filter" : $scope.intake_year_filter ? $scope.intake_year_filter : "ALL",
	"start_date" : $('#onego_start_date').val(),
	"end_date" : $('#onego_end_date').val()
};

console.log(postParams);

var api = ENV.apiEndpoint + '/aboutusfilter';

return reportService.reportsData(api, postParams, accessToken)
.then(function(data){

	// $scope.leadAppFilter = data.data;
var dataset = data.data;
$scope.tableParamsHearAboutUs = new NgTableParams({}, {dataset: dataset});
$scope.showTable = true;


})
.catch(function(error){
				$scope.error = {
						message: error.message
				};
});
};

		$scope.referredReports = function(){

	var counselorId = $('#counselorList').val();
	counselorId = counselorId.substring(7, counselorId.length);

	var teamChecked = false;

	if(document.getElementById('application_team').checked) {
			teamChecked = true;
	} else {
			teamChecked = false;
	}

	var postParams = {
			"partner_id" : $scope.application_partner_id ? $scope.application_partner_id : 0,
			"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
			"product_id" : $scope.product_id ? $scope.product_id : 0,
			"country_id" : $scope.country_id ? $scope.country_id : 0,
			"alldata" : "NO",
			"team_type" : teamChecked,
			"start_date" : $('#referred_start_date').val() ? $('#referred_start_date').val() : 0,
			"end_date" : $('#referred_end_date').val() ? $('#referred_end_date').val() : 0
	};

	console.log(postParams);

		var api = ENV.apiEndpoint + '/referredreports';

		return reportService.reportsData(api, postParams, accessToken)
		.then(function(data){

			// $scope.leadAppFilter = data.data;
	var dataset = data.data;
	$scope.applicationTableParams = new NgTableParams({}, {dataset: dataset});
	$scope.showTable = true;


		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

$scope.referredReportsAllData = function(){

var counselorId = $('#counselorList').val();
counselorId = counselorId.substring(7, counselorId.length);

var teamChecked = false;

if(document.getElementById('application_team').checked) {
	teamChecked = true;
} else {
	teamChecked = false;
}

var postParams = {
	"partner_id" : $scope.application_partner_id ? $scope.application_partner_id : 0,
	"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
	"product_id" : $scope.product_id ? $scope.product_id : 0,
	"country_id" : $scope.country_id ? $scope.country_id : 0,
	"alldata" : "YES",
	"team_type" : teamChecked,
	"start_date" : $('#referred_start_date').val() ? $('#referred_start_date').val() : 0,
	"end_date" : $('#referred_end_date').val() ? $('#referred_end_date').val() : 0
};

console.log(postParams);

var api = ENV.apiEndpoint + '/referredreports';

return reportService.reportsData(api, postParams, accessToken)
.then(function(data){

	// $scope.leadAppFilter = data.data;
var dataset = data.data;
$scope.applicationTableParamsAllData = new NgTableParams({}, {
	counts: [10, 50, 100, 1000],
	dataset: dataset
});
$scope.showTable = true;


})
.catch(function(error){
				$scope.error = {
						message: error.message
				};
});
};

       	$scope.hearAboutUsFilterInfoData = function(counselor_id,rm_id,current_leadstate,product_id,imperial_loan_required,imperial_enrolled){

			var postParams = {

					"hear_about_us_id" : $scope.hear_About_Us,
					"rm_id" : rm_id,
					"entity_id" : counselor_id,
					"product_id" : product_id,
					"lead_state" : current_leadstate,
					"imperial_loan_required" : imperial_loan_required,
					"imperial_enrolled" : imperial_enrolled,
					"intake_year_filter" : $scope.intake_year_filter ? $scope.intake_year_filter : "ALL",
					"start_date" : $('#hearaboutus_start_date').val(),
					"end_date" : $('#hearaboutus_end_date').val()
			};

			console.log(postParams);

    		var api = ENV.apiEndpoint + '/aboutusfilterdata';

    		return reportService.reportsData(api, postParams, accessToken)
    		.then(function(data){
					//alert("meeee");
					$scope.hearAboutUsFilterData = data.data;
					var dataset = data.data;
					$scope.hearAboutUsTableParams = new NgTableParams({}, {dataset: dataset});
					//$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.referredReportData = function(current_leadstate,productId,countryId,partnerId){

	var postParams = {

		"partner_id" : partnerId ? partnerId : 0,
		"lead_state" : current_leadstate ? current_leadstate : 0,
		"product_id" : productId ? productId : 0,
		"country_id" : countryId ? countryId : 0,
		"start_date" : $('#referred_start_date').val() ? $('#referred_start_date').val() : 0,
		"end_date" : $('#referred_end_date').val() ? $('#referred_end_date').val() : 0
	};

	//console.log(postParams);

		var api = ENV.apiEndpoint + '/referredreportsdata';

		return reportService.reportsData(api, postParams, accessToken)
		.then(function(data){
			//alert("meeee");
			//$scope.hearAboutUsFilterData = data.data;
			var dataset = data.data;
			$scope.applicationTableParamsData = new NgTableParams({}, {dataset: dataset});
			//$scope.showTable = true;


		})
		.catch(function(error){
						$scope.error = {
								message: error.message
						};
		});
};

			$scope.leadLogsSourceReport = function(){

						var postParams = {

						"partner_id" : $scope.lead_partner_id,
						"start_date" : $('#lead_start_date').val() ? $('#lead_start_date').val() : 0

						};

						var api = ENV.apiEndpoint + '/leads/source';

						return reportService.reportsData(api, postParams, accessToken)
						.then(function(data){
							//alert("meeee");
							$scope.leadLogsData = data.data;

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
			};

			$scope.leadLogsSourceData = function(sourceName, tagName){

						var postParams = {

						"partner_id" : $scope.lead_partner_id,
						"start_date" : $('#lead_start_date').val() ? $('#lead_start_date').val() : 0,
						"source_name" : sourceName,
						"tag_name" : tagName
						};

						var api = ENV.apiEndpoint + '/leads/source/data';

						return reportService.reportsData(api, postParams, accessToken)
						.then(function(data){
							//alert("meeee");
							var dataset = data.data;
							$scope.tableParamsLeads = new NgTableParams({}, {
								counts: [10, 50, 100, 1000, 5000],
								dataset: dataset
							});

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
			};

			$scope.leadLogsRmsReport = function(){

						var postParams = {

						"partner_id" : $scope.lead_partner_id,
						"start_date" : $('#lead_start_date').val() ? $('#lead_start_date').val() : 0

						};

						var api = ENV.apiEndpoint + '/leads/rms';

						return reportService.reportsData(api, postParams, accessToken)
						.then(function(data){
							//alert("meeee");
							$scope.leadLogsData = data.data;

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
			};

			$scope.leadLogsRmsData = function(agentEmail, tagName){

						var postParams = {

						"partner_id" : $scope.lead_partner_id,
						"start_date" : $('#lead_start_date').val() ? $('#lead_start_date').val() : 0,
						"agent_email" : agentEmail,
						"tag_name" : tagName
						};

						var api = ENV.apiEndpoint + '/leads/rms/data';

						return reportService.reportsData(api, postParams, accessToken)
						.then(function(data){
							//alert("meeee");
							var dataset = data.data;
							$scope.tableParamsLeads = new NgTableParams({}, {
								counts: [10, 50, 100, 1000, 5000],
								dataset: dataset
							});

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
			};

			$scope.dailyLeadLogs = function(){

						var postParams = {

						"partner_id" : $scope.lead_partner_id,
						"start_date" : $('#lead_start_date').val() ? $('#lead_start_date').val() : 0,
						"end_date" : $('#lead_end_date').val() ? $('#lead_end_date').val() : 0

						};

						var api = ENV.apiEndpoint + '/daily/leads/logs';

						return reportService.reportsData(api, postParams, accessToken)
						.then(function(data){
							//alert("meeee");

							var dataset = data.data;
							$scope.dailyLeadLogsData = new NgTableParams({}, {
								counts: [10, 50, 100],
								dataset: dataset
							});

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
			};

			$scope.sourceList = function(){

					var api = ENV.apiEndpoint + '/allsource/1';

				return assignService.assignList(api, accessToken)
				.then(function(data){

					$scope.sourceNameValue = data.data;

				})
				.catch(function(error){
								$scope.error = {
										message: error.message
								};
				});

			};

			$scope.sourceReport = function(){

						var postParams = {
						"source_id" : $scope.source_name_value
						};

						var api = ENV.apiEndpoint + '/calling/source/report';

						return reportService.reportsData(api, postParams, accessToken)
						.then(function(data){
							//alert("meeee");

							var dataset = data.data;
							$scope.sourceReportParam = new NgTableParams({}, {
								counts: [10, 50, 100],
								dataset: dataset
							});

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
			};

			$scope.sourceReportData = function(sourceId, leadState){

						var postParams = {
						"source_id" : sourceId,
						"lead_state" : leadState
						};

						var api = ENV.apiEndpoint + '/calling/source/reportdata';

						return reportService.reportsData(api, postParams, accessToken)
						.then(function(data){
							//alert("meeee");

							var dataset = data.data;
							$scope.callingleadTableParams = new NgTableParams({}, {
								counts: [10, 50, 100],
								dataset: dataset
							});

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
			};


		$scope.allStatus = [
		 {
			 id: "INTERESTED",
			 status: "Interested"
		 },
		 {
			 id: "POTENTIAL",
			 status: "Potential"
		 },
		 {
			 id: "APPLICATION_PROCESS",
			 status: "Application Process"
		 },
		 {
			 id: "APPLICATION_REVIEW"	,
			 status: "Application Review"
		 },
		 {
			 id: "PROVISIONAL_OFFER"	,
			 status: "Provisional Offer"
		 },
		 {
			 id: "DISBURSAL_DOCUMENTATION"	,
			 status: "Disbursal Documentation"
		 },
		 {
			 id: "DISBURSED"	,
			 status: "Disbursed"
		 },
		 {
			 id: "PARTIALLY_DISBURSED"	,
			 status: "Partially Disbursed"
			},
		 {
			 id: "BEYOND_INTAKE"	,
			 status: "Beyond Intake"
		 },
		 {
			 id: "NOT_INTERESTED",
			 status: "Not Interested"
		 },
		 {
			 id: "POTENTIAL_DECLINED",
			 status: "Potential Declined"
		 },
		 {
			 id: "APPLICATION_REVIEW_DECLINED"	,
			 status: "Application Review Declined"
		 },
		 {
				 id:"ACTIVE_LEADS",
				 status: "ACTIVELEADS"
		 },
		 {
				 id:"TOTAL_LEADS",
				 status: "TOTALLEADS"
		 }
	 ];

	 $scope.allStatusCalling = [
		 {
				 id: "FRESH",
				 lead_state: "FRESH"
		 },
			 {
					 id: "NOTINTERESTED",
					 lead_state: "NOTINTERESTED"
			 },
			 {
					 id:"INTERESTED",
					 lead_state: "INTERESTED"
			 },
			 {
					 id:"CALLBACK",
					 lead_state: "CALLBACK"
			 },
			 {
					 id:"BEYONDINTAKE",
					 lead_state: "BEYONDINTAKE"
			 },
			 {
					 id:"RINGING",
					 lead_state: "RINGING"
			 },
			 {
					 id:"LEADCONVERTED",
					 lead_state: "LEADCONVERTED"
			 },
			 {
					 id:"ACTIVE_LEADS",
					 lead_state: "ACTIVELEADS"
			 },
			 {
					 id:"TOTAL_LEADS",
					 lead_state: "TOTALLEADS"
			 }


	 ];

	 $scope.hearAboutUsOptions = [
        {
            id: "13",
            status: "Cold Calling"
        },
          {
              id: "312",
              status: "Education Counsellors"
          },
          {
              id:"8",
              status: "Email"
          },
          {
              id:"311",
              status: "Friend Reference"
          },
          {
              id:"9",
              status: "Newspaper"
          },
          {
              id:"314",
              status: "Other Reference"
          },
          {
              id:"11",
              status: "Seminar"
          },
          {
              id:"10",
              status: "Social Media"
          },
          {
              id:"313",
              status: "University"
          },
					{
              id:"407",
              status: "Webinar"
          },
					{
              id:"408",
              status: "Walkin"
          },
					{
              id:"411",
              status: "Website"
          },
					{
							id:"431",
							status: "Google ADS"
					},
					{
							id:"416",
							status: "Mobile App"
					},
					{
							id:"415",
							status: "Call To Action"
					},
					{
							id:"414",
							status: "Eduloans.us"
					},
					{
							id:"413",
							status: "Imperial Website"
					}

      ];

			$scope.rmReportTypeValue = [
			 {
				 id: "LEAD",
				 value: "LEAD"
			 },
			 {
				 id: "CALLINGSTUDENT",
				 value: "CALLING STUDENT"
			 },
			 {
				 id: "COUNSELOR",
				 value: "COUNSELOR"
			 },
			 {
				 id: "CALLINGCOUNSELOR",
				 value: "CALLING COUNSELOR"
			 },
			 {
				 id: "UNIVERSITY",
				 value: "UNIVERSITY"
			 },
			 {
				 id: "CALLINGUNIVERSITY",
				 value: "CALLING UNIVERSITY"
			 }
		 ];

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

		switch(filter){


			case 'filter-report':
						$scope.employeeList();
						break;

      case 'over-all-report':
          $scope.title = "Over All Report";
          $scope.overallreports();
          break;

			case 'referred-report':
		          $scope.title = "Referred All Report";
		          //$scope.hearAboutUsOptionsData(1);
							$scope.countryList();
		          break;

			case 'business-stat':
						$scope.title = "Business Stat Report";
						break;

			case 'login-stat':
						$scope.title = "Login Stat Report";
						break;

			case 'source-report-list':
						$scope.title = "Source Report List";
						$scope.sourceList();
						break;

			case 'lead-logs':
						$scope.title = "Leads Log";
						break;
				}



		//$scope.hearAboutUsSelect();
		$scope.counsellorsList();
		$scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();
		$scope.agentsList();
		$scope.productsList();
		//$scope.onloadReportsData();

    }])
		.filter('split', function() {
	        return function(input, splitChar, splitIndex) {
	            // do some bounds checking here to ensure it has that index
	            return input.split(splitChar)[splitIndex];
	        }
	    });
