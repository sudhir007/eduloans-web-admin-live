'use strict';

angular.module('app')
	.controller('searchController', ['$scope', 'searchService', '$cookieStore', '$route', '$location', '$rootScope', 'commonService', 'NgTableParams', 'assignService', 'ENV', 'studentService', function ($scope, searchService, $cookieStore, $route, $location, $rootScope, commonService, NgTableParams, assignService, ENV, studentService){
		var type = $location.search().type;
		var value = $location.search().value;
		value = value.replace(/\[dot\]/g, ".");
		//console.log(type, value);

		var accessToken = $cookieStore.get("access_token");
        var partnerName = $cookieStore.get("partner_name");
		if(accessToken === undefined || !accessToken){
			window.location.href = '/';
		}

		$scope.CurrentDate = new Date();
		$scope.present = new Date().getFullYear();
		$scope.next = new Date().getFullYear() + 1;
		$scope.future = new Date().getFullYear() + 2;
		$scope.test = [];
		$scope.lead = [];
		$scope.calling = [];

		var postParams =  {
			'type': type,
			'value': value
		};

		$scope.leadShow = true;

		$scope.getSearchData = function(){
			return searchService.authenticateUser(postParams, accessToken)
    		.then(function(data){
							if(data.length){
									data.forEach(function(value){
											if(value['type'] == "lead")
												{
													$scope.lead.push(value);
												}
												else
												{
												$scope.calling.push(value);
												}
										$scope.tableParams = new NgTableParams({}, {dataset: $scope.lead});
									})
							} else{
									alert("No lead found");
							}

				// if(data.length){
				// 	if(data[0].type != "lead") {
				// 		$scope.leadShow = false;
				// 	}
				// 	//delete data['type'];
				// 	$scope.isViewLoading = true;
				// 	$scope.tableParams = new NgTableParams({}, {dataset: data});
				// }
				// else{
				// 	alert("No lead found");
				// }
    		 });
		};
		$scope.getleads = function(){
			$scope.leadShow = true;
				$scope.tableParams = new NgTableParams({}, {dataset: $scope.lead});
		}
		$scope.getcalling = function(){
				$scope.leadShow = false;
				$scope.tableParams = new NgTableParams({}, {dataset: 	$scope.calling});
		}

		$scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};

		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

		$scope.agentsList = function(){
			var postParams =  {};
    		var api = ENV.apiEndpoint + '/agentslist';
    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){
    			$scope.agentslist = data;
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.mailData = function(leadData){
			$scope.error = {
				message: null
			};

			if(leadData.facilitator_id === undefined){
				var facilitator_id = leadData.assigned_to;
				var lead_type = 'calling';
				var lead_id = leadData.id;
				var name = leadData.name;
				var email = leadData.email;
				var mobile = leadData.mobile;
			}
			else{
				var facilitator_id = leadData.facilitator_id;
				var lead_type = 'lead';
				var lead_id = leadData.lead_id;
				var name = leadData.name;
				var email = leadData.email;
				var mobile = leadData.mobile_number;
			}

			if(!facilitator_id){
				alert("This lead has not been assigned yet");
			}
			else{
				var postParams =  {
					'lead_id': lead_id,
					'facilitator_id': facilitator_id,
					'type': lead_type,
					'name': name,
					'email': email,
					'mobile': mobile
				};

				return searchService.sendMailData(postParams, accessToken)
				.then(function(data){
					alert("mail has been sent");
				})
			}
		}
		$scope.claculateTime = function(dt) {
			return new Date(dt).getTime();
		};

		$scope.selectEntity = function (appId, checkedvalue) {
			if(checkedvalue){
				$scope.editData(appId, "MULTIPLE");
				$scope.test.push(appId.lead_id);
			} else {
				for(var i = 0; i < $scope.test.length; i++){
					if($scope.test[i]==appId.lead_id){
						$scope.test.splice(i,1);
						appId.isEditable = false;
					}
				}
			}

		};

		$scope.editData = function (appId, selection) {
			appId.isEditable = true;
			console.log($scope.test);
			if(selection == 'MULTIPLE'){
				if($scope.test.indexOf(appId.lead_id) === -1){
					$scope.test.push(appId.lead_id);
				}
			}
			else{
				$scope.test = [];
				$scope.test.push(appId.lead_id);
			}
		};

		$scope.cancelEditData = function (appId) {
				appId.isEditable = false;
				//$scope.isEditable = true;
				for(var i = 0; i < $scope.test.length; i++){
					if($scope.test[i]==appId.lead_id){
						$scope.test.splice(i,1);
						appId.isEditable = false;
					}
				}
		};

		$scope.saveData = function (appValue) {
			$scope.tableParams.data.forEach(function (params) {
				params.isEditable = false;
				$scope.checkedvalue = false;
			});
			$scope.test.forEach(function(index, key){
				var postParams = {
					"id": index,
					"facilitator_id": appValue.facilitator_id,
					"intake": appValue.intake,
					"mobile": appValue.mobile_number,
					"email": appValue.email
				};

				var api = ENV.apiEndpoint + '/lead/facupdate';
				return studentService.updateCurrentStatus(api, postParams, accessToken)
				.then(function (data) {
					//console.log(data);
					let update = false;
					$scope.tableParams.data.forEach(function(leadDetail, tableKey){
						if(leadDetail.lead_id == index){
							$scope.agentslist.forEach(function(value){
								if(value.id == appValue.facilitator_id){
									$scope.tableParams.data[tableKey]['partner_name'] = value.first_name + ' ' + value.last_name;
									update = true;
								}
							})
						}
					});
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
			})
		};

		$scope.saveDataCalling = function(appValue){

			var postParams = {
				"id": appValue.id,
				"facilitator_id": appValue.assigned_to,
				"mobile": appValue.mobile,
				"email": appValue.email
			};

    		var api = ENV.apiEndpoint + '/lead/facupdatecalling';

    		return studentService.updateCurrentStatus(api, postParams, accessToken)
    		.then(function(data){
    			//$scope.agentslist = data;
					alert("Success...!!");
					$route.reload();
          //console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.editDataCalling = function (emp) {
      emp.isEditable = true;
        //$scope.isEditable = true;
    };

		$scope.cancelEditDataCAlling = function (emp) {
      emp.isEditable = false;
        //$scope.isEditable = true;
    };

		$scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();
		$scope.agentsList();
		$scope.getSearchData();
}]);
