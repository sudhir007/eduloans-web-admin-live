'use strict';

angular.module('app')
	.controller('sbiController', ['$scope', 'leadService', 'ENV', '$cookieStore', 'commonService', '$routeParams', 'NgTableParams', '$compile', '$route', '$location', '$rootScope', function ($scope, leadService, ENV, $cookieStore, commonService, $routeParams, NgTableParams, $compile, $route, $location, $rootScope){
		var accessToken = $cookieStore.get("access_token");
		var leadId = $routeParams.id ? $routeParams.id : 0;
		var productId = $routeParams.pid ? $routeParams.pid : '0';
		var partnerName = $cookieStore.get("partner_name");
        var loginId = $cookieStore.get("pId");

        $scope.getLeadData = function(){
            return leadService.getLeadData(accessToken, leadId, productId)
            .then(function(leadData){
                $scope.leadData = leadData;
                $scope.getlistIds();
            });
        }
     $scope.getlistIds = function(){
         var listArray = [1, 362, 4, 366, 350, 328, 334, 360, 326, 357, 359, 333];
         return leadService.getleadIds(listArray)
         .then(function(listData){
			 var countryId = $scope.leadData.opted_university_create_object[0].country_id;
			 var courseId = $scope.leadData.opted_university_create_object[0].degree_id;
			 var universityId = $scope.leadData.opted_university_create_object[0].university_id;
			 var genId = $scope.leadData.customer_object.gender_id;
			 var relationshipId = $scope.leadData.co_applicant_object.customer_relationship_id;
			 var admissionId = $scope.leadData.customer_object.other_details.admission_quota;
			 var educationId = $scope.leadData.opted_university_create_object[0].education_type;
			 var qualificationId = $scope.leadData.customer_object.other_details.highest_qualification_id;
			 var existingEmployeeId = $scope.leadData.co_applicant_object.other_details.existing_sbi_employee;
			 var aadhaarConfirmationId = $scope.leadData.customer_pan_object.other_details.has_aadhaar;
			 var coApplicantAadhaarConfirmationId = $scope.leadData.co_applicant_object.other_details.has_aadhaar;
			 var coApplicantOtherIDProof = $scope.leadData.co_applicant_object.other_details.other_id_proof;
			 var otherIDProof = $scope.leadData.customer_pan_object.other_details.other_id_proof;
			 var occupationId = $scope.leadData.co_applicant_occupation_detail_object.occupation_id;

             listData[0].forEach(element => {
                 if(countryId == element.id){
                    $scope.countryName = element.display_name;
                 }
             });
             listData[1].forEach(element => {
                 if(courseId == element.id){
                    $scope.courseName = element.display_name;
                 }
             });
             listData[2].forEach(element => {
                 if(educationId == element.id){
                    $scope.educationName = element.display_name;
                 }
             });
             listData[3].forEach(element => {
                 if(universityId == element.id){
                    $scope.universityName = element.display_name;
                 }
             });
             listData[4].forEach(element => {
                if(educationId == element.id){
                   $scope.educationName = element.display_name;
                }
            });
             listData[5].forEach(element => {
                 if(qualificationId == element.id){
                    $scope.qualificationName = element.display_name;
                 }
             });
             listData[6].forEach(element => {
                 if(genId == element.id){
                    $scope.genName = element.display_name;
                 }
             });
             listData[7].forEach(element => {
                 if(relationshipId == element.id){
                    $scope.relationshipName = element.display_name;
                 }
             });
             listData[8].forEach(element => {
                 if(admissionId == element.id){
                    $scope.admissionName = element.display_name;
                 }
             });
			 listData[9].forEach(element => {
			   	 if(existingEmployeeId == element.id){
				  	$scope.existingEmployee = element.display_name;
			   	 }
				 if(aadhaarConfirmationId == element.id){
 				   $scope.aadhaarConfirmation = element.display_name;
 				 }
				if(coApplicantAadhaarConfirmationId == element.id){
				  	$scope.coApplicantAadhaarConfirmation = element.display_name;
			   	}
		    });
			listData[10].forEach(element => {
				if(otherIDProof == element.id){
				   $scope.otherIDProofValue = element.display_name;
				}
				if(coApplicantOtherIDProof == element.id){
				   $scope.coApplicantOtherIDProofValue = element.display_name;
				}
		    });
			listData[11].forEach(element => {
				if(occupationId == element.id){
				   $scope.occupation = element.display_name;
				}
		    });
         })
     }
     $scope.getLeadData();
}]);
