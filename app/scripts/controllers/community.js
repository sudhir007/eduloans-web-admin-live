'use strict';

angular.module('app')
  .controller('communityController', ['$scope', 'commentsService', 'assignService', 'approvalService', 'ENV', '$cookieStore', 'commonService', 'bulkuploadService', '$routeParams', 'NgTableParams', '$compile', '$route', 'menuService', '$location', '$rootScope', '$q','popUpService', function ($scope, commentsService, assignService, approvalService, ENV, $cookieStore, commonService, bulkuploadService, $routeParams, NgTableParams, $compile, $route, menuService, $location, $rootScope, $q, popUpService) {


    if (!$rootScope.bodylayout) {
      $rootScope.bodylayout = "login-page";
    }

    var accessToken = $cookieStore.get("access_token");
    var filter = $route.current.$$route.filter;
    $scope.test = [];

    $scope.rmList = [];
    $scope.facilitator_email = [];
    $scope.rmSetting = { displayProp: 'email', idProperty:'id', enableSearch: true, selectedToTop: true };


    $scope.getMenu = function () {
      return commonService.getMenu(accessToken)
        .then(function (data) {
          $scope.menus = data;
        })
        .catch(function (error) {
          if (error.error_code === "SESSION_EXPIRED"  || error.error_code === "HEADER_MISSING") {
            window.location.href = '/';
          }
        });
    };

    $scope.toggleMenu = function (id) {
      $('#menu-' + id).toggle();
    };
    $scope.toggleSubMenu = function (id) {
      $('#submenu-' + id).toggle();
    };

    $scope.communityList = function(){

        var apiEndpoint = "/community/list";

        return approvalService.getdatabyget(apiEndpoint, accessToken)
        .then(function(data){

          var dataset = data;

          $scope.communityTableParams = new NgTableParams({}, {
            counts: [10, 50, 100, 500, 1000],
            dataset: dataset
          });

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.communityPostList = function(communityId){

        var apiEndpoint = "/community/posts/"+communityId;

        return approvalService.getdatabyget(apiEndpoint, accessToken)
        .then(function(data){

          $scope.communityList = data.community_list
          var dataset = data.post_list;

          $scope.communityPostTableParams = new NgTableParams({}, {
            counts: [10, 50, 100, 500, 1000],
            dataset: dataset
          });

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.showPostBycommunityId = function(){

      var communityId = $scope.community_id;

      $scope.communityPostList(communityId);

    };

    $scope.communityPostActionList = function(postId){

        var apiEndpoint = "/community/postaction/"+postId;

        return approvalService.getdatabyget(apiEndpoint, accessToken)
        .then(function(data){

          var dataset = data;

          $scope.postActiontableParams = new NgTableParams({}, {
            counts: [10, 50, 100, 500, 1000],
            dataset: dataset
          });

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.communityStudentList = function(communityId){

        var apiEndpoint = "/community/studentListInCommunty/"+communityId;

        return approvalService.getdatabyget(apiEndpoint, accessToken)
        .then(function(data){

          var dataset = data;

          $scope.communityStudentTableParams = new NgTableParams({}, {
            counts: [10, 50, 100, 500, 1000],
            dataset: dataset
          });

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.showStudentBycommunityId = function(){

      var communityId = $scope.community_id;

      $scope.communityStudentList(communityId);

    };

    switch (filter) {

      case 'community-list':
        $scope.title = "Community List ";
        $scope.communityList();
        break;

      case 'community-post-list':
        $scope.title = "Counselor List";
        var communityId = $routeParams.community_id;
        $scope.communityPostList(communityId);
        break;

     case 'community-post-action-list':
          $scope.title = "Community Post Action List";
          var postId = $routeParams.post_id;
          $scope.communityPostActionList(postId);
          break;

      case 'community-student-list':
        $scope.title = "Community Student List";
        var communityId = $routeParams.community_id;
        $scope.communityStudentList(communityId);
        break;

      case 'community-create':
          $scope.title = "Community Create";
          $scope.communityCreateData();
          break;

    };

    $scope.viewFile = $route.current.$$route.pageName;
    $scope.location = $location.path();
    $scope.header = 'views/header.html';
    $scope.menu = 'views/menu.html';
    $scope.footer = 'views/footer.html';
    $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
    $scope.searchApi = ENV.apiEndpoint + 'cities/';
    $scope.getMenu();
    //$scope.getCounselorDocuments();
  }]);
