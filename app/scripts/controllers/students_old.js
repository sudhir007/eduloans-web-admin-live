'use strict';

angular.module('app')
	.controller('studentController', ['$scope', 'studentService', 'ENV', '$cookieStore', '$route', 'menuService', 'NgTableParams', 'applicationService', '$rootScope', '$location', '$routeParams', function ($scope, studentService, ENV, $cookieStore, $route, menuService, NgTableParams, applicationService, $rootScope, $location, $routeParams){
		var accessToken = $cookieStore.get("access_token");
		var partnerName = $cookieStore.get("partner_name");
        var filter = $route.current.$$route.filter;

        switch(filter){
            case 'collateral':
                $scope.title = "Collateral Applications";
                break;
            case 'non-collateral':
                $scope.title = "Non Collateral Applications";
                break;
            case 'us-cosignor':
                $scope.title = "US Cosignor Applications";
                break;
            case 'bob':
                $scope.title = "Bank Of Baroda Applications";
                break;
            case 'sbi':
                $scope.title = "State Bank Of India Applications";
                break;
            case 'other-banks':
                $scope.title = "Other Banks Applications";
                break;
        }

		$scope.getMenu = function(){
			var api = ENV.apiEndpoint + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.getDashboardData = function(){
			var api = ENV.apiEndpoint + '/dashboard';

			return applicationService.getData(api, accessToken)
			.then(function(data){
				$scope.dashboardData = data;
				var dataset = [];
                data.forEach(function(tableData, key){
					var params = {};
					params.application_id = key + 1;
                    params.university = "MIT";
                    params.course = "AI";
                    params.degree = "MS";
                    params.status = "Potential";
                    params.bank = "BOB";
                    params.loan_type = "Collateral";
                    params.loan_amount = "5000000";
                    params.comments = "Comment-1";
					dataset.push(params);
				});
                data.forEach(function(tableData, key){
					var params = {};
					params.application_id = key + 10;
                    params.university = "US University";
                    params.course = "CS";
                    params.degree = "Master";
                    params.status = "Application Review";
                    params.bank = "SBI";
                    params.loan_type = "Non Collateral";
                    params.loan_amount = "6000000";
                    params.comments = "Comment-4";
					dataset.push(params);
				});
				var initialParams = {
        			count: 5 // initial page size
      			};
      			var initialSettings = {
        			// page size buttons (right set of buttons in demo)
        			counts: [5, 10, 15, 20, 25],
        			// determines the pager buttons (left set of buttons in demo)
        			paginationMaxBlocks: 13,
        			paginationMinBlocks: 2,
        			dataset: dataset
      			};
                $scope.tableParams = new NgTableParams(initialParams, initialSettings);
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

        $scope.isEditable = false;
        $scope.editData = function () {
            $scope.isEditable = true;
        };
        $scope.saveData = function () {
            $scope.isEditable = false;
            console.log($scope.comment);
        };

        $scope.allStatus = [
            {
                id: 1,
                status: "REFERRED"
            },
            {
                id: 2,
                status: "INTERESTED"
            },
            {
                id:3,
                status: "POTENTIAL"
            }
        ];

        $scope.comments = [
            {
                id: 1,
                value: "Comment-1"
            },
            {
                id: 2,
                value: "Comment-2"
            }
        ];

        $scope.changeName = function(comment){
            console.log(comment);
        };

		$scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";

		console.log("Student Id - " + $routeParams.id);

		$scope.getMenu();
		$scope.getDashboardData();
    }]);
