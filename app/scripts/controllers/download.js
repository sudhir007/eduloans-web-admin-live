'use strict';

angular.module('app')
    .controller('downloadController', ['$scope', 'downloadService', 'ENV', '$cookieStore', '$route', 'menuService', 'NgTableParams', '$rootScope', '$location', function ($scope, downloadService, ENV, $cookieStore, $route, menuService, NgTableParams, $rootScope, $location){
    	var accessToken = $cookieStore.get("access_token");
        var partnerName = $cookieStore.get("partner_name");

    	$scope.getMenu = function(){
    		var api = ENV.apiEndpoint + '/menu';

    		return menuService.getMenu(api, accessToken)
    		.then(function(data){
    			$scope.menus = data;
    		});
    	};

        $scope.export = function(){
            var quotes = document.getElementById('download-section');
            html2canvas(quotes, {
                onrendered: function(canvas) {
                //! MAKE YOUR PDF
                var pdf = new jsPDF('p', 'pt', [1200, 1800], true);

                for (var i = 0; i <= quotes.clientHeight/980; i++) {
                    //! This is all just html2canvas stuff
                    var srcImg  = canvas;
                    var sX      = 0;
                    var sY      = 1600*i; // start 980 pixels down for every new page
                    var sWidth  = 1500;
                    var sHeight = 1500;
                    var dX      = 120;
                    var dY      = 70;
                    var dWidth  = 1000;
                    var dHeight = 1400;

                    window.onePageCanvas = document.createElement("canvas");
                    onePageCanvas.setAttribute('width', 1200);
                    onePageCanvas.setAttribute('height', 1700);
                    var ctx = onePageCanvas.getContext('2d');
                    ctx.webkitImageSmoothingEnabled = true;
                    ctx.mozImageSmoothingEnabled = true;
                    ctx.imageSmoothingEnabled = true;
                    ctx.imageSmoothingQuality = "high";
                    // details on this usage of this function:
                    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
                    ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);

                    // document.body.appendChild(canvas);
                    var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

                    var width         = onePageCanvas.width;
                    var height        = onePageCanvas.height;
                    console.log(width, height, "Client");

                    //! If we're on anything other than the first page,
                    // add another page
                    if (i > 0) {
                        pdf.addPage(1200, 1800); //8.5" x 11" in pts (in*72)
                    }
                    //! now we declare that we're working on that page
                    pdf.setPage(i+1);
                    //! now we add content to that page!
                    pdf.addImage(canvasDataURL, 'PNG', 0, 0, width, height, '', 'FAST');

                }
                //! after the for loop is finished running, we save the pdf.
                pdf.save('test.pdf');
            }
          });
            /*html2canvas(document.getElementById('download-section'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var width = canvas.width;
                    var height = canvas.height;
                    console.log(width, height, "wh");
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: width,
                            height: height,
                            fit: [width, height]
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("test.pdf");
                }
            });*/
        }

        $scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

        console.log("I m in download");
        $scope.viewFile = $route.current.$$route.pageName;
        $scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();
    }]);
