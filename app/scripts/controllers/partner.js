'use strict';

angular.module('app')
.controller('partnerController', ['$scope', 'leadService', 'studentService', 'ENV', '$cookieStore', 'commonService', '$routeParams', 'NgTableParams', '$compile', '$route', 'menuService', '$location', '$rootScope', '$q', 'popUpService', function ($scope, leadService, studentService, ENV, $cookieStore, commonService, $routeParams, NgTableParams, $compile, $route, menuService, $location, $rootScope, $q, popUpService) {
    var accessToken = $cookieStore.get("access_token");
    var loginId = $cookieStore.get("pId");
    var leadId = $routeParams.id ? $routeParams.id : 0;
    var filter = $route.current.$$route.filter;

    $scope.getMenu = function () {
        return commonService.getMenu(accessToken)
            .then(function (data) {
                $scope.menus = data;
            })
            .catch(function (error) {
                if (error.error_code === "SESSION_EXPIRED") {
                    window.location.href = '/';
                }
            });
    };
    $scope.getunassignedpartner = function (){
        var api = ENV.apiEndpoint + '/unassigned';
         leadService.getData(api, accessToken)
        .then(function(data){
            $scope.names = data;
            $scope.getrmlist();
        })
    };
    $scope.checkedModels = {};
    $scope.updaterm = function(key, partnerId, partnerLoginId, rmId, leadAssignment){
        $scope.loader = true;
        var rmIds = [];
        for(let keyIndex in $scope.checkedModels){
            let splitKeyIndex = keyIndex.split('|');
            if(splitKeyIndex[0] == partnerId && $scope.checkedModels[keyIndex])
            {
                rmIds.push(splitKeyIndex[1]);
            }
        }
        var api = '/rm-update';
        var postParams = {
            "partner_id": partnerId,
            "rm_id":rmIds,
            "login_id": partnerLoginId,
            "changed_rm_id": rmId,
            "lead_assignment": leadAssignment
        };

        return leadService.update(accessToken,api,postParams)
        .then(function (response) {
            alert("Success");
            $scope.loader = false;
        })
        .catch(function (err) {
            alert("Error");
            $scope.loader = false;
        });
    }
    $scope.getrmlist = function(){
        var api = ENV.apiEndpoint + '/rm';
        leadService.getData(api, accessToken)
        .then(function(data){
            $scope.rmlist = data;
            var tableArray = [];
            $scope.names.forEach(function(unassignedPartner, index){
                let tableObj = {};
                tableObj.name = unassignedPartner['name'];
                tableObj.company_name = unassignedPartner['company_name'];
                tableObj.lead_assignment = unassignedPartner['lead_assignment'];
                tableObj.id = unassignedPartner['id'];
                tableObj.login_id = unassignedPartner['login_id'];
                if(unassignedPartner.rm_id){
                    unassignedPartner.rm_id = angular.fromJson(unassignedPartner.rm_id);
                }
                else{
                    unassignedPartner.rm_id = [];
                }
                $scope.rmlist.forEach(function(rmData){
                    tableObj[rmData.id] = 0;
                    $scope.checkedModels[tableObj.id+'|'+rmData.id]=false;
                    unassignedPartner.rm_id.forEach(function(value, key){
                        if(value == rmData.id){
                            tableObj[rmData.id] = 1;
                            $scope.checkedModels[tableObj.id+'|'+rmData.id]=true;
                        }
                    });
                })
                tableArray.push(tableObj);
            });
            $scope.tableParams = new NgTableParams({}, {dataset: tableArray});
        })

    };
    $scope.assign = function(partner,rmname){
        var api = '/rm-update';
        if(!partner || !rmname){
            alert("please Select ");
        }
        var partnerLoginId = '';
        $scope.names.forEach(function(data){
            if(data.id == partner)
            {
                partnerLoginId = data.login_id
            }
        })
        var postparams = {
            "partner_id": partner,
            "rm_id":rmname,
            "login_id": partnerLoginId
        };
        return leadService.update(accessToken,api,postparams)
        .then(function (response) {
            alert("Success");

        })
        .catch(function (err) {
            alert("Error");
        });

    }
    $scope.toggleMenu = function (id) {
        $('#menu-' + id).toggle();
    };
    $scope.toggleSubMenu = function (id) {
        $('#submenu-' + id).toggle();
    };

    $scope.uploadBOBStatusReport = function(){
        $scope.error = {
            message: null
        };

        var file = $scope.file;

        var api = ENV.apiEndpoint + '/upload-status-report';
        return commonService.uploadStatusReport(api, file,  accessToken, 10)
        .then(function(data){
            alert(data);
        })
        .catch(function(error){
            $scope.error = {
                message: error.message
            };
        });
    };

    $scope.editRecord = function(rowData)
    {
        rowData.isEditable = true;
    }

    $scope.saveEditRecord = function(rowData)
    {
        $scope.loader = true;
        var api = '/rm-update';
        var postParams = {
            "partner_id": rowData.id,
            "lead_assignment": rowData.lead_assignment
        };


        return leadService.update(accessToken,api,postParams)
        .then(function (response) {
            $scope.loader = false;
            alert("Success");
            rowData.isEditable = false;
        })
        .catch(function (err) {
            alert("Error");
            $scope.loader = false;
        });
    }

    $scope.viewFile = $route.current.$$route.pageName;
	$scope.location = $location.path();
	$scope.header = 'views/header.html';
	$scope.menu = 'views/menu.html';
	$scope.footer = 'views/footer.html';
	$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
    $scope.getMenu();
    $scope.getunassignedpartner();
    if(leadId){
        $scope.partnername = leadId;
    }
}]);
