'use strict';

angular.module('app')

.directive('fileModel', ['$parse',
    function ($parse) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;

          element.bind('change', function(){
            scope.$apply(function(){
              if (element[0].files.length > 1) {
                modelSetter(scope, element[0].files);
              }
              else {
                modelSetter(scope, element[0].files[0]);
              }
            });
          });
        }
      };
    }
  ])

	.controller('bulkuploadController', ['$scope', 'bulkuploadService', 'ENV', '$cookieStore',  '$route',  'menuService', 'assignService', 'NgTableParams', '$rootScope', '$location', function ($scope, bulkuploadService, ENV, $cookieStore,  $route, menuService, assignService, NgTableParams,  $rootScope, $location){
		//angular.element(document.body).addClass("login-page");
		/*if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
		}*/

    var accessToken = $cookieStore.get("access_token");
    var filter = $route.current.$$route.filter;

    $scope.getMenu = function(){
			var api = ENV.apiEndpoint + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
      .catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};


		$scope.uploadFile = function(userFile){

            $scope.error = {
                message: null
            };

      var sourceId = $scope.select_source;
      var sourceType = $scope.select_type ? $scope.select_type : 0;
      var collOrAgentId = $scope.select_coll_agent ? $scope.select_coll_agent : 0;

      var file = $scope.userfile;
      console.log('file is '+ file );
               console.dir(file);

    		var api = ENV.apiEndpoint + '/callingdata';

    		return bulkuploadService.fileUpload(api, file, sourceId, sourceType, collOrAgentId, accessToken)
    		.then(function(data){

    			window.location.href='/assign/data';
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
        };

        $scope.uploadImperialFile = function(){

                $scope.error = {
                    message: null
                };

          var branchId = $scope.branch_id;


          var file = $scope.userfile;
          console.log('file is '+ file );
          console.dir(file);

        		var api = ENV.apiEndpoint + '/imperial/upload/leads';

        		return bulkuploadService.fileImperialUpload(api, file, branchId, accessToken)
        		.then(function(data){
              alert("Uploaded Successfully ...!!!");
        			//window.location.href='/assign/data';
        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
            };

        $scope.uploadMarketingEmailFile = function(){

                $scope.error = {
                    message: null
                };

          var sourceName = $scope.source_name;

          var file = $scope.userfile;
          console.log('file is '+ file );
          console.dir(file);

        		var api = ENV.apiEndpoint + '/marketing/email/upload';

        		return bulkuploadService.marketingEmailFileUpload(api, file, sourceName, accessToken)
        		.then(function(data){

        			window.location.href='/marketing/email/send';
        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
            };


        $scope.uploadBankRsponseFile = function(userFile){

                $scope.error = {
                    message: null
                };

          var bankName = $scope.select_bank;

          var file = $scope.userfile;
          console.log('file is '+ file );
                   console.dir(file);

        		var api = ENV.apiEndpoint + '/upload/bankresponse';

        		return bulkuploadService.fileUploadBankResponse(api, file, bankName, accessToken)
        		.then(function(data){

        			//window.location.href='/assign/data';
              alert('Uploaded Successfully ...!!!');
        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
            };

    $scope.unassigndatadropdown = function(){

    			var postParams =  {};

        		var api = ENV.apiEndpoint + '/allsource/1';

        		return assignService.unassigndata(api, postParams, accessToken)
        		.then(function(data){
        			$scope.dropdowndata = data.data;

              console.log(dropdowndata);

        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};

        $scope.collegeListOrAgentList = function(){

          if($scope.select_type == 'SEMINAR' ){

            $('#agentcolid').removeClass('hidden');
            var api = ENV.apiEndpoint + '/indianCollegeOrAgentList/1';

          } else if ($scope.select_type == 'COUNSELOR') {

            $('#agentcolid').removeClass('hidden');
            var api = ENV.apiEndpoint + '/indianCollegeOrAgentList/2';

          } else if ($scope.select_type == 'UNIVERSITY') {

            $('#agentcolid').removeClass('hidden');
            var api = ENV.apiEndpoint + '/indianCollegeOrAgentList/3';

          } else {

            $('#agentcolid').addClass('hidden');
            return;
          }

          var postParams = {};

          return assignService.unassigndata(api, postParams, accessToken)
          .then(function(data){
            $scope.collagentlist = data.data;

            console.log(dropdowndata);

          })
          .catch(function(error){
                  $scope.error = {
                      message: error.message
                  };
          });

        };


        $scope.sendMarketingEmail = function(){

          var postParams = {
            "email_subject" : $scope.email_subject,
            "email_body" : $scope.email_body,
            "email_limit" : $scope.email_limit,
            "email_from" : $scope.email_from
          };

          console.log(postParams);

          var api = ENV.apiEndpoint + "/marketing/email/send";

          return assignService.assignData(api, postParams, accessToken)
          .then(function(data){


          })
          .catch(function(error){
                  $scope.error = {
                      message: error.message
                  };
          });

        };

        $scope.getUnsendEmailCount = function(){

          var postParams = {};
          var api = ENV.apiEndpoint + "/marketing/email/remainingcount";

          return assignService.unassigndata(api, postParams, accessToken)
          .then(function(data){
            $scope.remaining_email_count = data.data.remaining_email_count;

          })
          .catch(function(error){
                  $scope.error = {
                      message: error.message
                  };
          });

        };

      switch(filter){

              case 'marketing-email-upload':
                  $scope.title = "Marketing Email Upload";
                  break;

              case 'marketing-email-send':
                  $scope.title = "Marketing Email Send";
                  $scope.getUnsendEmailCount();

                  break;

              default:
              $scope.unassigndatadropdown();

        }

    $scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

    $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();
    //$scope.unassigndatadropdown();
    //$scope.fileChange();

}]);
