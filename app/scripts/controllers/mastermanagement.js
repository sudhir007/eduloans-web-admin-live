'use strict';

angular.module('app')
	.controller('masterManagementController', ['$scope', 'masterManagementService', 'ENV', '$cookieStore',  '$route',  'menuService', 'NgTableParams', '$rootScope', '$routeParams', '$location', function ($scope, masterManagementService, ENV, $cookieStore,  $route, menuService, NgTableParams,  $rootScope, $routeParams, $location){
		console.log("In Master Management");
        $scope.viewFile = $route.current.$$route.pageName;

				var accessToken = $cookieStore.get("access_token");
		    var filter = $route.current.$$route.filter;

				$scope.country_list = [];
				$scope.countrySetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true };

				$scope.university_status_list = [];
				$scope.universityStatusSetting = { displayProp: 'value', idProperty:'id' };

				$scope.university_list = [];
				$scope.universityListSetting = { displayProp: 'university_display_name', idProperty:'id', enableSearch: true };

				$scope.course_list = [];
				$scope.courseListSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true };

				$scope.getMenu = function(){
					var api = ENV.apiEndpoint + '/menu';

					return menuService.getMenu(api, accessToken)
					.then(function(data){
						$scope.menus = data;
					})
					.catch(function(error){
						if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
							window.location.href = '/';
						}
					});
				};

				$scope.masterData = function(type){

		    		var api = ENV.apiEndpoint + type;
						console.log(api,"hello");

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
		    			$scope.masterDataList = data;

							var dataset = data;

		          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

		          $scope.data.forEach(function(employee) {
		            employee.isEditable = false;
		        });

		          //console.log(masterDataList);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataAdd = function(){

						var postParams = {
							"name" : $scope.country_name,
							"display_name" : $scope.country_display_name,
							"code" : $scope.country_code,
							"status" : $scope.country_status
						};

						console.log(postParams,"hello");

		    		var api = ENV.apiEndpoint + '/masterdata/country/add';

						console.log(api,"hello");

		    		return masterManagementService.masterDataAdd(api, postParams, accessToken)
		    		.then(function(data){
							$('#country_name').val('');
			        $('#country_display_name').val('');
							$('#country_code').val('');
			        $('#country_status').val('');
		    			console.log(data);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataUpdate = function(cnt){

						cnt.isEditable = false;

						var postParams = {
							"id" : cnt.id,
							"name" : cnt.name,
							"display_name" : cnt.display_name,
							"code" : cnt.code,
							"status" : cnt.status
						};

						console.log(postParams,"hello");

		    		var api = ENV.apiEndpoint + '/masterdata/country/update';

						console.log(api,"hello");

		    		return masterManagementService.masterDataUpdate(api, postParams, accessToken)
		    		.then(function(data){
							$('#country_name').val('');
			        $('#country_display_name').val('');
							$('#country_code').val('');
			        $('#country_status').val('');
		    			console.log(data);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataUniversity = function(type){

		    		var api = ENV.apiEndpoint + type;
						console.log(api,"hello");

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
		    			$scope.masterDataList = data;

							var dataset = data;

		          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

		          $scope.data.forEach(function(employee) {
		            employee.isEditable = false;
		        });

		          //console.log(masterDataList);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataUpdateUniversity = function(cnt){

						cnt.isEditable = false;

						var postParams = {
							"id" : cnt.id,
							"name" : cnt.name,
							"display_name" : cnt.display_name,
							"code" : cnt.code,
							"status" : cnt.status
						};

						console.log(postParams,"hello");

		    		var api = ENV.apiEndpoint + '/masterdata/universities/update';

						console.log(api,"hello");

		    		return masterManagementService.masterDataUpdate(api, postParams, accessToken)
		    		.then(function(data){
							$('#country_name').val('');
			        $('#country_display_name').val('');
							$('#country_code').val('');
			        $('#country_status').val('');
		    			console.log(data);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataAddUniversity = function(){

						var postParams = {
							"name" : $scope.university_name,
							"display_name" : $scope.university_display_name,
							"code" : $scope.university_code,
							"status" : $scope.university_status
						};

						console.log(postParams,"hello");

		    		var api = ENV.apiEndpoint + '/masterdata/universities/add';

						console.log(api,"hello");

		    		return masterManagementService.masterDataAdd(api, postParams, accessToken)
		    		.then(function(data){
							$('#country_name').val('');
			        $('#country_display_name').val('');
							$('#country_code').val('');
			        $('#country_status').val('');
		    			console.log(data);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataCountryList = function(type){

		    		var api = ENV.apiEndpoint + type;
						console.log(api,"hello");

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
		    			$scope.masterDataList = data;
							$scope.countryList = data;

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataUniversityList = function(type){

		    		var api = ENV.apiEndpoint + type;
						console.log(api,"hello");

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
		    			$scope.masterDataUniversityList = data;

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.searchUniversity = function(){

					$scope.tableParams = new NgTableParams({}, {dataset: []});

					var postParams ={
						"country_list" : $scope.country_list ? $scope.country_list : 0,
						"university_status_list" : $scope.university_status_list ? $scope.university_status_list : 0,
						}

						var api = ENV.apiEndpoint + '/masterdata/universitiesbycontryid';

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){
							var dataset = data;

		          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

		          $scope.data.forEach(function(employee) {
		            employee.isEditable = false;
		        });

						})
						.catch(function(error){

										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.addUniversityToCountry = function(){

					var countryId = $('#country_add_name').val();
					countryId = countryId.substring(7, countryId.length);
					var universityId = $('#university_add_name').val();
					universityId = universityId.substring(7, universityId.length);

					var postParams ={
						'country_id': countryId,
						'university_id': universityId
					};


						var api = ENV.apiEndpoint + '/masterdata/universitiesbycontryid/add';
						console.log(api,"hello");

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){
							window.location.href = '/masterdata/universities/bycountry';

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.associationListIdDelete = function(cnt){

					cnt.isEditable = false;
					var associationId = cnt.association_id;

		    		var api = ENV.apiEndpoint + '/masterdata/association/delete/'+associationId;
						console.log(api,"hello");

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
		    			$scope.masterDataUniversityList = data;

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataCourse = function(type){

		    		var api = ENV.apiEndpoint + type;
						console.log(api,"hello");

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
		    			$scope.masterDataList = data.course_list;

							$scope.categoryList = data.category_course_list

							var dataset = data.course_list;

		          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

		          $scope.data.forEach(function(employee) {
		            employee.isEditable = false;
		        });

		          //console.log(masterDataList);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.getcourseSubCategoryList = function(){

            var categoryId = $scope.course_category_id;
            var apiEndpoint = ENV.apiEndpoint + "/course/subcategory/"+categoryId;

		    		return masterManagementService.masterData(apiEndpoint, accessToken)
		    		.then(function(data){

							$scope.sub_category_course_list = data.data.sub_category_course_list;

							console.log($scope.sub_category_course_list,data,"hell");

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataUpdateCourse = function(cnt){

						cnt.isEditable = false;

						var postParams = {
							"id" : cnt.id,
							"name" : cnt.name,
							"display_name" : cnt.display_name,
							"status" : cnt.status
						};

						console.log(postParams,"hello");

		    		var api = ENV.apiEndpoint + '/masterdata/courses/update';

						console.log(api,"hello");

		    		return masterManagementService.masterDataUpdate(api, postParams, accessToken)
		    		.then(function(data){

		    			console.log(data);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataAddCourse = function(){

						var postParams = {
							"name" : $scope.course_name,
							"display_name" : $scope.course_display_name,
							"course_sub_category_id" : $scope.category_sub_id,
							"status" : $scope.course_status
						};

						console.log(postParams,"hello");

		    		var api = ENV.apiEndpoint + '/masterdata/courses/add';

						console.log(api,"hello");

		    		return masterManagementService.masterDataAdd(api, postParams, accessToken)
		    		.then(function(data){
							$('#course_name').val('');
			        $('#course_display_name').val('');
			        $('#course_status').val('');
		    			console.log(data);

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.masterDataCourseList = function(type){

		    		var api = ENV.apiEndpoint + type;
						console.log(api,"hello");

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
		    			$scope.masterDataCourseList = data;

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.searchCourse = function(){

					var courseId = $('#university_name').val();
					courseId = courseId.substring(7, courseId.length);
					var statusId = $('#course_status').val();


						var api = ENV.apiEndpoint + '/masterdata/coursebyuniversityid/'+courseId+'/'+statusId;
						console.log(api,"hello");

						return masterManagementService.masterData(api, accessToken)
						.then(function(data){
							var dataset = data;

		          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

		          $scope.data.forEach(function(employee) {
		            employee.isEditable = false;
		        });

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.addCourseToUniversity = function(){

					var courseId = $('#course_add_name').val();
					courseId = courseId.substring(7, courseId.length);
					var universityId = $('#university_add_name').val();
					universityId = universityId.substring(7, universityId.length);

					var postParams ={
						'course_id': courseId,
						'university_id': universityId
					};


						var api = ENV.apiEndpoint + '/masterdata/coursebyuniversityid/add';
						console.log(api,"hello");

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){
							window.location.href = '/masterdata/courses/byuniversity';

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				//$scope.isEditable = false;
				$scope.editData = function (emp) {
					emp.isEditable = true;
						//$scope.isEditable = true;
				};

				$scope.cancelEditData = function (emp) {
					emp.isEditable = false;
						//$scope.isEditable = true;
				};

				$scope.countrymasterlist = function(){

		    		var api = ENV.apiEndpoint + '/countrymaster';

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
							var dataset = data.data;
							$scope.masterCountryDataList = data.data;

		          /*$scope.tableParamsState = new NgTableParams({}, {dataset: dataset});

		          $scope.data.forEach(function(employee) {
		            employee.isEditable = false;
		        });*/

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.statemasterlist = function(){

		    		var api = ENV.apiEndpoint + '/statemaster';

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
							var dataset = data.data;
							$scope.masterStateDataList = data.data;

		          $scope.tableParamsState = new NgTableParams({}, {dataset: dataset});

		          $scope.data.forEach(function(employee) {
		            employee.isEditable = false;
		        });

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.addstatelist = function(){

					var postParams ={
						'state_name': $scope.state_name,
						'state_display_name': $scope.state_display_name,
						'country_id': $scope.country_id,
						'state_status' : $scope.state_status
					};


						var api = ENV.apiEndpoint + '/statemasterupsert';

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){
							alert('New State Updated...!!!');

							$('#state_name').val('');
	            $('#state_display_name').val('');
							$('#state_status').val('');

							$scope.statemasterlist();
							//window.location.href = '/masterdata/statemaster';

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.updatestatelist = function(statedata){

					statedata.isEditable = false;

					var postParams ={
						'state_id' : statedata.id,
						'state_name': statedata.name,
						'state_display_name': statedata.display_name,
						'country_id': statedata.mapping_id,
						'state_status' : statedata.status
					};

						var api = ENV.apiEndpoint + '/statemasterupsert';

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){
							//window.location.href = '/masterdata/statemaster';
							alert("Updated Successfully");

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.citymasterlist = function(){

		    		var api = ENV.apiEndpoint + '/citymaster';

		    		return masterManagementService.masterData(api, accessToken)
		    		.then(function(data){
							var dataset = data.data;

		          $scope.tableParamsCity = new NgTableParams({}, {dataset: dataset});

		          $scope.data.forEach(function(employee) {
		            employee.isEditable = false;
		        });

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.addcitylist = function(){

					var postParams ={
						'city_name': $scope.city_name,
						'city_display_name': $scope.city_display_name,
						'state_id': $scope.state_id,
						'city_status' : $scope.city_status
					};


						var api = ENV.apiEndpoint + '/citymasterupsert';

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){
							alert("New City Added...!!!");

							$('#city_name').val('');
	            $('#city_display_name').val('');
	            $('#state_id').val('');
							$('#city_status').val('');

							$scope.citymasterlist();
							//window.location.href = '/masterdata/citymaster';

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.updatecitylist = function(citydata){

					citydata.isEditable = false;

					var postParams ={
						'city_id' : citydata.id,
						'city_name': citydata.name,
						'city_display_name': citydata.display_name,
						'state_id': citydata.state_id,
						'city_status' : citydata.status
					};

						var api = ENV.apiEndpoint + '/citymasterupsert';

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){
							//window.location.href = '/masterdata/statemaster';
							$scope.citymasterlist();
							alert("Updated Successfully");

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};


				$scope.activeUniversityMapping = function(universityId){

					var api = ENV.apiEndpoint + '/active/university/mapping/'+ universityId;
					console.log(api,"active university hello");

					return masterManagementService.masterData(api, accessToken)
					.then(function(data){

						console.log(data,data.data);

						$scope.university_id = data.data.id;
						$scope.university_name = data.data.university_display_name;
						$scope.country_name = data.data.country_name;
						$scope.universityList = data.university_list;


					})
					.catch(function(error){
									$scope.error = {
											message: error.message
									};
					});

				};

				$scope.activeUniversityMappingSubmit = function(){

					var postParams ={
						'university_map_with' : $scope.university_id,
						'university_list_for_map': $scope.university_list,
					};

						var api = ENV.apiEndpoint + '/active/university/mapping/submit';

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){

							alert("University Successfully Mapped. Now You can close this window. Thanks");

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.activeCourseMapping = function(courseId){

					var api = ENV.apiEndpoint + '/active/course/mapping/'+ courseId;

					return masterManagementService.masterData(api, accessToken)
					.then(function(data){

						console.log(data,data.data);

						$scope.course_id = data.data.id;
						$scope.course_name = data.data.display_name;
						$scope.courseList = data.course_list;

					})
					.catch(function(error){
									$scope.error = {
											message: error.message
									};
					});

				};

				$scope.activeCourseMappingSubmit = function(){

					var postParams ={
						'course_map_with' : $scope.course_id,
						'course_list_for_map': $scope.course_list,
					};

						var api = ENV.apiEndpoint + '/active/course/mapping/submit';

						return masterManagementService.masterDataAdd(api, postParams, accessToken)
						.then(function(data){

							alert("Course Successfully Mapped. Now You can close this window. Thanks");

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.DataStatus = [
      {
          id: "1",
          status: "Active"
      },
        {
            id: "0",
            status: "Inactive"
        }
    ];

		$scope.universityStatus = [
		 {
			 id: "1",
			 value: "Active"
		 },
		 {
			 id: "0",
			 value: "Inactive"
		 },
		 {
			 id: "3",
			 value: "Pending"
		 },
		 {
			 id: "4",
			 value: "Rejected"
		 },

	 ];



				switch(filter){

					case 'country-active':
							$scope.title = "Active Countries";
							$scope.masterData('/masterdata/country/1');
							break;
            case 'country-inactive':
                $scope.title = "Inactive Countries";
								$scope.masterData('/masterdata/country/0');
                break;
            case 'country-pending':
                $scope.title = "Pending Countires";
								$scope.masterData('/masterdata/country/2');
                break;
            case 'country-rejected':
                $scope.title = "Rejected Countries";
								$scope.masterData('/masterdata/country/3');
                break;
						case 'active-university-mapping':
								$scope.title = "Active University Mapping";
								var universityId = $routeParams.university_id;
								$scope.activeUniversityMapping(universityId);
								break;
					 case 'university-list':
		             $scope.title = "Universities List";
						   	 $scope.masterDataUniversity('/masterdata/universities');
		             break;
					 case 'university-bycountry-list':
	 							$scope.title = "Universities List";
								$scope.masterDataCountryList('/masterdata/country/1');
								$scope.masterDataUniversityList('/masterdata/universities');
			 					break;
					case 'active-course-mapping':
								$scope.title = "Active Course Mapping";
								var courseId = $routeParams.course_id;
								$scope.activeCourseMapping(courseId);
								break;
					case 'course-list':
		 		        $scope.title = "Course List";
		 						$scope.masterDataCourse('/masterdata/courses');
		 		        break;
				  case 'course-byuniversity-list':
								$scope.title = "Course List";
								$scope.masterDataUniversityList('/masterdata/universities');
								$scope.masterDataCourseList('/masterdata/courses');
								break;
					case 'statemaster-list':
								$scope.title = "State List";
								$scope.countrymasterlist();
								$scope.statemasterlist();
								break;

					case 'citymaster-list':
								$scope.title = "City List";
								$scope.statemasterlist();
								$scope.citymasterlist();
								break;
        };

				$scope.toggleMenu = function(id){
					$('#menu-'+id).toggle();
				};
				$scope.toggleSubMenu = function(id){
					$('#submenu-'+id).toggle();
				};



		    //$scope.title = "My Assign Calling List";
		    $scope.viewFile = $route.current.$$route.pageName;
				$scope.location = $location.path();
				$scope.header = 'views/header.html';
				$scope.menu = 'views/menu.html';
				$scope.footer = 'views/footer.html';
				$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
				$scope.getMenu();


    }]);
