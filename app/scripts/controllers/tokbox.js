'use strict';

angular.module('app')
.controller('tokboxController',  ['$scope', '$location', '$rootScope', 'commonService', 'tokboxService', 'dashboardService', '$cookieStore', 'menuService', '$route', '$routeParams', 'listService', 'ENV', 'NgTableParams', function ($scope, $location, $rootScope, commonService, tokboxService, dashboardService, $cookieStore, menuService, $route, $routeParams, listService, ENV, NgTableParams){

        var accessToken = $cookieStore.get("access_token");
        var partnerName = $cookieStore.get("partner_name");
        var roleId = $cookieStore.get("rId");
				var loginFromId = $cookieStore.get("login_id");
        var filter = $route.current.$$route.filter;

        $scope.role_id = roleId;
				$scope.login_from_id = loginFromId;
				$scope.entity_name = "";
				$scope.partner_name = partnerName;


				$scope.source_list_id = [];
				$scope.sourceListSetting = { displayProp: 'display_name', idProperty:'id' };

				$scope.product_id = [];
				$scope.lead_state_id = [];
				$scope.calling_lead_state_id = [];
				$scope.lead_state_bankwise_id = [];
				$scope.lead_rm_id = [];
				$scope.entity_list_id = [];
				$scope.rm_report_type = [];
				$scope.leadStateSetting = { displayProp: 'status', idProperty:'id' };
				$scope.callingleadStateSetting = { displayProp: 'lead_state', idProperty:'id' };
				$scope.productSetting = { displayProp: 'display_name', idProperty:'id' };
				$scope.rmSetting = { displayProp: 'email', idProperty:'id' };
				$scope.entityListSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true };
				$scope.rmReportTypeSetting = { displayProp: 'value', idProperty:'id' };
				$scope.showTable = false;
				$scope.showTableApplication = false;

				//calling data
				$scope.lead_state_calling = [];
				$scope.leadStateSettingCalling = { displayProp: 'lead_state', idProperty:'id' };

        $scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
    };

    $scope.tokboxInfo = function(loginId){

      var apiUrl = ENV.apiEndpoint + "/tokbox/token/"+loginId;

  		return tokboxService.getData(apiUrl, accessToken)
		  .then(function(data){
				//console.log("hello", data);
		    $scope.tokboxdata = data;
				$scope.entity_name = data.username_to;
				console.log("hello_name", $scope.entity_name);
				//console.log("hello", $scope.tokboxdata);
		  })
		  .catch(function(error){
		    if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
		      window.location.href = '/';
		    }
		  });
		  };

			$scope.chatList = function(loginId){

	      var apiUrl = ENV.apiEndpoint + "/chat/getmsg/"+loginId;

	  		return tokboxService.getData(apiUrl, accessToken)
			  .then(function(data){
					//console.log("hello", data);

			    $scope.chatList = data;
					$('.msger-chat').scrollTop($('.msger-chat')[0].scrollHeight);

			  })
			  .catch(function(error){

			    alert("Error While Sending Chat");

			  });
			  };

				$scope.sendChat = function(){

					var postData = {
						to:$scope.login_to_id,
						msg: $scope.chat_msg
					};

		      var apiUrl = ENV.apiEndpoint + "/chat/postmsg";

		  		return tokboxService.postData(accessToken, apiUrl, postData)
				  .then(function(data){

						console.log("respnse", data);

						$scope.chatList($scope.login_to_id);

						$scope.chat_msg = "";

				  })
				  .catch(function(error){

						if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
 						 window.location.href = '/';
 					 } else{
 						 alert("Error While Sending Chat");
 					 }

				  });
				};

				$scope.updateIframe = function(){

					var tokboxUrl = "https://testadmin.eduloans.org/opentok/index.php?login_id_to=" + $scope.login_to_id + "&login_id_from="+ loginFromId + "&un=" + $scope.partner_name ;
					document.getElementById('tokboxIframe').src = tokboxUrl;

					var apiUrl = ENV.apiEndpoint + "/videocall/notification/"+ $scope.login_to_id;

		  		return tokboxService.getData(apiUrl, accessToken)
				  .then(function(data){
						//console.log("hello", data);

				    $scope.chatList = data;
						$('.msger-chat').scrollTop($('.msger-chat')[0].scrollHeight);

				  })
				  .catch(function(error){
						if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
							window.location.href = '/';
						} else{
							alert("Error While Sending notification");
						}

				  });

				};

				$scope.agentsList = function(){

		    		var api = ENV.apiEndpoint + '/agentslist';

		    		return tokboxService.getDataDirect(api, accessToken)
		    		.then(function(data){

		    			$scope.agentslist = data;

		    		})
		    		.catch(function(error){
		                $scope.error = {
		                    message: error.message
		                };
		    		});
				};

				$scope.hearAboutUsSelect = function(){
		          	// alert("hi");
		          	if(document.getElementById('hear_About_Us').value == "311" || document.getElementById('hear_About_Us').value == "314" || document.getElementById('hear_About_Us').value == "8" || document.getElementById('hear_About_Us').value == "411" || document.getElementById('hear_About_Us').value == "431" )
		            {
		            	// alert("hiiii");
		            	$("#counselorDropDown").css("display", "none");
		            }
		            else{
									$("#counselorDropDown").css("display", "block");
									$scope.hearAboutUsOptionsData(document.getElementById('hear_About_Us').value);
		            }
		        };

						$scope.hearAboutUsOptionsData = function(hearId){

								var api = ENV.apiEndpoint + '/indianCollegeOrAgentList/'+hearId;

							return tokboxService.getData(api, accessToken)
							.then(function(data){
								$scope.hearAboutUsListData = data;

							})
							.catch(function(error){
											$scope.error = {
													message: error.message
											};
							});

						};

						$scope.productsList = function(){

				    		var api = ENV.apiEndpoint + '/productList/1';

				    		return tokboxService.getData(api, accessToken)
				    		.then(function(data){

										$scope.productlists = data;

				    		})
				    		.catch(function(error){
				                $scope.error = {
				                    message: error.message
				                };
				    		});
						};


						$scope.sourceList = function(){

								var api = ENV.apiEndpoint + '/allsource/3';

								return tokboxService.getData(api, accessToken)
								.then(function(data){

										$scope.allSourceList = data;

								})
								.catch(function(error){
												$scope.error = {
														message: error.message
												};
								});
						};


			$scope.allLeadChats = function(){

					var postParams = {
							"hear_about_us_id" : $scope.hear_About_Us ? $scope.hear_About_Us : 0,
							"rm_id" : $scope.lead_rm_id ? $scope.lead_rm_id : 0,
							"entity_id" : $scope.entity_list_id ? $scope.entity_list_id : 0,
							"lead_state" : $scope.lead_state_id ? $scope.lead_state_id : 0,
							"product_list" : $scope.product_id ? $scope.product_id : 0,
							"intake_year_filter" : $scope.intake_year_filter ? $scope.intake_year_filter : "ALL",
							"show_all_leads" : "YES"
					};

					console.log(postParams);

						var api = ENV.apiEndpoint + '/aboutusfilter';

						return tokboxService.postData(accessToken, api, postParams)
						.then(function(data){

							var dataset = data;
							$scope.leadFilterData = new NgTableParams({}, {
								counts: [10, 50, 100, 1000, 5000],
								dataset: dataset
							});

						})
						.catch(function(error){
										$scope.error = {
												message: error.message
										};
						});
				};

				$scope.allCallingLeadChats = function(){

						var postParams = {
								"lead_state" : $scope.calling_lead_state_id ? $scope.calling_lead_state_id : 0,
								"rm_id" : $scope.lead_rm_id ? $scope.lead_rm_id : 0,
								"source_id" : $scope.source_list_id ? $scope.source_list_id : 0
						};

						console.log(postParams);

							var api = ENV.apiEndpoint + '/calling/filter/chatdata';

							return tokboxService.postData(accessToken, api, postParams)
							.then(function(data){

								var dataset = data;

								$scope.callingfilterreportdata = new NgTableParams({}, {
									counts: [10, 50, 100, 1000, 5000],
									dataset: dataset
								});

							})
							.catch(function(error){
											$scope.error = {
													message: error.message
											};
							});
					};


				$scope.allStatus = [
				 {
					 id: "INTERESTED",
					 status: "Interested"
				 },
				 {
					 id: "POTENTIAL",
					 status: "Potential"
				 },
				 {
					 id: "APPLICATION_PROCESS",
					 status: "Application Process"
				 },
				 {
					 id: "APPLICATION_REVIEW"	,
					 status: "Application Review"
				 },
				 {
					 id: "PROVISIONAL_OFFER"	,
					 status: "Provisional Offer"
				 },
				 {
					 id: "DISBURSAL_DOCUMENTATION"	,
					 status: "Disbursal Documentation"
				 },
				 {
					 id: "DISBURSED"	,
					 status: "Disbursed"
				 },
				 {
					 id: "PARTIALLY_DISBURSED"	,
					 status: "Partially Disbursed"
					},
				 {
					 id: "BEYOND_INTAKE"	,
					 status: "Beyond Intake"
				 },
				 {
					 id: "NOT_INTERESTED",
					 status: "Not Interested"
				 },
				 {
					 id: "POTENTIAL_DECLINED",
					 status: "Potential Declined"
				 },
				 {
					 id: "APPLICATION_REVIEW_DECLINED"	,
					 status: "Application Review Declined"
				 },
				 {
						 id:"ACTIVE_LEADS",
						 status: "ACTIVELEADS"
				 },
				 {
						 id:"TOTAL_LEADS",
						 status: "TOTALLEADS"
				 }
			 ];

			 $scope.allStatusCalling = [
				 {
						 id: "FRESH",
						 lead_state: "FRESH"
				 },
					 {
							 id: "NOTINTERESTED",
							 lead_state: "NOTINTERESTED"
					 },
					 {
							 id:"INTERESTED",
							 lead_state: "INTERESTED"
					 },
					 {
							 id:"CALLBACK",
							 lead_state: "CALLBACK"
					 },
					 {
							 id:"BEYONDINTAKE",
							 lead_state: "BEYONDINTAKE"
					 },
					 {
							 id:"RINGING",
							 lead_state: "RINGING"
					 },
					 {
							 id:"LEADCONVERTED",
							 lead_state: "LEADCONVERTED"
					 },
					 {
							 id:"ACTIVE_LEADS",
							 lead_state: "ACTIVELEADS"
					 },
					 {
							 id:"TOTAL_LEADS",
							 lead_state: "TOTALLEADS"
					 }


			 ];

			 $scope.hearAboutUsOptions = [
		        {
		            id: "13",
		            status: "Cold Calling"
		        },
		          {
		              id: "312",
		              status: "Education Counsellors"
		          },
		          {
		              id:"8",
		              status: "Email"
		          },
		          {
		              id:"311",
		              status: "Friend Reference"
		          },
		          {
		              id:"9",
		              status: "Newspaper"
		          },
		          {
		              id:"314",
		              status: "Other Reference"
		          },
		          {
		              id:"11",
		              status: "Seminar"
		          },
		          {
		              id:"10",
		              status: "Social Media"
		          },
		          {
		              id:"313",
		              status: "University"
		          },
							{
		              id:"407",
		              status: "Webinar"
		          },
							{
		              id:"408",
		              status: "Walkin"
		          },
							{
		              id:"411",
		              status: "Website"
		          },
							{
		              id:"431",
		              status: "Google ADS"
		          }

		      ];

					$scope.rmReportTypeValue = [
					 {
						 id: "LEAD",
						 value: "LEAD"
					 },
					 {
						 id: "CALLINGSTUDENT",
						 value: "CALLING STUDENT"
					 },
					 {
						 id: "COUNSELOR",
						 value: "COUNSELOR"
					 },
					 {
						 id: "CALLINGCOUNSELOR",
						 value: "CALLING COUNSELOR"
					 },
					 {
						 id: "UNIVERSITY",
						 value: "UNIVERSITY"
					 },
					 {
						 id: "CALLINGUNIVERSITY",
						 value: "CALLING UNIVERSITY"
					 }
				 ];

        switch (filter) {

					case 'lead-chat-student':

						$scope.title = "Lead Chat List ";
						$scope.agentsList();
						$scope.productsList();

						break;

					case 'calling-chat-student':

						$scope.title = "Calling Chat List ";
						$scope.agentsList();
						$scope.sourceList();

						break;

          case 'msg-video-calling':
            $scope.title = "Student Chat List ";
						var login_to_id = $routeParams.login_id;
						var entity_name = $routeParams.entity_name;
						$scope.entity_name = entity_name;
            $scope.login_to_id = login_to_id;
						$scope.chatList(login_to_id);

						setInterval(() => {

							var apiUrl = ENV.apiEndpoint + "/chat/getmsg/"+login_to_id;

				  		return tokboxService.getData(apiUrl, accessToken)
						  .then(function(data){
								//console.log("hello", data);

						    $scope.chatList = data;
								$('.msger-chat').scrollTop($('.msger-chat')[0].scrollHeight);

						  })
						  .catch(function(error){
						    if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
						      window.location.href = '/';
						    }
						  });

						}, 10000);

            break;

        };


        $scope.viewFile = $route.current.$$route.pageName;
        $scope.location = $location.path();
        $scope.partnerName = partnerName;
        $scope.header = 'views/header.html';
        $scope.menu = 'views/menu.html';
        $scope.footer = 'views/footer.html';
        $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
        $scope.getMenu();

  }]);
