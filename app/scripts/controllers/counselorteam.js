'use strict';

angular.module('app')
.controller('counselorTeamController', ['$scope', 'ENV', '$cookieStore', 'commonService', '$routeParams', 'NgTableParams', '$route', 'menuService', '$location', '$rootScope', '$q', function ($scope, ENV, $cookieStore, commonService, $routeParams, NgTableParams, $route, menuService, $location, $rootScope, $q) {
    if (!$rootScope.bodylayout) {
        $rootScope.bodylayout = "login-page";
    }

    var accessToken = $cookieStore.get("access_token");
    var filter = $route.current.$$route.filter;
    var counselorId = $routeParams.cid ? $routeParams.cid : '0';
    $scope.counselorId = counselorId;
    var memberId = $routeParams.tid ? $routeParams.tid : '0';

    $scope.getMenu = function () {
        return commonService.getMenu(accessToken)
            .then(function (data) {
            $scope.menus = data;
        })
        .catch(function (error) {
            if (error.error_code === "SESSION_EXPIRED"  || error.error_code === "HEADER_MISSING") {
                window.location.href = '/';
            }
        });
    };

    $scope.toggleMenu = function (id) {
        $('#menu-' + id).toggle();
    };
    $scope.toggleSubMenu = function (id) {
        $('#submenu-' + id).toggle();
    };

    $scope.getMyTeam = function(){
		commonService.getPartnerListData(accessToken, counselorId)
		.then(function (data) {
            data.forEach(function(value, key){
                data[key]['partner_id'] = counselorId;
            })
			$scope.tableParams = new NgTableParams({}, { dataset: data });
		});
	}

    $scope.getPartnerData = function(id){
        var api = ENV.apiEndpoint + '/' + id;
        return commonService.getPartnerData(api, accessToken)
        .then(function(response){
            $scope.gentitle = response.title_id;
            $scope.first_name = response.first_name;
            $scope.last_name = response.last_name;
            $scope.email = response.email;
            $scope.mobile = response.mobile_number;
            $scope.company_logo = "";
            if(response.other_details && response.other_details.company_name){
                $scope.company_name = response.other_details.company_name;
            }
            if(response.other_details && response.other_details.company_url){
                $scope.company_url = response.other_details.company_url;
            }
            if(response.other_details && response.other_details.company_logo){
                $scope.company_logo = response.other_details.company_logo;
            }
            $scope.landline = response.telephone_number;
            if(response.address && response.address.address){
                $scope.address = response.address.address;
            }
            if(response.other_details && response.other_details.students_college){
                $scope.students_college = response.other_details.students_college;
            }
            if(response.other_details && response.other_details.students_final_year){
                $scope.students_final_year = response.other_details.students_final_year;
            }
            if(response.other_details && response.other_details.students_going_abroad){
                $scope.students_going_abroad = response.other_details.students_going_abroad;
            }
            if(response.other_details && response.other_details.designation){
                $scope.designation = response.other_details.designation;
            }
            if(response.other_details && response.other_details.agreement){
                $scope.agreement = response.other_details.agreement;
            }
            if(response.branch_detail && response.branch_detail.name){
                $scope.branch_name = response.branch_detail.name;
            }
            if(response.branch_detail && response.branch_detail.email){
                $scope.branch_email = response.branch_detail.email;
            }
            if(response.branch_detail && response.branch_detail.counselor_name){
                $scope.branch_counselor_name = response.branch_detail.counselor_name;
            }
            if(response.branch_detail && response.branch_detail.grade){
                $scope.branch_grade = response.branch_detail.grade;
            }
            if(response.branch_detail && response.branch_detail.address){
                $scope.branch_address = response.branch_detail.address;
            }
            $scope.city = {};
            $scope.city.description = {};
            $scope.city.description.id = response.city_id;
            $scope.city.title = response.city_name;
            $scope.country = response.country_id;
            $scope.partnerId = id;
            $scope.counselorLoginId = response.login_id;
        })
        .catch(function(error){
            console.log(error);
        })
    }

    $scope.addMember = function () {
        if (!$scope.member_title || !$scope.member_first_name || !$scope.member_last_name || !$scope.member_email) {
            alert("Please fill all mandatory details");
            return;
        }

        var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
        var branchDetail = {};
        branchDetail.name = ($scope.branch_name !== undefined) ? $scope.branch_name : '';
		branchDetail.email = ($scope.branch_email !== undefined) ? $scope.branch_email : '';
		branchDetail.counselor_name = ($scope.branch_counselor_name !== undefined) ? $scope.branch_counselor_name : '';
		branchDetail.address = ($scope.branch_address !== undefined) ? $scope.branch_address : '';
		branchDetail.grade = ($scope.branch_grade !== undefined) ? $scope.branch_grade : '';

        $scope.loader = true;
        var postParams = {
            partner_type: $scope.partner_type,
            title_id: $scope.member_title,
            first_name: $scope.member_first_name,
            last_name: $scope.member_last_name,
            email: $scope.member_email,
            mobile_number: $scope.member_mobile,
            country_id: $scope.country,
            address: {
                "address": $scope.address
            },
            other_details: {
                "company_name": $scope.company_name,
                "company_url": $scope.company_url,
                "students_college": $scope.students_college,
                "students_final_year": $scope.students_final_year,
                "students_going_abroad": $scope.students_going_abroad,
                "designation": $scope.designation,
                "agreement": $scope.agreement
            },
            role_id: 18,
            landline_number: $scope.landline,
            city_id: cityId,
            city_name: $scope.city.title,
            parent_id: $scope.counselorLoginId,
            branch_detail: branchDetail
        }
        return commonService.add(accessToken, 'register', postParams)
        .then(function (data) {
            alert(data.result);
            $scope.loader = false;
            window.location.href = '/counselor/' + counselorId + '/team/list';
        })
        .catch(function (err) {
            $scope.loader = false;
            alert(err.message);
        })
    };

    $scope.updateMember = function (partnerId) {
        if (!$scope.gentitle || !$scope.first_name || !$scope.last_name) {
            alert("Please fill all mandatory details");
            return;
        }
        var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
        $scope.loader = true;
        var branchDetail = {};
        branchDetail.name = ($scope.branch_name !== undefined) ? $scope.branch_name : '';
		branchDetail.email = ($scope.branch_email !== undefined) ? $scope.branch_email : '';
		branchDetail.counselor_name = ($scope.branch_counselor_name !== undefined) ? $scope.branch_counselor_name : '';
		branchDetail.address = ($scope.branch_address !== undefined) ? $scope.branch_address : '';
		branchDetail.grade = ($scope.branch_grade !== undefined) ? $scope.branch_grade : '';
        var putParams = {
            title_id: $scope.gentitle,
            first_name: $scope.first_name,
            last_name: $scope.last_name,
            other_details: {
                "designation": $scope.designation
            },
            branch_detail: branchDetail
        }

        return commonService.updatePartnerData(accessToken, '/' + partnerId, putParams)
        .then(function (data) {
            $scope.loader = false;
            alert(data.result);
            window.location.href = '/counselor/' + counselorId + '/team/list';
        })
        .catch(function (err) {
            $scope.loader = false;
            alert(err.message);
        })
    }

    //$scope.isEditable = false;
    $scope.editData = function (emp) {
        emp.isEditable = true;
    };

    $scope.cancelEditData = function (emp) {
        emp.isEditable = false;
    };

    $scope.getAllTitles = function () {
        var listId = 3;
        commonService.getListData(listId)
        .then(function (data) {
            $scope.allTitles = data;
        });
    };

    switch (filter) {
        case 'list':
            $scope.title = "Counselor Team List ";
            $scope.getMyTeam();
            $scope.getPartnerData(counselorId);
            break;
        case 'add':
            $scope.title = "Add Counselor Team Member";
            $scope.getPartnerData(counselorId);
            $scope.getAllTitles();
            break;
        case 'edit':
            $scope.title = "Edit Counselor Team Member";
            $scope.getAllTitles();
            $scope.getPartnerData(memberId);
            break;
    };

    $scope.viewFile = $route.current.$$route.pageName;
    $scope.location = $location.path();
    $scope.header = 'views/header.html';
    $scope.menu = 'views/menu.html';
    $scope.footer = 'views/footer.html';
    $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";

    $scope.getMenu();
}]);
