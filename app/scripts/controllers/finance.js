'use strict';

angular.module('app')
	.controller('financeController',  ['$scope', '$location', '$rootScope', 'commonService', 'dashboardService', '$cookieStore', 'menuService', '$route', '$routeParams', 'listService', 'ENV', 'NgTableParams', function ($scope, $location, $rootScope, commonService, dashboardService, $cookieStore, menuService, $route, $routeParams, listService, ENV, NgTableParams){

        var accessToken = $cookieStore.get("access_token");
        var partnerName = $cookieStore.get("partner_name");
        var roleId = $cookieStore.get("rId");
        var filter = $route.current.$$route.filter;

				$scope.disbursed_bank_id = [];
				$scope.bankSetting = { displayProp: 'value', idProperty:'id', enableSearch: true };
        $scope.role_id = roleId;
        $scope.bank_id;

        $scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
        };

        $scope.disbursedStudentList = function(){

          var intake_year = $routeParams.year;
          $scope.intakeYear = intake_year;

        		var api = ENV.apiEndpoint + '/disbursedListIntakeWise/'+intake_year;

        		return dashboardService.getdisbursedData(api, accessToken)
        		.then(function(data){

        			var dataset = data.data;
              $scope.estimatedearnings = data.total_earning_info.total_calculate_earnings;
              $scope.reacivedearnings = data.total_earning_info.total_reacived_earnings;
	      			$scope.commisiongiven = data.total_earning_info.total_commision_given;

    				$scope.AllStudentListTableParams = new NgTableParams({}, {
    					counts: [10, 50, 100, 500, 1000],
    					dataset: dataset
    				});
        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};

				$scope.confirmdisbursedStudentList = function(){

          var intake_year = $routeParams.year;
          $scope.intakeYear = intake_year;

        		var api = ENV.apiEndpoint + '/confirmdisbursedListIntakeWise/'+intake_year;

        		return dashboardService.getdisbursedData(api, accessToken)
        		.then(function(data){

        			var dataset = data.data;
              $scope.confirmestimatedearnings = data.total_earning_info.total_calculate_earnings;
              $scope.confirmreacivedearnings = data.total_earning_info.total_reacived_earnings;
	      			$scope.confirmcommisiongiven = data.total_earning_info.total_commision_given;

    				$scope.confirmAllStudentListTableParams = new NgTableParams({}, {
    					counts: [10, 50, 100, 500, 1000],
    					dataset: dataset
    				});
        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};

        $scope.disbursedLeadInfo = function(ldid){

        		var api = ENV.apiEndpoint + '/disbursalleadinfo/'+ldid;

        		return dashboardService.getdisbursedData(api, accessToken)
        		.then(function(data){

        			$scope.disbursedDataInfo = data.data;
              $scope.name = data.data.name;
              $scope.mobile_number = data.data.mobile_number;
              $scope.email = data.data.email;
              $scope.partner_name = data.data.partner_name;
              $scope.invoice_raised = data.data.invoice_raised;
							$scope.invoice_raised_date = data.data.invoice_raised_date;
							$scope.invoice_raised_number = data.data.invoice_raised_number;
              $scope.sanctioned_date = data.data.sanctioned_date;
              $scope.sanctioned_amount = data.data.sanctioned_amount;
              $scope.disbursed_date = data.data.disbursed_date;
              $scope.disbursed_amount = data.data.disbursed_amount;
              $scope.amount_received = data.data.amount_received;
							$scope.other_income = data.data.other_income;
							$scope.total_income_rececived = data.data.total_income_rececived;
							$scope.payment_status = data.data.payment_status;
							$scope.payment_transaction_number = data.data.payment_transaction_number;
							$scope.payment_in_bank = data.data.payment_in_bank;
							$scope.payment_received_date = data.data.payment_received_date;
              $scope.product_name = data.data.product_name;
              $scope.current_lead_state = data.data.current_lead_state;
              $scope.ldid = data.data.ldid;
              $scope.amount_calculated = data.data.amount_calculated;
							$scope.sanctioned_amount_by_finance = data.data.sanctioned_amount_by_finance;
							$scope.disbursed_amount_by_finance = data.data.disbursed_amount_by_finance;
							$scope.approved_by_finance = data.data.approved_by_finance;

							if(data.data.invoice_raised == 'YES'){
								$('#invoice_raised_date_div').show();
								$('#invoice_raised_number_div').show();
							}

							if(data.data.payment_status == 'RECEIVED'){
								$('#payment_received_date_div').show();
								$('#amountReceived_div').show();
								$('#otheramountReceived_div').show();
								$('#totalamountReceived_div').show();
								$('#payment_transaction_number_div').show();
								$('#payment_in_bank_div').show();
							}
              //console.log("hellllooooooo",$scope.disbursedDataInfo);
        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};
			$scope.active = 'Disbursal 1';
			$scope.selectTab = function(key){
				$scope.active = key;
			}
			$scope.disbursedInfoByLeadId = function(lead_id){
        		var api = ENV.apiEndpoint + '/disbursalleadinfoleadid/'+lead_id;

        		return dashboardService.getdisbursedData(api, accessToken)
        		.then(function(data){

							//var leaddisbursalinfobyleadid = data.data;

							$scope.leaddisbursalinfobyleadid = data.data;

              //console.log("hellllooooooo",$scope.leaddisbursalinfobyleadid);
        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};

				$scope.disbursedInfoDeleteByLdnid = function(ldnId,leadId){

	        		var api = ENV.apiEndpoint + '/productdisbursalinfo/deletebyid';

							var postparams = {
								"ldn_id" : ldnId
							}

	        		return dashboardService.updateDisbursedData(api, postparams, accessToken)
	        		.then(function(data){

								//$scope.disbursedInfoByLeadId(leadId);
								alert('Deleted Successfully. Refresh To View Changes');

	        		})
	        		.catch(function(error){
	                    $scope.error = {
	                        message: error.message
	                    };
	        		});
	    		};

				$scope.totalIncomeAdd = function(){

					var amountReceived = ($scope.amount_received ? $scope.amount_received : 0) * 1;
					var otherIncome = ($scope.other_income ? $scope.other_income : 0 ) * 1
					$scope.total_income_rececived = amountReceived + otherIncome;

				};

        $scope.updateDisbursedLeadInfo = function(){

            /*var postparams = {
              "id" : $scope.ldid,
              "invoice_raised": $scope.invoice_raised,
							"invoice_raised_date": $scope.invoice_raised_date,
							"invoice_raised_number": $scope.invoice_raised_number,
              "amount_received": $scope.amount_received,
							"other_income": $scope.other_income,
							"total_income_rececived": $scope.total_income_rececived,
							"payment_status": $scope.payment_status,
							"payment_transaction_number": $scope.payment_transaction_number,
							"payment_in_bank": $scope.payment_in_bank,
							"payment_received_date": $scope.payment_received_date,
              "sanctioned_date": $scope.sanctioned_date,
              "sanctioned_amount": $scope.sanctioned_amount,
              "disbursed_date": $scope.disbursed_date,
              "disbursed_amount": $scope.disbursed_amount,
              "amount_calculated" : $scope.amount_calculated,
							"sanctioned_amount_by_finance" : $scope.sanctioned_amount_by_finance,
							"disbursed_amount_by_finance" : $scope.disbursed_amount_by_finance,
							"approved_by_finance" : $scope.approved_by_finance
            };

            console.log("Data",$scope.leaddisbursalinfobyleadid);
			return;*/
			var postparams = [];
			for (let key in $scope.leaddisbursalinfobyleadid){
				postparams.push($scope.leaddisbursalinfobyleadid[key]);
			}
            var api = ENV.apiEndpoint + '/disbursalleadinfo/update';

        		return dashboardService.updateDisbursedData(api, postparams, accessToken)
        		.then(function(data){

              alert("Updated Successfully !!!");

        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};

				$scope.invoiceChange = function(){

    			if($scope.invoice_raised == 'YES'){
						$('#invoice_raised_date_div').show();
						$('#invoice_raised_number_div').show();
					} else {
						$('#invoice_raised_date_div').hide();
						$('#invoice_raised_number_div').hide();
					}

    		};

				$scope.paymentChange = function(){

					if($scope.payment_status == 'RECEIVED'){
						$('#payment_received_date_div').show();
						$('#amountReceived_div').show();
						$('#otheramountReceived_div').show();
						$('#totalamountReceived_div').show();
						$('#payment_transaction_number_div').show();
						$('#payment_in_bank_div').show();
					} else {
						$('#payment_received_date_div').hide();
						$('#amountReceived_div').hide();
						$('#otheramountReceived_div').hide();
						$('#totalamountReceived_div').hide();
						$('#payment_transaction_number_div').hide();
						$('#payment_in_bank_div').hide();
					}

    		};
        $scope.intakeYearFilter = function(){

    			var intake_year_filter = $scope.intake_year_filter;
    				window.location.href='/finance/disbursedlist/'+intake_year_filter;

    		};

				$scope.confirmintakeYearFilter = function(){

    			var confirm_intake_year_filter = $scope.confirm_intake_year_filter;
    				window.location.href='/confirmfinance/disbursedlist/'+confirm_intake_year_filter;

    		};

        $scope.disbursalData = function(){

          $(document).on('change',"#bankId", function(){
            var bank_id = $scope.bank_id;
            //alert("ID : " + bank_id);
            $scope.bankDisbursedInfo(bank_id);
          });
        };

        $scope.bankDisbursedInfo = function(bank_id){

            var api = ENV.apiEndpoint + '/productdisbursalinfoandrange/'+ bank_id;

            return dashboardService.getdisbursedData(api, accessToken)
            .then(function(data){
              $scope.product_info = data;
              $scope.proID = data.product_info.id;
              $scope.sancAmt = data.product_info.on_sanction;
              $scope.disbAmt = data.product_info.on_disbursal;
              $scope.firstDis = data.product_info.singal_disbursal;
              $scope.allDis = data.product_info.all_disbursal;
              $scope.percentVal = data.product_info.percentage_value;
              $scope.flatValue = data.product_info.flat_value;
              $scope.range = data.product_info.range;
              $scope.rangeAmt = data.product_info.range_on_amount;
              //console.log("range",$scope.range);
              var productRangeData = data.product_range;
              $scope.productRangeDataList = new NgTableParams({}, {dataset: productRangeData});
              $scope.showTable = true;
              //console.log("hellllooooooo",productRangeData);
              if($scope.range == "YES"){
                  $('#RangeTableDiv').show();
                  $(document).ready(function()
                  {
                    $('#amountRange').change(function()
                    {
                      if(this.checked == false)
                      {
                           //alert('No values Will be added to Range?');
                           var r = confirm("Please Confirm!!\nNo values Will be added to Range ?");
                            if (r == true) {
                              //alert("You pressed OK!");
                              $scope.updateProductDisbursedInfo();
                            } else {
                             //alert("You pressed Cancel!");
                             // document.getElementById("Rlabel").innerHTML = chk.checked ? "Yes" : "No";
                             document.getElementById("Rlabel").innerHTML = "YES";
                             document.getElementById("amountRange").checked = true;
                             //$("#checkbox").prop("checked", true);
                            }
                          // $('#RangeTableDiv').hide();
                      }else{
                          //alert('Do you want to Add Range Value?');
                          var r = confirm("Please Confirm!!\nDo you want to Add Range Value ?");
                            if (r == true) {
                              //alert("You pressed Cancel!");
                              $scope.updateProductDisbursedInfo();
                            } else {
                             //alert("You pressed Cancel!");
                             $("#checkbox").prop("checked", false);
                            }
                          //$('#RangeTableDiv').show();
                      }
                    });
                  });

              }else if($scope.range == "NO"){
                  $('#RangeTableDiv').hide();
                  $(document).ready(function()
                  {
                    $('#amountRange').change(function()
                    {
                      if(this.checked == true)
                      {
                           //alert('Do you want to Add Range Value?');
                           //$('#RangeTableDiv').show();
                           var r = confirm("Please Confirm!!\nDo you want to Add Range Value ?");
                            if (r == true) {
                              //alert("You pressed OK!");
                              $scope.updateProductDisbursedInfo();
                            } else {
                             //alert("You pressed Cancel!");
                             $("#checkbox").prop("checked", false);
                            }
                      }else{
                          //alert('No values Will be added to Range?');
                          //$('#RangeTableDiv').hide();
                          var r = confirm("Please Confirm!!\nNo values Will be added to Range ?");
                            if (r == true) {
                              //alert("You pressed OK!");
                             // $("#checkbox").prop("checked", false);
                              $scope.updateProductDisbursedInfo();
                            } else {
                             //alert("You pressed Cancel!");
                            }
                      }
                    });
                  });
              }else{
                 $('#RangeTableDiv').hide();
              }
            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        $scope.updateProductDisbursedInfo = function(){

            var postparams = {
              "id" : $scope.proID,
              "bank_id" : $scope.bank_id,
              "on_sanction": $scope.sancAmt,
              "on_disbursal": $scope.disbAmt,
              "singal_disbursal": $scope.firstDis,
              "all_disbursal": $scope.allDis,
              "percentage_value": $scope.percentVal,
              "flat_value": $scope.flatValue,
              "range" : $scope.range,
              "range_on_amount": $scope.rangeAmt
            };

            //console.log("Data :",postparams);
            var api = ENV.apiEndpoint + '/productdisbursalinfo/update';

            return dashboardService.updateDisbursedData(api, postparams, accessToken)
            .then(function(data){

              alert("Updated Successfully !!!");
              window.location.href = '/finance/bankDisbursalInfo';


            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        $scope.updateProductDisbursedRangeInfo = function(rangeValue){

            var postparams = {
              "id" : rangeValue.id,
              "bank_id" : rangeValue.bank_id,
              "start_lead_count": rangeValue.start_lead_count,
              "end_lead_count": rangeValue.end_lead_count,
              "percentage": rangeValue.percentage,
              "amount": rangeValue.amount
            };

           // console.log("Range :",postparams);
            var api = ENV.apiEndpoint + '/productdisbursalinforange/update';

            return dashboardService.updateDisbursedData(api, postparams, accessToken)
            .then(function(data){

              alert("Updated Successfully !!!");
              window.location.href = '/finance/bankDisbursalInfo';

            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        // $scope.updateProductDisbursedRangeInfo1 = function(){

        //     var postparams = {

        //       "bank_id" : $scope.bank_id,
        //       "start_lead_count": $scope.start_lead_count,
        //       "end_lead_count": $scope.end_lead_count,
        //       "percentage": $scope.percentage,
        //       "amount": $scope.amount
        //     };

        //     console.log("Range :",postparams);
        //     var api = ENV.apiEndpoint + '/productdisbursalinforange/update';

        //     return dashboardService.updateDisbursedData(api, postparams, accessToken)
        //     .then(function(data){

        //       alert("Added Successfully !!!");
        //       window.location.href = '/finance/bankDisbursalInfo';

        //     })
        //     .catch(function(error){
        //             $scope.error = {
        //                 message: error.message
        //             };
        //     });
        // };


        $scope.editData = function (appId) {
          appId.isEditable = true;
        };

        $scope.cancelEditData = function (appId) {
          appId.isEditable = false;
          //$scope.isEditable = true;
        };



        $scope.calculateData = function(){

            var postparams = {
              "bank_id" : $scope.bank_id,
              "intake_year": $scope.intake_year,
              "start_date": $('#start_date').val() ? $('#start_date').val() : 0,
              "end_date": $('#end_date').val() ? $('#end_date').val() : 0
            };

           // console.log("Data",postparams);
            var api = ENV.apiEndpoint + '/finance/calculateamount';

            return dashboardService.updateDisbursedData(api, postparams, accessToken)
            .then(function(data){

              alert("Updated Successfully !!!");

            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        $scope.activeBankList = function(){

            var api = ENV.apiEndpoint + '/activebanks';

            return dashboardService.getdisbursedData(api, accessToken)
            .then(function(data){

              $scope.banklist = data.data;

            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        $scope.agentIncome = function(intakeYear,agentId){

            var api = ENV.apiEndpoint + '/agentincome/'+intakeYear+'/'+agentId;

            return dashboardService.getdisbursedData(api, accessToken)
            .then(function(data){

              alert("New Data Fetched For My Income");

              $scope.agentIncomeList = data.data;
              $scope.agentEstimatedIncome = data.estimated_income;
              $scope.agentActualIncome = data.actual_income;
              $scope.agentIncomeLeadData = data.lead_data;

            /*  var dataset = data.lead_data;
              $scope.tableParamsAgentIncomeData = new NgTableParams({}, {dataset: dataset});
              $scope.showTable = true;*/

            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        $scope.tlIncome = function(intakeYear,agentId){

            var api = ENV.apiEndpoint + '/tlincome/'+intakeYear+'/'+agentId;

            return dashboardService.getdisbursedData(api, accessToken)
            .then(function(data){

              alert("New Data Fetched For My Team");

              $scope.tlIncomeList = data.data;
              $scope.tlEstimatedIncome = data.estimated_income;
              $scope.tlActualIncome = data.actual_income;
              $scope.tlIncomeLeadData = data.lead_data;

              /*var dataset = data.lead_data;
              $scope.tableParamstlIncomeData = new NgTableParams({}, {dataset: dataset});
              $scope.showTable = true;*/

            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        $scope.intakeYearFilterAgentIncome = function(){

          var intake_year_filter = $scope.intake_year_filter;
          //alert(intake_year_filter);
            window.location.href='/finance/agentincome/'+intake_year_filter;

        };

        $scope.agentsList = function(){

            var api = ENV.apiEndpoint + '/agentslist';

            return dashboardService.getdisbursedData(api, accessToken)
            .then(function(data){
              $scope.agentslist = data;

              // console.log(agentslist);

            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };



        $scope.agentFilterAgentIncome = function(){

          //var partnerId = $scope.lead_partner_id;
          var intake_year = $routeParams.year;
          //alert(partnerId);

          var partnerIdd = $('#lead_partner_id').val();
          partnerIdd = partnerIdd.substring(7, partnerIdd.length);
          //alert(partnerIdd);

          $scope.agentIncome(intake_year,partnerIdd);
          $scope.tlIncome(intake_year,partnerIdd);

        };

        $scope.agentIncomeLeadData = function(leadstate){

          var intake_year = $routeParams.year;
          var partnerIdd = 0 ;
          if(roleId == 1){
            partnerIdd = $('#lead_partner_id').val();
            partnerIdd = partnerIdd.substring(7, partnerIdd.length);
          }

            var postparams = {
              "lead_state" : leadstate,
              "intake_year" : intake_year,
              "partner_id" : partnerIdd
            };

            var api = ENV.apiEndpoint + '/finance/agentleaddata';

            return dashboardService.updateDisbursedData(api, postparams, accessToken)
            .then(function(data){

              $scope.applicationleadFilterData = data.data;
              var dataset = data.data;
              $scope.tableParamsAgentIncomeData = new NgTableParams({}, {dataset: dataset});
              $scope.showTable = true;

            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        $scope.tlIncomeLeadData = function(leadstate){

          var intake_year = $routeParams.year;
          var partnerIdd = 0 ;
          if(roleId == 1){
            partnerIdd = $('#lead_partner_id').val();
            partnerIdd = partnerIdd.substring(7, partnerIdd.length);
          }

            var postparams = {
              "lead_state" : leadstate,
              "intake_year" : intake_year,
              "partner_id" : partnerIdd
            };

            var api = ENV.apiEndpoint + '/finance/tlleaddata';

            return dashboardService.updateDisbursedData(api, postparams, accessToken)
            .then(function(data){

              $scope.applicationleadFilterData = data.data;
              var dataset = data.data;
              $scope.tableParamsAgentIncomeData = new NgTableParams({}, {dataset: dataset});
              $scope.showTable = true;

            })
            .catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
            });
        };

        $scope.setLeadState = function(modalleadstate,type){

          if(type == 'agent'){
            // console.log($scope.agentIncomeLeadData);
            var dataset = $scope.agentIncomeLeadData;
            var newdataset = [];

            for(var key in dataset){

              if(dataset[key]['current_lead_state'] == modalleadstate){
                newdataset.push(dataset[key]);
              }

            }

            $scope.tableParamsAgentIncomeData = new NgTableParams({}, {dataset: newdataset});
            $scope.showTable = true;

          } else{

            var dataset = $scope.tlIncomeLeadData;
            var newdataset = [];

            for(var key in dataset){

              if(dataset[key]['current_lead_state'] == modalleadstate){
                newdataset.push(dataset[key]);
              }

            }

            $scope.tableParamstlIncomeData = new NgTableParams({}, {dataset: newdataset});
            $scope.showTable = true;

          }

        };

				$scope.studentOfferList = function(intake_year){

        		var api = ENV.apiEndpoint + '/newoffer/offerstudents/'+intake_year;

        		return dashboardService.getdisbursedData(api, accessToken)
        		.then(function(data){

        			var dataset = data.data;

    				$scope.AllofferStudentListTableParams = new NgTableParams({}, {
    					counts: [10, 30, 50, 100, 1000],
    					dataset: dataset
    				});

						$scope.data.forEach(function (employee) {
	            employee.isEditable = false;
	          });


        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};

				$scope.intakeYearFilterOfferStudent = function(){

    			var intake_year_filter = $scope.intake_year_filter_offer_student;
    				window.location.href='/finance/studentlist/byoffer/'+intake_year_filter;

    		};

				$scope.saveStudentOfferData = function(studentOfferData){

    			console.log(studentOfferData);

					var postparams = {
						"id" : studentOfferData.id,
						"amount_paid" : studentOfferData.amount_paid,
						"claim_status" : studentOfferData.claim_status
					};

					var api = ENV.apiEndpoint + '/student/offerupdate';

					return dashboardService.updateDisbursedData(api, postparams, accessToken)
					.then(function(data){

					alert("Updated Successfully ....!!!");
						studentOfferData.isEditable = false;

					})
					.catch(function(error){
									$scope.error = {
											message: error.message
									};
					});



    		};


				$scope.offerVendorList = function(intake_year){

        		var api = ENV.apiEndpoint + '/vendor/list';

        		return dashboardService.getdisbursedData(api, accessToken)
        		.then(function(data){

        			var dataset = data.data;

    				$scope.AllofferVendorListTableParams = new NgTableParams({}, {
    					counts: [10, 30, 50, 100, 1000],
    					dataset: dataset
    				});


        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};

				$scope.agentDisbursedIncomef = function(type){

					if($scope.team_type == "YES"){

					if($scope.lead_partner_id != "2" && $scope.lead_partner_id != "3"){
						alert("For this RM Team is not present...!!!");
						return false;
					}

				}

					var postparams = {

						"agent_id" : $scope.lead_partner_id,
						"start_date": $('#start_date').val() ? $('#start_date').val() : 0,
						"end_date": $('#end_date').val() ? $('#end_date').val() : 0,
						"bank_ids" : $scope.disbursed_bank_id,
						"team_type" : $scope.team_type,
						"rm_percentage" : $scope.agent_percentage,
						"rm_team_percentage" : $scope.agent_team_percentage,
						"intake_year" : $scope.intake_year

					};

					if(type == "YES"){
						var api = ENV.apiEndpoint + '/agentDisbursedApprovedIncome';
					} else {
						var api = ENV.apiEndpoint + '/agentDisbursedIncome';
					}

					$('#loaderAjax').show();

					return dashboardService.updateDisbursedData(api, postparams, accessToken)
					.then(function(data){

						//console.log(data);
						$scope.agentDisbursedIncome = data.my_income;
						$scope.agentDisbursedTeamIncome = data.my_team_income ? data.my_team_income : [];
						$scope.disbursedApproved = type;
						$scope.rm_total_income = data.rm_total_income ? data.rm_total_income : 0 ;
						$scope.rm_total_team_income = data.rm_team_total_income ? data.rm_team_total_income : 0 ;
						$scope.rm_overall_income = data.rm_overall_income ? data.rm_overall_income : 0 ;

						$('#loaderAjax').hide();

					})
					.catch(function(error){
									$scope.error = {
											message: error.message
									};
					});

    		};

				$scope.disbursedIncomeLeadList = function(type,bankId,resultType){

					$('#loaderAjax').show();

					var postparams = {

						"agent_id" : $scope.lead_partner_id,
						"start_date": $('#start_date').val() ? $('#start_date').val() : 0,
						"end_date": $('#end_date').val() ? $('#end_date').val() : 0,
						"bank_ids" : bankId,
						"team_type" : $scope.team_type,
						"disbursed_approved" : type,
						"result_type" : resultType,
						"intake_year" : $scope.intake_year

					};

						var api = ENV.apiEndpoint + '/agentDisbursedIncomeLeadDetails';

					return dashboardService.updateDisbursedData(api, postparams, accessToken)
					.then(function(data){

						var dataset = data.data;

					$scope.disbursedIncomeTableParams = new NgTableParams({}, {
						counts: [10, 30, 50, 100, 1000],
						dataset: dataset
					});

						$('#loaderAjax').hide();

					})
					.catch(function(error){
									$scope.error = {
											message: error.message
									};
					});

    		};

				$scope.banklists = [
				 {
					 id: "1",
					 value: "Axis Bank"
				 },
				 {
					 id: "2",
					 value: "Bank of Baroda"
				 },
				 {
					 id: "3",
					 value: "State Bank of India"
				 },
				 {
					 id: "4",
					 value: "MPower Financing"
				 },
				 {
					 id: "5",
					 value: "Auxilo"
				 },
				 {
					 id: "6",
					 value: "HDFC Credila"
				 },
				 {
					 id: "7",
					 value: "Avanse"
				 },
				 {
					 id: "8",
					 value: "Incred"
				 },
				 {
					 id: "9",
					 value: "Prodigy"
				 },
				 {
					 id: "11",
					 value: "Bajaj Finserv Ltd"
				 },
				 {
					 id: "12",
					 value: "US Banks"
				 },
				 {
					 id: "13",
					 value: "ICICI Bank"
				 },
				 {
					 id: "14",
					 value: "Ascent Bank"
				 },
				 {
					 id: "15",
					 value: "Sallie Mae Bank"
				 },
				 {
					 id: "16",
					 value: "Earnest Bank"
				 },
				 {
					 id: "17",
					 value: "Leap Finance"
				 }
			 ];

        switch(filter){

          case 'disbursed-list':
              //$scope.title = "All Source List";
    					$scope.disbursedStudentList();

              break;

				case 'confirm-disbursed-list':
						//$scope.title = "All Source List";
						$scope.confirmdisbursedStudentList();

						break;

          case 'disbursed-info':
              //$scope.title = "All Source List";
              //$scope.add_row();
              $scope.bankDisbursedInfo();
              $scope.activeBankList();
              $scope.disbursalData();

              break;

          case 'calculate-amount':
                  //$scope.title = "All Source List";
              $scope.activeBankList();

              break;

					case 'offer-student-list':

					var intake_year = $routeParams.year;
					$scope.intakeYear = intake_year;
					$scope.studentOfferList(intake_year);

							break;

				case 'offer-vendor-list':
							$scope.offerVendorList();
						break;

				case 'agent-income':

                var intake_year = $routeParams.year;
                $scope.intakeYear = intake_year;
                $scope.agentIncome(intake_year,3);
                $scope.agentsList();
                if(roleId == 1 || roleId == 3){
                  $scope.tlIncome(intake_year,3);
                }

              break;

				case 'agent-disbursed-income':

               $scope.agentsList();

	            break;
            }




        $scope.viewFile = $route.current.$$route.pageName;
        $scope.location = $location.path();
        $scope.partnerName = partnerName;
		    $scope.header = 'views/header.html';
	      $scope.menu = 'views/menu.html';
		    $scope.footer = 'views/footer.html';
	      $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
        $scope.getMenu();

}]);
