'use strict';

angular.module('app')
.controller('popUpController', ['$scope', 'leadService', 'studentService', 'ENV', '$cookieStore', 'commonService', '$routeParams', 'NgTableParams', '$compile', '$route', 'menuService', '$location', '$rootScope', '$q', 'popUpService', function ($scope, leadService, studentService, ENV, $cookieStore, commonService, $routeParams, NgTableParams, $compile, $route, menuService, $location, $rootScope, $q, popUpService) {
    var accessToken = $cookieStore.get("access_token");
    var loginId = $cookieStore.get("pId");
    var leadId = $routeParams.id ? $routeParams.id : 0;
    var filter = $route.current.$$route.filter;

    $scope.getMenu = function () {
        return commonService.getMenu(accessToken)
            .then(function (data) {
                $scope.menus = data;
            })
            .catch(function (error) {
                if (error.error_code === "SESSION_EXPIRED") {
                    window.location.href = '/';
                }
            });
    };
    $scope.toggleMenu = function (id) {
        $('#menu-' + id).toggle();
    };
    $scope.toggleSubMenu = function (id) {
        $('#submenu-' + id).toggle();
    };

 
    $scope.declinedLeadList = function(){
            var queryParams = {
                type : 'declined'
            }   
    var api = ENV.apiEndpoint +  "/lead/list";
     return popUpService.leadList(api, accessToken, queryParams)
    .then(function (data) {
          $scope.declinedTableParams = new NgTableParams({}, {dataset: data});
        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
        }; 

    $scope.totalLeadList = function(){
            var queryParams = {
                type : 'all'
            }   
    var api = ENV.apiEndpoint + "/lead/list";
     return popUpService.leadList(api, accessToken, queryParams)
    .then(function (data) {
          $scope.totalTableParams = new NgTableParams({}, {dataset: data});
        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
        }; 

    $scope.disbursedLeadList = function(){
            var queryParams = {
                type : 'disbursed'
            }   
    var api = ENV.apiEndpoint + "/lead/list";
     return popUpService.leadList(api, accessToken, queryParams)
    .then(function (data) {
          $scope.disbursedTableParams = new NgTableParams({}, {dataset: data});
        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
        }; 
    $scope.potentialLeadList = function(){
            var queryParams = {
                type : 'potential'
            }   
    var api = ENV.apiEndpoint + "/lead/list";
     return popUpService.leadList(api, accessToken, queryParams)
    .then(function (data) {
          $scope.potentialTableParams = new NgTableParams({filter:{current_lead_state: 'potential'}}, {dataset: data} );
        })
        .catch(function (error) {
          $scope.error = {
            message: error.message
          };
        });
        }; 

        switch(filter){
            case 'total':
            $scope.totalLeadList();
            break;
        
        case 'declined':
            $scope.declinedLeadList();
            break;
        
        case 'disbursed':
            $scope.disbursedLeadList();
            break;
        case 'potential':
            $scope.potentialLeadList();
            break;

        }
    $scope.viewFile = $route.current.$$route.pageName;
	$scope.location = $location.path();
	$scope.header = 'views/header.html';
	$scope.menu = 'views/menu.html';
	$scope.footer = 'views/footer.html';
	$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
    $scope.getMenu();
    if(leadId){
        $scope.partnername = leadId;
    }      

}]);
