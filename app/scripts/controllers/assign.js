'use strict';

angular.module('app')
.filter('num', function() {
    return function(input) {
      return parseInt(input, 10);
    };
})
.controller('assignController', ['$scope', 'assignService', 'ENV', '$cookieStore',  '$route',  'menuService', 'NgTableParams', '$rootScope', '$location', '$routeParams', '$compile', function ($scope, assignService, ENV, $cookieStore,  $route, menuService, NgTableParams,  $rootScope, $location, $routeParams, $compile){
		//angular.element(document.body).addClass("login-page");
		if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
		}

    var accessToken = $cookieStore.get("access_token");
    var filter = $route.current.$$route.filter;

    $scope.allcitieslist = [];
    $scope.rmcity = [];
    $scope.citySetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };
    $scope.rmproduct = [];
    $scope.productSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };
    $scope.rmstate = [];
    $scope.stateSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };
    $scope.parentrmid = [];
    $scope.parentRMSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };
    $scope.bankbranchid = [];
    $scope.bankBranchSetting = { displayProp: 'display_name', idProperty:'id', enableSearch: true, selectedToTop: true, scrollableHeight: '250px', scrollable: true };


    $scope.$back = function() {
    window.history.back();
  };

  $scope.claculateTime = function(dt) {
    return new Date(dt).getTime();

  }

    $scope.getMenu = function(){
			var api = ENV.apiEndpoint + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
      .catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.assignData = function(){
            $scope.error = {
                message: null
            };

            if(!($scope.select_type)){

              alert("Assign Type Not Given !");
              return ;

            }

            if(!($scope.amountdata)){

              alert("Data Amount Not Given !");
              return ;

            }

            if(!($scope.select_agent)){

              alert("Agent Not Given !");
              return ;

            }

            var selectSource = $scope.select_source ? $scope.select_source : 0;
            var selectSourceAgent = $scope.select_source_agent ? $scope.select_source_agent : 0;

            var postParams =  {
          'data_type': $scope.select_type,
    			'amount': $scope.amountdata,
    			'assign_to': $scope.select_agent,
          'source_id': selectSource,
          'source_agent_id': selectSourceAgent,
    		};
        console.log($scope.select_source);
    		var api = ENV.apiEndpoint + '/assigndata';

    		return assignService.assignData(api, postParams, accessToken)
    		.then(function(data){

    			window.location.href='/assign/data';
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
        };

    $scope.unassigndatadropdown = function(){

			var postParams =  {};

    		var api = ENV.apiEndpoint + '/unassigndata';

    		return assignService.unassigndata(api, postParams, accessToken)
    		.then(function(data){
    			$scope.dropdowndata = data;

          console.log(dropdowndata);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.myAssignSourceList = function(){

			var postParams =  {};

    		var api = ENV.apiEndpoint + '/unassigndata';

    		return assignService.unassigndata(api, postParams, accessToken)
    		.then(function(data){
    			$scope.dropdowndata = data;
          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.agentsList = function(){

			var postParams =  {};

    		var api = ENV.apiEndpoint + '/agentslist';

    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){
    			$scope.agentslist = data;

          console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};



    $scope.myAssignList = function(sourceId){

      var lead_state = $routeParams.leadstate;

    		var api = ENV.apiEndpoint + '/callingdata/'+ lead_state +'/'+ sourceId;

    		return assignService.assignList(api, accessToken)
    		.then(function(data){
    			$scope.assignlist = data;
          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

          console.log(assignlist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.myTodaysAssignList = function(){

    		var api = ENV.apiEndpoint + '/callingdata/todayscallingdata';

    		return assignService.assignList(api, accessToken)
    		.then(function(data){
    			$scope.assignlist = data;
          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

          console.log(assignlist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.getLeadStateFiltterData = function(lead_state_filtter){

      var sourceId = $routeParams.id;
      $location.url('/assign/mycalldata/'+sourceId+'/'+lead_state_filtter);

      console.log('hello',lead_state_filtter,sourceId);

    		var api = ENV.apiEndpoint + '/callingdata/'+lead_state_filtter+'/'+sourceId;

    		return assignService.assignList(api, accessToken)
    		.then(function(data){
    			$scope.assignlist = data;
          //console.log(assignlist);
          //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};



    $scope.dateformat = function(D){
			 var pad = function(num) { var s = '0' + num; return s.substr(s.length - 2); }
			 var Result = D.getFullYear() + '-' + pad((D.getMonth() + 1)) + '-' + pad(D.getDate());
			 return Result; };



    $scope.allStatus = [
      {
          id: "FRESH",
          lead_state: "FRESH"
      },
        {
            id: "NOTINTERESTED",
            lead_state: "NOTINTERESTED"
        },
        {
            id:"INTERESTED",
            lead_state: "INTERESTED"
        },
        {
            id:"CALLBACK",
            lead_state: "CALLBACK"
        },
        {
            id:"BEYONDINTAKE",
            lead_state: "BEYONDINTAKE"
        },
        {
            id:"RINGING",
            lead_state: "RINGING"
        }

    ];

    //$scope.isEditable = false;
    $scope.editData = function (emp) {
      emp.isEditable = true;
        //$scope.isEditable = true;
    };

    $scope.cancelEditData = function (emp) {
      emp.isEditable = false;
        //$scope.isEditable = true;
    };

    $scope.saveData = function (emp) {
      //$scope.appkeys[$scope.editing] = $scope.newField;

        emp.isEditable = false;
        console.log('hello',emp,emp.lead_state);
        var postParams = {
          "id" : emp.id,
          "lead_state" : emp.lead_state,
          "name" : emp.name,
          "mobile1" : emp.mobile1,
          "gre_score" : emp.gre_score
        };

        var api = ENV.apiEndpoint + '/calldata/update';

    		return assignService.updateCallingLeadState(api, postParams, accessToken)
    		.then(function(data){

          //console.log(assignlist);
          $route.reload();

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});


    };

    $scope.callingSourceData = function(){

    		var api = ENV.apiEndpoint + '/allsource/3';

    		return assignService.sourceRawList(api, accessToken)
    		.then(function(data){
    			$scope.assignlist = data;
          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

          console.log(assignlist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.saveSourceData = function (emp) {
      //$scope.appkeys[$scope.editing] = $scope.newField;

        emp.isEditable = false;
        console.log('hello',emp,emp.lead_state);
        var postParams = {
          "id" : emp.id,
          "name" : emp.name,
          "display_name" : emp.display_name,
          "status" : emp.status
        };

        var api = ENV.apiEndpoint + '/upsertsource';

    		return assignService.saveSourceData(api, postParams, accessToken)
    		.then(function(data){

          //console.log(data);
          $route.reload();

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});


    };

    $scope.addSourceData = function(){

        var postParams = {
          "name" : $scope.source_name,
          "display_name" : $scope.source_display_name,
          "status" : $scope.source_status
        };

        console.log(postParams,"hello");

        var api = ENV.apiEndpoint + '/upsertsource';

        console.log(api,"hello");

        return assignService.saveSourceData(api, postParams, accessToken)
        .then(function(data){

          //console.log(data);

          $('#source_name').val('');
          $('#source_display_name').val('');
          $('#source_status').val('');

          window.location.href = '/source/data';

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.folloupData = function(){

			console.log($('#start_date').val(),"end dateeeee",$('#partner_id').val());

			//var start_date = $scope.start_date ? $scope.dateformat($('#start_date').val()) : $scope.dateformat(new Date());
			//var end_date = $scope.end_date ? $scope.dateformat($('#end_date').val()) : $scope.dateformat(new Date());


			 var postParams = {
				 "start_date" : $('#start_date').val(),
				 "end_date" : $('#end_date').val()
			 };

				var api = ENV.apiEndpoint + '/followupdate';
				//console.log(postParams,"hello");

				return assignService.saveSourceData(api, postParams, accessToken)
				.then(function(data){
          $scope.assignlist = data;
          //console.log(assignlist);
          //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });


				})
				.catch(function(error){
					$scope.showTable = false;
								$scope.error = {
										message: error.message
								};
				});
		};

    $scope.assignType = function(){

      if($scope.select_type == 'REASSIGN' ){

        $('#select_source_div').addClass('hidden');
        $('#select_source_agent_div').removeClass('hidden');

      } else {

        $('#select_source_div').removeClass('hidden');
        $('#select_source_agent_div').addClass('hidden');
        return;
      }


      var postParams =  {};

    		var api = ENV.apiEndpoint + '/reports/partners/overall';

    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){

					$scope.assignagentsourcelist = data;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});

    };

    $scope.productsList = function(){
			console.log("hello");

    		var api = ENV.apiEndpoint + '/productList/1';

    		return assignService.sourceRawList(api, accessToken)
    		.then(function(data){
						$scope.productlists = data;
					//console.log("hello",data.data);

          //console.log("errrr");

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.stateList = function(){

    		var api = ENV.apiEndpoint + '/statemaster';

    		return assignService.sourceRawList(api, accessToken)
    		.then(function(data){
						$scope.statelists = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.citiesByStateId = function(){

      var postParams = {
        "state_list" : $scope.rmstate ? $scope.rmstate : 0,
      };

     var api = ENV.apiEndpoint + '/citiesbystateid';

    		return assignService.saveSourceData(api, postParams, accessToken)
    		.then(function(data){

						$scope.allcitieslist  = data.data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.productsRmList = function(){

        var bank_id = $scope.bank_id;

    		var api = ENV.apiEndpoint + '/bankrmlist/' + bank_id;

    		return assignService.sourceRawList(api, accessToken)
    		.then(function(data){

          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {
						counts: [10, 50, 100, 500, 1000],
						dataset: dataset
					});

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.newProductsRmList = function(){

        //var bank_id = $scope.bank_id;
        console.log("scope $scope.rmproduct :",$scope.rmproduct);

        var postParams = {
          "product_list" : $scope.rmproduct
        }

    		var api = ENV.apiEndpoint + '/newbankrmlist';

    		return assignService.saveSourceData(api, postParams, accessToken)
    		.then(function(data){

          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
						counts: [10, 50, 100, 500, 1000],
						dataset: dataset
					});

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.bankBranchList = function(){

        //var bank_id = $scope.bank_id;

        var postParams = {
          "product_list" : $scope.rmproduct
        }

    		var api = ENV.apiEndpoint + '/list/bankbranch';

    		return assignService.saveSourceData(api, postParams, accessToken)
    		.then(function(data){

          var dataset = data.data;

          $scope.tableParams = new NgTableParams({}, {
						counts: [10, 50, 100, 500, 1000],
						dataset: dataset
					});

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.bankRmById = function(rmId){

      $scope.productsList().then(function(pdata) {

        $scope.stateList().then(function(sdata) {

          var api = ENV.apiEndpoint + '/bankrmbyid/' + rmId;

      		return assignService.sourceRawList(api, accessToken)
      		.then(function(data){

            /*var api = ENV.apiEndpoint + '/allcities';

        		return assignService.sourceRawList(api, accessToken)
        		.then(function(citydata){*/

              $scope.allcitieslist = data.city_list;
              $scope.parentrmlist = data.parent_list;
              $scope.bankbranchlist = data.branch_list;

              var productArray = [];
              var branchArray = [];
              var stateArray = [];
              var cityArray = [];
              var parentArray = [];
              var rmdefaultCities = 'NO';

              var productlistarray = $scope.productlists;
              var selectedProductList = data[0].products;

              var branchlistarray = $scope.bankbranchlist;
              var selectedBranchList = data[0].branches;

              //var citylistarray = $scope.allcitieslist;
              var citylistarray = data.city_list;
              var selectedCityList = data[0].city;

              var statelistarray = $scope.statelists;
              var selectedStateList = data[0].state ? data[0].state :[];

              var parentrmlistarray = data.parent_list;


              selectedProductList.forEach( function(productId) {

                productlistarray.forEach(function (productArrayId,index) {

                  if(productId == productArrayId['id']){

                    productArray.push(productlistarray[index]);
                  }


                })

              });

              selectedBranchList.forEach( function(branchId) {

                branchlistarray.forEach(function (branchArrayId,indexbranch) {

                  if(branchId == branchArrayId['id']){

                    branchArray.push(branchlistarray[indexbranch]);
                  }


                })

              });

              selectedStateList.forEach( function(stateId) {

                statelistarray.forEach(function (stateArrayId,indexstate) {

                  if(stateId == stateArrayId['id']){

                    stateArray.push(statelistarray[indexstate]);
                  }

                })

              });


              selectedCityList.forEach( function(cityId) {

                if(cityId == 0){
                  rmdefaultCities   = 'YES';
                 }

                citylistarray.forEach(function (citylistarrayArrayId,indexcity) {

                  if(cityId == citylistarrayArrayId['id']){

                    cityArray.push(citylistarray[indexcity]);
                  }
                })

              });

              var parent_id = data[0].parent_id ? data[0].parent_id : 0;

              if( parent_id != 0 ){

                parentrmlistarray.forEach(function (parentArrayId,indexparent) {

                  if(parent_id == parentArrayId['id']){
                    parentArray.push(parentrmlistarray[indexparent]);
                  }

                });

              }

              //console.log("hello",cityArray);
              $scope.rmid = data[0].id;
              $scope.rmname = data[0].name;
              $scope.rmemail = data[0].email;
              $scope.rmmobile = data[0].mobile;
              $scope.rmmobile1 = data[0].mobile1;
              $scope.rmtype = data[0].contact_type;
              $scope.rmproduct = productArray;//data[0].products;
              $scope.rmstate = stateArray;
              $scope.rmcity = cityArray;
              $scope.rmstatus = data[0].status;
              $scope.rmdefault = rmdefaultCities;
              $scope.parentrmid = parentArray;
              $scope.bankbranchid = branchArray;

      		})
      		.catch(function(error){
                  $scope.error = {
                      message: error.message
                  };
      		});

          });

        });

		};


    $scope.bankBranchById = function(branchId){

      $scope.productsList().then(function(pdata) {

        $scope.stateList().then(function(sdata) {

          var api = ENV.apiEndpoint + '/bankbranchbyid/' + branchId;

      		return assignService.sourceRawList(api, accessToken)
      		.then(function(data){

              $scope.allcitieslist = data.city_list;

              var productArray = [];
              var stateArray = [];
              var cityArray = [];
              var parentArray = [];
              var rmdefaultCities = 'NO';

              var productlistarray = $scope.productlists;
              var selectedProductList = data[0].products;

              //var citylistarray = $scope.allcitieslist;
              var citylistarray = data.city_list;
              var selectedCityList = data[0].branch_city;

              var statelistarray = $scope.statelists;
              var selectedStateList = data[0].branch_state ? data[0].branch_state :[];

              selectedProductList.forEach( function(productId) {

                productlistarray.forEach(function (productArrayId,index) {

                  if(productId == productArrayId['id']){

                    productArray.push(productlistarray[index]);
                  }


                })

              });

              selectedStateList.forEach( function(stateId) {

                statelistarray.forEach(function (stateArrayId,indexstate) {

                  if(stateId == stateArrayId['id']){

                    stateArray.push(statelistarray[indexstate]);
                  }

                })

              });


              selectedCityList.forEach( function(cityId) {

                if(cityId == 0){
                  rmdefaultCities   = 'YES';
                 }

                citylistarray.forEach(function (citylistarrayArrayId,indexcity) {

                  if(cityId == citylistarrayArrayId['id']){

                    cityArray.push(citylistarray[indexcity]);
                  }
                })

              });



              //console.log("hello",cityArray);
              $scope.branchid = data[0].id;
              $scope.branchname = data[0].branch_name;
              $scope.branchemail = data[0].branch_email;
              $scope.branchmobile = data[0].branch_mobile;
              $scope.branchmobile1 = data[0].branch_mobile1;
              $scope.rmproduct = productArray;//data[0].products;
              $scope.rmstate = stateArray;
              $scope.rmcity = cityArray;
              $scope.branchpin = data[0].branch_pin;
              $scope.branchadd = data[0].branch_add;
              $scope.branchstatus = data[0].status;
              $scope.branchdefault = rmdefaultCities;

      		})
      		.catch(function(error){
                  $scope.error = {
                      message: error.message
                  };
      		});

          });

        });

		};

    $scope.bankAllCities = function(){

    		var api = ENV.apiEndpoint + '/allcities';

    		return assignService.sourceRawList(api, accessToken)
    		.then(function(data){

          var dataset = data;
          $scope.allcitieslist = data;

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};


    $scope.addBankRm = function(){

        var postParams = {
          "rm_name" : $scope.rmname,
          "rm_email" : $scope.rmemail,
          "rm_mobile" : $scope.rmmobile,
          "rm_mobile1" : $scope.rmmobile1,
          "rm_type" : $scope.rmtype,
          "rm_product" : $scope.rmproduct,
          "rm_state" : $scope.rmstate,
          "rm_city" : $scope.rmcity,
          "rm_status" : $scope.rmstatus,
          "rm_default" : $scope.rmdefault,
          "rm_parent" : $scope.parentrmid,
          "rm_branch" : $scope.bankbranchid
        };

        var api = ENV.apiEndpoint + '/add/bankrm';

        return assignService.saveSourceData(api, postParams, accessToken)
        .then(function(data){
          alert('Successfully ADDED....!!');
          //window.location.href = '/bank/rmlist';

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.addBankBranch = function(){

        var postParams = {
          "branch_name" : $scope.branchname,
          "branch_email" : $scope.branchemail,
          "branch_mobile" : $scope.branchmobile,
          "branch_mobile1" : $scope.branchmobile1,
          "branch_product" : $scope.rmproduct,
          "branch_state" : $scope.rmstate,
          "branch_city" : $scope.rmcity,
          "branch_add" : $scope.branchadd,
          "branch_pin" : $scope.branchpin,
          "branch_status" : $scope.branchstatus,
          "branch_default" : $scope.branchdefault
        };

        var api = ENV.apiEndpoint + '/add/bankbranch';

        return assignService.saveSourceData(api, postParams, accessToken)
        .then(function(data){

          alert('Successfully ADDED....!!');

          window.location.href = '/bank/branchlist';

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.editBankRm = function(){

        var postParams = {
          "rm_id" : $scope.rmid,
          "rm_name" : $scope.rmname,
          "rm_email" : $scope.rmemail,
          "rm_mobile" : $scope.rmmobile,
          "rm_mobile1" : $scope.rmmobile1,
          "rm_type" : $scope.rmtype,
          "rm_product" : $scope.rmproduct,
          "rm_state" : $scope.rmstate,
          "rm_city" : $scope.rmcity,
          "rm_status" : $scope.rmstatus,
          "rm_default" : $scope.rmdefault,
          "rm_parent" : $scope.parentrmid,
          "rm_branch" : $scope.bankbranchid
        };

        var api = ENV.apiEndpoint + '/edit/bankrm';

        return assignService.saveSourceData(api, postParams, accessToken)
        .then(function(data){
          alert('Successfully UPDATED....!!');

          window.location.reload();

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.editBankBranch = function(){

      var postParams = {
        "branch_id" : $scope.branchid,
        "branch_name" : $scope.branchname,
        "branch_email" : $scope.branchemail,
        "branch_mobile" : $scope.branchmobile,
        "branch_mobile1" : $scope.branchmobile1,
        "branch_product" : $scope.rmproduct,
        "branch_state" : $scope.rmstate,
        "branch_city" : $scope.rmcity,
        "branch_add" : $scope.branchadd,
        "branch_pin" : $scope.branchpin,
        "branch_status" : $scope.branchstatus,
        "branch_default" : $scope.branchdefault
      };

        var api = ENV.apiEndpoint + '/edit/bankbranch';

        return assignService.saveSourceData(api, postParams, accessToken)
        .then(function(data){
          alert('Successfully UPDATED....!!');

          window.location.reload();

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.bankRmByRoleProduct = function(){

        var postParams = {
          "rm_type" : $scope.rmtype,
          "rm_product" : $scope.rmproduct
        };

        var api = ENV.apiEndpoint + '/newbankrmlist/byroleproduct';

        return assignService.saveSourceData(api, postParams, accessToken)
        .then(function(data){

          $scope.parentrmlist = data.data;
          $scope.bankbranchlist = data.branch_data;

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

    switch(filter){

      case 'source-data':
          $scope.title = "All Source List";
          $scope.callingSourceData();
          break;

    case 'myassign-list':
          $scope.title = "My Assign Calling List";
          var sourceId = $routeParams.id;
          $scope.myAssignList(sourceId);
          break;

    case 'myassign-list-todays':
          $scope.title = "My Todays Assign Calling List";
          $scope.myTodaysAssignList();
          break;

    case 'myassign-source-list':
          $scope.title = "My Assign Calling Source List";
          $scope.myAssignSourceList();
          break;

    case 'assign-list':
          $scope.title = "Assign Calling List";
          $scope.agentsList();
          $scope.unassigndatadropdown();
          break;

    case 'calling-comments':
          $scope.title = "Calling Comments";
          var callId = $routeParams.id;
          $scope.commentsById(callId,"callingcomments");
          $scope.productList();
          break;

    case 'lead-comments':
          $scope.title = "Lead Comments";
          var leadId = $routeParams.id;
          $scope.commentsById(callId,"lead");
          break;

    case 'myassign-folloup-list':
          $scope.title = "Followup Data List";
          //var leadId = $routeParams.id;
          //$scope.commentsById(callId,"lead");
          break;

    case 'bankrmlist':
          $scope.title = "Bank RM List";
          //var leadId = $routeParams.id;
          $scope.productsList();
          break;

    case 'new-bank-rm-list':
          $scope.title = "New Bank RM List";
          //var leadId = $routeParams.id;
          $scope.productsList();
          break;

    case 'bank-rm-add':
          $scope.title = "Bank RM Add";
          //var leadId = $routeParams.id;
          $scope.productsList();
          $scope.stateList();
          break;

    case 'bank-rm-edit':
          $scope.title = "Bank RM Edit";
          var rmId = $routeParams.rmId;
          $scope.bankRmById(rmId);
          break;

          case 'bank-branch-list':
                $scope.title = "Bank Branch List";
                //var leadId = $routeParams.id;
                $scope.productsList();
                break;

          case 'bank-branch-add':
                $scope.title = "Bank Branch Add";
                //var leadId = $routeParams.id;
                $scope.productsList();
                $scope.stateList();
                break;

          case 'bank-branch-edit':
                $scope.title = "Bank Branch Edit";
                var branchId = $routeParams.branchId;
                $scope.bankBranchById(branchId);
                break;

    };

    //$scope.title = "My Assign Calling List";
    $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();


    //$scope.commentsById();

}])

.filter('strip', function(){
    return function(str) {
      var finaldate = str.substring(1, str.length);
      finaldate = Number(finaldate)
      var fdate = new Date(finaldate);
      var pdate = fdate.toString('yyyy-MM-dd HH:mm:ss');
          //finaldate = Date.parse(finaldate);
                        console.log(finaldate,fdate,pdate);
      return pdate ;
    };
  });
