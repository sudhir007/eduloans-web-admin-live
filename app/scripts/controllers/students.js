'use strict';

angular.module('app')
	.controller('studentController', ['$scope', 'studentService', 'assignService', '$cookieStore', '$route', 'menuService', 'NgTableParams', 'applicationService', '$rootScope', '$location', '$routeParams', 'commonService', '$compile', 'ENV', '$sce', '$q', function ($scope, studentService, assignService, $cookieStore, $route, menuService, NgTableParams, applicationService, $rootScope, $location, $routeParams, commonService, $compile, ENV, $sce, $q){
		var accessToken = $cookieStore.get("access_token");
		var partnerName = $cookieStore.get("partner_name");
		var loginId = $cookieStore.get("pId");
        var filter = $route.current.$$route.filter;
		$scope.searchApi = ENV.apiEndpoint.replace("/partner", "") + '/cities/';
		$scope.searchname = ENV.apiEndpoint.replace("/partner", "") + '/autosearch/';
		$scope.error = {};
		$scope.test = [];

		$scope.CurrentDate = new Date();
		$scope.present = new Date().getFullYear();
		$scope.next = new Date().getFullYear() + 1;
		$scope.future = new Date().getFullYear() + 2;

		$scope.getMenu = function(){
			return commonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

		$scope.offers = [
			{id: 1, display_name: "Travel Booking"},
			{id: 2, display_name: "TOEFL Exam"},
			{id: 3, display_name: "Fedex DHL Courier Service"},
			{id: 4, display_name: "TOEFL Exam"},
			{id: 5, display_name: "Forex Booking"},
			{id: 6, display_name: "GRE Exam"}
		];

		$scope.allHearAboutUs = [];
		$scope.subSources = [];
		$scope.getHearAboutUs = function () {
			commonService.getListData(6)
				.then(function (hearAboutUs) {
					let webHearAboutUs = {};
				 //webHearAboutUs.id = 411;
				 //webHearAboutUs.display_name = 'Website';
				 //hearAboutUs.push(webHearAboutUs);
					$scope.allHearAboutUs = hearAboutUs;
				})
				.catch(function (err) {
					//console.log(err);
				})
		}

		$scope.getSubSources = function(){
			$scope.subSources = [];
			if($scope.sourceId){
				var api = ENV.apiEndpoint + '/indianCollegeOrAgentList/' + $scope.sourceId;
				return commonService.unassigndata(api, accessToken)
				.then(function(response){
					$scope.subSources = response.data;
				});
			}
		}

		$scope.getHearAboutUs();

		if(filter == 'create'){
			$scope.commonSubmit = function(){
				if(!$scope.loginId){
					$scope.submitCreateForm('/auth/signup', 'login_create_object')
					.then(function(data){
						$scope.submitCreateForm('/customer/:customer_id', 'personal_detail_create_object')
						.then(function(data){
							$scope.submitCreateForm('/customer/:customer_id', 'registration_type_object')
							.then(function(data){

								var valuemultiple = $scope.templateFields['customers|registration_type'];

								valuemultiple.forEach(function(rtypevalue, rtypeindex){

									if(rtypevalue['id'] == '423'){

										$scope.submitCreateForm('/forex/:customer_id', 'forex_info_object');

									}

									if(rtypevalue['id'] == '422'){

										$scope.submitCreateForm('/lead/:lead_id', 'lead_create_object');

									}

								});

								$scope.submitCreateForm('/lead/:lead_id/opted-universities', 'opted_university_create_object')
								.then(function(data){
									alert("Success");
								})
							.catch(function(err){
								//console.log(err);
							})
						})
					.catch(function(err){
							//console.log(err);
						})
					})
						.catch(function(err){
							//console.log(err);
						})
					})
					.catch(function(err){
						//console.log(err);
					})
				}
				else{
					$scope.submitCreateForm('/customer/:customer_id', 'personal_detail_create_object')
					.then(function(data){
						$scope.submitCreateForm('/customer/:customer_id', 'registration_type_object')
						.then(function(data){

							var valuemultiple = $scope.templateFields['customers|registration_type'];

							valuemultiple.forEach(function(rtypevalue, rtypeindex){

								if(rtypevalue['id'] == '423'){

									$scope.submitCreateForm('/forex/:customer_id', 'forex_info_object');

								}

								if(rtypevalue['id'] == '422'){

									$scope.submitCreateForm('/lead/:lead_id', 'lead_create_object');

								}

							});

							$scope.submitCreateForm('/lead/:lead_id/opted-universities', 'opted_university_create_object')
							.then(function(data){
								alert("Success");
							})
							.catch(function(err){
								//console.log(err);
							})
						})
						.catch(function(err){
							//console.log(err);
						})
					})
					.catch(function(err){
						console.log(err);
					})
				}
			}
			var leadId = $routeParams.lead_id ? $routeParams.lead_id : 0;
			var callingId = $routeParams.calling_id ? $routeParams.calling_id : 0;
			$scope.getOffers= false;
			var visibleFor = 3;
			$scope.templateFields = {};
			$scope.templateFields['leads|loan_type'] = [];
			$scope.templateFields['customers|registration_type'] = [];
			$scope.getTemplate = function(productTypeId){
				return commonService.getTemplate(accessToken, visibleFor, productTypeId)
				.then(function(data){
					$scope.template = data;
					for(let key in $scope.template){
						for(let subKey in $scope.template[key]){
							if(subKey === 'active'){
								continue;
							}
							$scope.template[key][subKey].forEach(function(templateData, templateIndex){
									if(templateData['field_ui_type'] === 'static_multiselect_drop_down'){
										templateData['list_items'].forEach(function(listItems, listIndex){
											$scope.template[key][subKey][templateIndex]['list_items'][listIndex]['label'] = listItems['display_name'];
										})
									}
							});
						}
					}
					if($scope.templateFields['leads_opted_universities|country_id'] !== undefined){
						$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
							if(arrayValue['field_name'] === 'opted_country'){
							   arrayValue['list_items'].forEach(function(listValue, listKey){
								   if(listValue['id'] == $scope.templateFields['leads_opted_universities|country_id'] ){
									$scope.templateFields['leads_opted_universities|country_id'] = listValue['display_name'];
								    }
							    })
						    }

					    })
						$scope.showDependent('leads_opted_universities', 'country_id', 'opted_country', 0);
						if($scope.templateFields['leads_opted_universities|university_id']){
							let c = $scope.templateFields['leads_opted_universities|university_id'];
							$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
								if(arrayValue['field_name'] === 'opted_university'){
									arrayValue['list_items'].forEach(function(listValue, listKey){
										if(listValue['id'] == $scope.templateFields['leads_opted_universities|university_id'] ){
											$scope.templateFields['leads_opted_universities|university_id'] = listValue['display_name'];
											return studentService.getCourses(c)
									         .then(function(data){
										          data.forEach(function(data1,index){
											                if(data1.id == $scope.templateFields['leads_opted_universities|course_id'])
											                    {
												                      $scope.templateFields['leads_opted_universities|course_id'] = data1.display_name;

											                    }
										            })
									            })
										}
									})
								}
							})
							$scope.showCourses($scope.templateFields['leads_opted_universities|university_id'], 0);
						}
					}
					if($scope.templateFields['logins|hear_about_us_id'] !== undefined){
						for(let key in $scope.template){
							for(let subKey in $scope.template[key]){
								if(subKey === 'active'){
									continue;
								}
								$scope.template[key][subKey].forEach(function(templateData, templateIndex){
										if(templateData['field_column_name'] === 'hear_about_us_id'){
											$scope.appendMapping('logins|hear_about_us_id', templateData['list_items']);
										}
								});
							}
						}
					}
				})
				.catch(function(error){
					if(error.error_code === "SESSION_EXPIRED"){
						window.location.href = '/';
					}
				});
			};
			$scope.example2settings = {idProperty: 'id', displayProp: 'id'};

			$scope.getMinLeadData = function(){
				return studentService.getMinLeadData(accessToken, leadId)
				.then(function(leadData){
					$scope.loginId = leadData.login_id;
					$scope.customerId = leadData.customer_id;
					$scope.leadId = leadId;

					//$scope.leadData = leadData;
					$scope.templateFields['logins|email'] = leadData.email;
					$scope.templateFields['logins|mobile_number'] = leadData.mobile_number;
					$scope.templateFields['logins|hear_about_us_id'] = leadData.hear_about_us_id;
					if(leadData.reference.friend_id !== undefined){
						$scope.templateFields['logins|reference.friend_id'] = leadData.reference.friend_id;
						$scope.templateFields['logins|reference.counsellor_id'] = leadData.reference.counsellor_id;
						$scope.templateFields['logins|reference.university'] = leadData.reference.university;
					}

					$scope.templateFields['customers|title_id'] = leadData.title_id;
					$scope.templateFields['customers|first_name'] = leadData.first_name;
					$scope.templateFields['customers|middle_name'] = leadData.middle_name;
					$scope.templateFields['customers|last_name'] = leadData.last_name;

					$scope.templateFields['leads|requested_loan_amount'] = leadData.requested_loan_amount;
					$scope.templateFields['leads|gre_score'] = leadData.gre_score;
					$scope.templateFields['leads|parent_it_return'] = leadData.parent_it_return;
					$scope.templateFields['leads|mortgage_value'] = leadData.mortgage_value;
					if(leadData.loan_type){
						$scope.templateFields['leads|loan_type'] = [];
						leadData.loan_type.forEach(function(loanTypeValue){
							$scope.templateFields['leads|loan_type'].push({id: loanTypeValue})
						});
					}

					if(leadData.registration_type){
						$scope.templateFields['customers|registration_type'] = [];
						leadData.registration_type.forEach(function(registrationTypeValue){
							$scope.templateFields['customers|registration_type'].push({id: registrationTypeValue})
						});
					}
					$scope.templateFields['leads_opted_universities|country_id'] = leadData.country_id;
					$scope.templateFields['leads_opted_universities|course_id'] = leadData.course_id;
					$scope.templateFields['leads_opted_universities|university_id'] = leadData.university_id;
					//console.log($scope.templateFields);

					$scope.getTemplate(1);
					$scope.submitLogin = true;
				})
				.catch(function(error){
					if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
						$cookieStore.remove("access_token", "");
						$cookieStore.remove("login_id", "");
						$cookieStore.remove("customer_id", "");
						window.location.href = '/';
					}
				});
			};

			$scope.selectregistrationType = function(){

				var valuemultiple = $scope.templateFields['customers|registration_type'];

				$("#lead_create_object77777").css("display","none");
				$("#forex_info_object77777").css("display","none");

				valuemultiple.forEach(function(rtypevalue, rtypeindex){

					if(rtypevalue['id'] == '422'){
						$("#lead_create_object77777").css("display","block");

					}

					if(rtypevalue['id'] == '423'){
						$("#forex_info_object77777").css("display","block");

					}

				});

			};

			$scope.getCallingData = function(){
				return studentService.getCallingData(accessToken, callingId)
				.then(function(leadData){
					//$scope.leadData = leadData;
					$scope.templateFields['logins|email'] = leadData.email;
					$scope.templateFields['logins|mobile_number'] = leadData.mobile;
					var splitName = leadData.name.split(' ');
					$scope.templateFields['customers|first_name'] = splitName[0];
					if(splitName[2] !== undefined)
					{
						$scope.templateFields['customers|middle_name'] = splitName[1];
						$scope.templateFields['customers|last_name'] = splitName[2];
					}
					else if(splitName[1] !== undefined)
					{
						$scope.templateFields['customers|last_name'] = splitName[1];
					}
					$scope.templateFields['leads|gre_score'] = leadData.gre_score;
					//$scope.checkUser("email");
					$scope.getTemplate(1);
					$scope.submitLogin = true;
					//$scope.checkUser("mobile_number");
				})
				.catch(function(error){
					if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
						$cookieStore.remove("access_token", "");
						$cookieStore.remove("login_id", "");
						$cookieStore.remove("customer_id", "");
						window.location.href = '/';
					}
				});
			};

			$scope.submitCreateForm = function(groupUrl, groupObject){
				var def = $q.defer();
				var postedFields = {};
				var mandatoryCheck = true;
				if ($scope.templateFields['customers|current_city_id'] && $scope.templateFields['customers|current_city_id']['description']) {
					$scope.templateFields['customers|current_city_id'] = $scope.templateFields['customers|current_city_id']['description']['id'];
				}
				if ($scope.templateFields['logins|reference.friend_id'] && ($scope.templateFields['logins|reference.friend_id']['description'] || $scope.templateFields['logins|reference.friend_id']['originalObject'])) {
					$scope.templateFields['logins|reference.friend_id'] = $scope.templateFields['logins|reference.friend_id']['description'] !== undefined ? $scope.templateFields['logins|reference.friend_id']['description']['id'] : $scope.templateFields['logins|reference.friend_id']['originalObject']['description']['id'];
				}
				for(let key in $scope.template){
					for(let subKey in $scope.template[key]){
						if(subKey === 'active'){
							continue;
						}
						$scope.template[key][subKey].forEach(function(templateData){
							if(templateData['group_object'] === groupObject){
								postedFields[templateData['field_column_name']] = '';
								//console.log(templateData['mandatory'], $scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']])
								if(templateData['mandatory']){
									if(!$scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']] || $scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']] === 'Please Select' || !$scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']].length){
										mandatoryCheck = false;
									}
								}
								if(templateData['list_items']){
									templateData['list_items'].forEach(function(listItems){
										if(listItems.mapping){
											var mappingItems = listItems.mapping;
											//console.log(mappingItems, templateData['field_column_name'], "Mapping");
											mappingItems.forEach(function(mappedItemList){
												//console.log(mappedItemList['field_column_name'], "Mpped Field Column");
												if(mappedItemList['field_column_name']){
													postedFields[mappedItemList['field_column_name']] = '';
												}
											})
										}
									})
								}
							}
						});
					}
				}
				if(!mandatoryCheck){
					alert("Please fill all mandatory fields");
					//return;
					def.reject(false);
					return def.promise;
				}

				//console.log($scope.templateFields, postedFields, groupObject);
				//return;

				switch(groupObject){
					case 'login_create_object':
						if(!$scope.submitLogin){
							alert("Please fill all details proper.");
							//return;
							def.reject(false);
							return def.promise;
						}
						if(callingId){
							postedFields.calling_id = callingId;
						}
						postedFields.password = "test";
						if(leadId){
							groupUrl = '/auth/login/' + $scope.loginId;
						}
						break;
					case 'personal_detail_create_object':
						if($scope.loginId === undefined || !$scope.loginId){
							alert("Please create login first");
							//return;
							def.reject(false);
							return def.promise;
						}
						groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
						postedFields.login_id = $scope.loginId;
						break;
					case 'lead_create_object':
						var customerId = $scope.customerId ? $scope.customerId : $cookieStore.get("customer_id");
						if(!customerId){
							alert("Please fill student basic detail first");
							//return;
							def.reject(false);
							return def.promise;
						}
						groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
						postedFields.customer_id = customerId;
						break;
					case 'registration_type_object':
							var customerId = $scope.customerId ? $scope.customerId : $cookieStore.get("customer_id");
							if(!customerId){
								alert("Please fill student basic detail first");
								//return;
								def.reject(false);
								return def.promise;
							}
							groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
							postedFields.login_id = $scope.loginId;
							break;
					case 'forex_info_object':
					case 'forex_detail_object':
								var customerId = $scope.customerId ? $scope.customerId : $cookieStore.get("customer_id");
								if(!customerId){
									alert("Please fill student basic detail first");
									//return;
									def.reject(false);
									return def.promise;
								}
								groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
								postedFields.customer_id = customerId;
								break;
					case 'opted_university_create_object':
						if($scope.leadId === undefined || !$scope.leadId){
							alert("Please fill required loan amount and loan type");
							//return;
							def.reject(false);
							return def.promise;
						}
						postedFields.lead_id = $scope.leadId;
						groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
						break;
				}

				for(let key in $scope.templateFields){
					if($scope.templateFields[key] === 'Please Select'){
						continue;
					}

					var originalKey = key;
					var splitKey = key.split('|');
					key = splitKey[1];
					if(postedFields[key] !== undefined){
						postedFields[key] = $scope.templateFields[originalKey];
					}
				}

				for(let key2 in postedFields){
					if(key2.indexOf(".") !== -1){
						var newKey = key2.split(".");
						if(postedFields[newKey[0]] === undefined){
							postedFields[newKey[0]] = {};
						}
						postedFields[newKey[0]][newKey[1]] =postedFields[key2];
						delete postedFields[key2];
					}
				}

				if(groupObject === 'lead_create_object'){
					var loanTypeIndex = 0;
					var loanTypeCount = postedFields.loan_type.length;
					$scope.templateFields['leads|loan_type'] = [];
					for(let i = 0; i < loanTypeCount; i++){
						let loanTypeJson = {};
						loanTypeJson["id"] = postedFields.loan_type[loanTypeIndex]['id'];
						$scope.templateFields['leads|loan_type'].push(loanTypeJson);
						postedFields.loan_type.push(postedFields.loan_type[loanTypeIndex]['id']);
						postedFields.loan_type.splice(loanTypeIndex, 1);
					}
				}

				if(groupObject === 'registration_type_object'){
					var registrationTypeIndex = 0;
					var registrationTypeCount = postedFields.registration_type.length;
					$scope.templateFields['customers|registration_type'] = [];
					for(let i = 0; i < registrationTypeCount; i++){
						let registrationTypeJson = {};
						registrationTypeJson["id"] = postedFields.registration_type[registrationTypeIndex]['id'];
						$scope.templateFields['customers|registration_type'].push(registrationTypeJson);
						postedFields.registration_type.push(postedFields.registration_type[registrationTypeIndex]['id']);
						postedFields.registration_type.splice(registrationTypeIndex, 1);
					}
				}

				if((groupObject === 'login_create_object' && !leadId) || groupObject === 'opted_university_create_object'){
					if(groupObject === 'opted_university_create_object'){
						let postedUniversity = [];
						postedUniversity.push(postedFields);
						var postedFields = [];
						postedFields = postedUniversity;
						let countryFound = false;
						let universityFound = false;
						let courseFound = false;

						$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
							if(arrayValue['field_name'] === 'opted_country'){
								arrayValue['list_items'].forEach(function(listValue, listKey){
									if(listValue['name'] === postedFields[0].country_id && listValue['mapping'] !== undefined){
										postedFields[0].country_id = listValue['id'];
										countryFound = true;
									}
								})
							}
							if(arrayValue['field_name'] === 'opted_university'){
								arrayValue['list_items'].forEach(function(listValue, listKey){
									if(listValue['display_name'] === postedFields[0].university_id ){
										postedFields[0].university_id = listValue['id'];
										universityFound = true;
									}
								})
							}
							if(arrayValue['field_name']=== 'opted_course'){
								arrayValue['list_items'].forEach(function(listValue, listKey){
									if(listValue['display_name'] == postedFields[0].course_id){
										postedFields[0].course_id = listValue.id;
										courseFound = true;
									}
								})
							}
						})
						if(!countryFound || !universityFound || !courseFound){
							alert("Please select country / university / course from the dropdown list. If not present, select Others and enter value.");
							def.reject(false);
							return def.promise;
						}
					}
					return studentService.add(accessToken, groupUrl, postedFields)
					.then(function(response){
						switch(groupObject){
							case 'login_create_object':
								$scope.loginId = response.login_id;
								$scope.customerId = response.customer_id;
								$cookieStore.put("customer_id", $scope.customerId);
								$scope.leadId = response.lead_id;
								break;
							case 'opted_university_create_object':
								$scope.getOffers = true;
								break;
						}
						//alert("Success");
						def.resolve(true);
						return def.promise;
					})
					.catch(function(err){
						/*if(err.message.message.indexOf("Duplicate") !== -1){
							$scope.error = {
								field: "email",
								message: "Email or mobile already registered"
							}
						}*/
						alert(err.message);
						def.reject(err.message);
						return def.promise;
					});
					//return def.promise;
				}
				else{
					return studentService.update(accessToken, groupUrl, postedFields)
					.then(function(response){
						//alert("Success");
						def.resolve(true);
						return def.promise;
					})
					.catch(function(err){
						alert(err.message);
						def.reject(err.message);
						return def.promise;
					});
				}
			};

			$scope.gotoOffer = function(leadId){
				window.location.href = "/offers/" + leadId;
			}

			$scope.courses = [];

			$scope.loanTypeSetting = {
				//searchField: 'name',
				//enableSearch: true,
				idProperty: 'id'
			};

			$scope.submitLogin = false;

			$scope.checkUser = function(columnName){
				$scope.error = {};

				if(columnName === 'email' || columnName === 'mobile_number'){
					$scope.submitLogin = false;
					var email = $scope.templateFields['logins|email'] ? $scope.templateFields['logins|email'] : '';
					var mobileNumber = $scope.templateFields['logins|mobile_number'] ? $scope.templateFields['logins|mobile_number'] : '';
					if(columnName === 'email'){
						var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
						if(!email_regex.test(email)){
							$scope.error = {
								field: "email",
								message: $sce.trustAsHtml("Not a valid email")
							}
							return;
						}
					}
					if(columnName === 'mobile_number'){
						var mobile_regex = /^[0]?[6789]\d{9}$/i;
						if(!mobile_regex.test(mobileNumber)){
							$scope.error = {
								field: "mobile_number",
								message: $sce.trustAsHtml("Not a valid mobile")
							}
							return;
						}
					}

					var postParams =  {
						'mobile_number': mobileNumber,
						'email': email,
						"first_name": $scope.templateFields['customers|first_name']
					};

					studentService.existingUser(postParams)
					.then(function(data){
						$scope.submitLogin = true;
						//window.location.href='/assign/data';
					})
					.catch(function(error){
						$scope.error = {
							field: "email",
							message: $sce.trustAsHtml(error.message)
						}
					});
				}
			};

			/*$scope.showUniversities = function(countryId, cloneCount){
				$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
					if(arrayValue['field_name'] === 'opted_country'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['id'] === countryId && listValue['mapping'] !== undefined){
								$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
									if(arrayValue1['field_name'] === 'opted_university'){
										if(!cloneCount){
											$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else{
											$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			};*/

			$scope.showUniversities = function(countryId, cloneCount){
				$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
					//console.log(arrayValue);
					if(arrayValue['field_name'] === 'opted_country'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['name'] === countryId && listValue['mapping'] !== undefined){
								$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
									if(arrayValue1['field_name'] === 'opted_university'){
										if(!cloneCount){
											$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else{
											$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			};

			/*$scope.showCourses = function(universityId, cloneCount){
				if(universityId == 831){
					document.getElementById("leads_opted_universities|other_details.university").style.display = "block";
				}
				else{
					document.getElementById("leads_opted_universities|other_details.university").style.display = "none";
				}
				return studentService.getCourses(universityId)
				.then(function(data){
					var courses = data;
					$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
						if(arrayValue['field_name'] === 'opted_university'){
							arrayValue['list_items'].forEach(function(listValue, listKey){
								if(angular.isArray(listValue)){
									listValue = listValue[0];
								}
								if(listValue['id'] == universityId){
									$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
										if(arrayValue1['field_name'] === 'opted_course'){
											if(!cloneCount){
												$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
											}
											else{
												$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
											}
										}
									});
								}
							})
						}
					});
				});
			};*/

			$scope.showCourses = function(universityId, cloneCount){
				if(isNaN(universityId)){
					if(universityId == 'Others'){
						document.getElementById("leads_opted_universities|other_details.university").style.display = "block";
					}
					else{
						document.getElementById("leads_opted_universities|other_details.university").style.display = "none";
					}
					$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
						var a;
						if(arrayValue['field_name'] === 'opted_university'){
							arrayValue['list_items'].forEach(function(listValue, listKey){
								if(angular.isArray(listValue)){
									listValue = listValue[0];
								}
								if(listValue['display_name'] == universityId){
									a =listValue['id'];
									return studentService.getCourses(a)
									.then(function(data){
										var courses = data;
									$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
										if(arrayValue1['field_name'] === 'opted_course'){
											if(!cloneCount){
												$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
											}
											else{
													$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
											}
										}
									});
								});
								}
							})
						}
					});
				}

			};

			$scope.showDependent = function(tableName, columnName, fieldName, cloneCount){
				var countryId = '';
				var universityId = '';
				var courseId = '';
				switch(fieldName){
					case 'opted_country':
						if(!cloneCount){
							countryId = $scope.templateFields[tableName + '|' + columnName];
						}
						else{
							countryId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
						}
						$scope.showUniversities(countryId, cloneCount);
						break;
					case 'opted_university':
						if(!cloneCount){
							universityId = $scope.templateFields[tableName + '|' + columnName];
						}
						else{
							universityId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
						}
						if(universityId){
							$scope.showCourses(universityId, cloneCount);
						}
						break;
					case 'opted_course':
						courseId = $scope.templateFields[tableName + '|' + columnName];
						if(courseId == 710 || courseId == 'Other'){
							document.getElementById("leads_opted_universities|other_details.course").style.display = "block";
						}
						else{
							document.getElementById("leads_opted_universities|other_details.course").style.display = "none";
						}
						break;
				}
			}

			var counts = {};

			$scope.cloneOptedUniversities = function(clone, fieldsList){
				//console.log(fieldsList, clone);
				if(!counts[clone]){
					counts[clone] = 1;
				}

				var cloneCount = counts[clone];
				var delid = clone + cloneCount;

				//var html = '<input type="button" value="Delete" id="rm' + cloneCount + '" style="float: right; margin-top: 10px;" onclick="deleteValue(\'' + clone + cloneCount + '\',\'rm' + cloneCount + '\')">';
				var html = '<div id="' + clone + cloneCount + '" style="box-shadow: 0px 2px 5px 2px rgb(167, 167, 167); padding: 5px;"><div class="row form-group formRow">';
				html += '<div class="col-md-3" ng-repeat="(index, list) in fieldsList" ng-if="fieldsList[index].field_name==\'opted_country\' || fieldsList[index].field_name==\'opted_university\' || fieldsList[index].field_name==\'opted_course\'">';
				html += '<label for="{{fieldsList[index].field_name}}" class="control-label">{{fieldsList[index].field_display_name}}</label>';
				html += '<select class="form-control" name="{{fieldsList[index].field_name}}" ng-init="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] = templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] || \'Please Select\'" ng-model="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\']"  ng-change="showDependent(fieldsList[index].field_table_name, fieldsList[index].field_column_name, fieldsList[index].field_name,' + cloneCount + ')">';
				html += '<option>Please Select</option>';
				html += '<option ng-if="fieldsList[index].field_name!=\'opted_country\'" ng-repeat="option in fieldsList[index].list_items[' + cloneCount + ']" value="{{option.id}}">{{option.display_name}}</option>';
				html += '<option ng-if="fieldsList[index].field_name==\'opted_country\' && option.mapping" ng-repeat="option in fieldsList[index].list_items"  value="{{option.id}}">{{option.display_name}}</option>';
				html += '</select>';
				html += '</div></div><div class="submit_button"><input type="submit" name="submit" value="Submit"></div></div>';

				$scope.fieldsList = fieldsList;
				var cloneDiv = document.getElementById(clone);
				var parentElement = angular.element( html );
				parentElement.insertAfter(cloneDiv);
				$compile(parentElement)($scope);

				cloneCount++;
				counts[clone] = cloneCount;
			};

			if(leadId){
				$scope.getMinLeadData();
			}
			else if(callingId){
				$scope.getCallingData();
			}
			else{
				$scope.getTemplate(1);
			}
		}
		else if(filter == 'my-leads'){
			$scope.getMyLeads = function(queryParams){

				return studentService.getList(queryParams, accessToken)
				.then(function(response){
					return commonService.getListData(5)
					.then(function(listData){
						response.forEach(function(leadInfo, index){
							var displayLoanType = "";
							listData.forEach(function(loanType){
								if(leadInfo['loan_type'] && leadInfo['loan_type'].indexOf(loanType['id']) !== -1){
									displayLoanType += loanType['display_name'] + ", ";
								}
							});
							response[index]['loan_type'] = displayLoanType;
						});
						$scope.tableParams = new NgTableParams({}, {dataset: response});
						//console.log("hello",response);

					});
				})
				.catch(function(err){
					alert("No lead found");
					$scope.tableParams = new NgTableParams({}, {dataset: []});
				})
			}

			let queryParams = {
				type : 'originator'
			}

			$scope.getMyLeads(queryParams);
		}
		else if(filter == 'assigned-me'){
			var type = $routeParams.type ? $routeParams.type : '';
			$scope.type = $routeParams.type;
			$scope.getMyAssignedLeads = function(queryParams){
				var predefinedState = $location.search().lead_state ? $location.search().lead_state : '';
				var partnerId = $location.search().pid ? $location.search().pid : '';
				if(partnerId){
					queryParams = {
						type : 'counselor',
						lead_state: 'all',
						pid: partnerId
					}
				}

				return studentService.getList(queryParams, accessToken)
				.then(function(response){
					var todayFollowup = [];
					response.forEach(function(value, key){
						if(value.followup_date){
							var currentDate = new Date();
							var followupDate = new Date(value['followup_date']);
							if(currentDate > followupDate){
								todayFollowup.push(value);
							}
						}
						//response[key]['followup_date'] = new Date(value['followup_date']);
					})
					//$scope.followlisttoday = response;
					$scope.followlisttoday = new NgTableParams({}, {dataset: todayFollowup});
					return commonService.getListData(5)
					.then(function(listData){
						response.forEach(function(leadInfo, index){
							var displayLoanType = "";
							listData.forEach(function(loanType){
								if(leadInfo['loan_type'] && leadInfo['loan_type'].indexOf(loanType['id']) !== -1){
									displayLoanType += loanType['display_name'] + ", ";
								}
							});
							response[index]['loan_type'] = displayLoanType;
							if($scope.type == 'offer'){
								response[index]['offer_name'] = '';
								$scope.offers.forEach(function(offer){
									if(offer['id'] == leadInfo['offer_id']){
										response[index]['offer_name'] = offer['display_name'];
									}
								})
							}
							//response[index]['followup_date'] = new Date(leadInfo['followup_date']);
						});
						$scope.tableParams = new NgTableParams({filter:{current_lead_state: predefinedState}}, {dataset: response});
					});
				})
				.catch(function(err){
					alert("No lead found");
					$scope.tableParams = new NgTableParams({}, {dataset: []});
				})
			}
			let queryParams = {
				type : type
			}
			$scope.getMyAssignedLeads(queryParams);
		}

		$scope.applySource = function(){
			let type = 'originator';
			if(filter == 'assigned-me'){
				type = $routeParams.type ? $routeParams.type : '';
			}
			let queryParams = {
				type : type,
				source: $scope.sourceId
			}
			if(filter == 'assigned-me'){
				$scope.getMyAssignedLeads(queryParams);
			}
			if(filter == 'my-leads'){
				$scope.getMyLeads(queryParams);
			}
			$scope.getSubSources();
		}

		$scope.applySubSource = function(value){
			let reference = {};
			switch($scope.sourceId){
				case '9':
					reference = {
						key : 'news_paper_id',
						value : $scope.subSourceId
					}
					break;
				case '10':
					reference = {
						key : 'social_media_id',
						value : $scope.subSourceId
					}
					break;
				case '11':
					reference = {
						key : 'seminar_id',
						value : $scope.subSourceId
					}
					break;
				case '13':
					reference = {
						key : 'cold_calling_id',
						value : $scope.subSourceId
					}
					break;
				case '312':
					reference = {
						key : 'counsellor_id',
						value : $scope.subSourceId
					}
					break;
				case '313':
					reference = {
						key : 'university_id',
						value : $scope.subSourceId
					}
					break;
				/*case '314':
				reference = {
					news_paper_id: $scope.subSourceId
				}*/
				case '407':
					reference = {
						key : 'webinar_id',
						value : $scope.subSourceId
					}
					break;
				case '408':
					reference = {
						key : 'walkin_id',
						value : $scope.subSourceId
					}
					break;
				default:
					return;
			}
			let type = 'originator';
			if(filter == 'assigned-me'){
				type = $routeParams.type ? $routeParams.type : '';
			}
			let queryParams = {
				type : type,
				source: $scope.sourceId,
				reference: reference
			}
			if(filter == 'assigned-me'){
				$scope.getMyAssignedLeads(queryParams);
			}
			if(filter == 'my-leads'){
				$scope.getMyLeads(queryParams);
			}
		}

		$scope.appendMapping = function(divId, listOptions){
			if(divId == 'logins|hear_about_us_id'){
				listOptions.forEach(function(listData){
					if(listData['id'] == $scope.templateFields[divId]){
						//$("#mapping-column").remove();
						if(listData['mapping']!==undefined){
							var newScope = $scope.$new();
							newScope.mappingFieldsList = listData['mapping'];
							var html = '<div class="col-md-3" ng-repeat="(index, list) in mappingFieldsList"'
										+' id="mapping-column">'
										+' <label for="{{mappingFieldsList[index].field_name}}" class="control-label">{{mappingFieldsList[index].field_display_name}}</label>'
										+'<angucomplete-alt id = "ex2" placeholder = "Enter name or email or mobile" pause = "300" '
										+' selected-object="templateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]" selected-object-data="id" '
										+ 'remote-url={{searchname}} remote-url-data-field="data" title-field="display_name"  minlength="1" input-class="form-control" match-class="highlight"'
										+ 'ng-if="mappingFieldsList[index].field_ui_type == \'auto_search\'">'
										+ '</angucomplete-alt>'
										+' <input type="text" class="form-control txtBoxBS" ng-if="mappingFieldsList[index].field_ui_type == \'text\'"'
										+' ng-model="templateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]">'
										+' <select class="form-control" name="{{mappingFieldsList[index].field_name}}" id="{{mappingFieldsList[index].field_name}}"'
										+' ng-init="templateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]=\'Please Select\'"'
										+' ng-model="templateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]"'
										+' ng-if="mappingFieldsList[index].field_ui_type == \'static_drop_down\'">'
										+' <option>Please Select</option>'
										+' <option ng-repeat="option in mappingFieldsList[index].list_items" value="{{option.id}}">{{option.display_name}}</option>'
										+' </select></div>';
							var element = document.getElementById(divId);
							if(newScope.mappingFieldsList){
								var contentTr = angular.element(html);
								contentTr.insertAfter(element);
								//angular.element(element).prepend($compile(html)($scope));
								//console.log($scope.fieldsList);
								$compile(contentTr)(newScope);
							}
						}
					}
					else{
						$("#mapping-column").remove();
					}
				});
			}
		};

		$scope.agentsList = function(){

			var postParams =  {};

    		var api = ENV.apiEndpoint + '/agentslist';

    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){
    			$scope.agentslist = data;

          //console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.selectEntity = function (appId, checkedvalue) {
			if(checkedvalue){
				$scope.editData(appId, "MULTIPLE");
				$scope.test.push(appId.lead_id);
			} else {
				for(var i = 0; i < $scope.test.length; i++){
					if($scope.test[i]==appId.lead_id){
						$scope.test.splice(i,1);
						appId.isEditable = false;
					}
				}
			}

		};

		//$scope.isEditable = false;
		$scope.editData = function (appId, selection) {
			appId.isEditable = true;
			if(selection == 'MULTIPLE'){
				if($scope.test.indexOf(appId.lead_id) === -1){
					$scope.test.push(appId.lead_id);
				}
			}
			else{
				$scope.test = [];
				$scope.test.push(appId.lead_id);
			}
		};

		$scope.cancelEditData = function (appId) {
					appId.isEditable = false;
					//$scope.isEditable = true;
					for(var i = 0; i < $scope.test.length; i++){
						if($scope.test[i]==appId.lead_id){
							$scope.test.splice(i,1);
							appId.isEditable = false;
						}
					}
			};
		$scope.setActive = function(menuItem) {
			$scope.activeMenu = menuItem ;
		}

		$scope.saveData = function (appValue) {
			$scope.tableParams.data.forEach(function (params) {
				params.isEditable = false;
				$scope.checkedvalue = false;
			});
			$scope.followlisttoday.data.forEach(function (params) {
				params.isEditable = false;
				$scope.checkedvalue = false;
			});
			var intakeYear = appValue.intake ? appValue.intake.substr(appValue.intake.length - 4) : '';
			var intakeMonth = appValue.intake ? appValue.intake.substr(0, appValue.intake.length - 5) : '';
			switch(intakeMonth){
				case 'January - Febraury':
					intakeMonth = 372;
					break;
				case 'June - July':
					intakeMonth = 373;
					break;
				case 'August - September':
					intakeMonth = 374;
					break;
			}
			$scope.test.forEach(function(index, key){
				var postParams = {
					"id": index,
					"facilitator_id": appValue.facilitator_id,
					"intake": appValue.intake,
					"mobile": appValue.mobile_number,
					"customer_id": appValue.customer_id,
					"email": appValue.email
				};

				if(intakeYear && intakeMonth){
					var optedUniApiEndPoint = '/lead/' + index + '/opted-universities';
					var optedUniParamsArray = [];
					var optedUniParamsObj = {
						intake_year: intakeYear,
						intake_month_id: intakeMonth,
						lead_id: index
					}
					optedUniParamsArray.push(optedUniParamsObj);

					studentService.add(accessToken, optedUniApiEndPoint, optedUniParamsArray)
				}

				var api = ENV.apiEndpoint + '/lead/facupdate';
				return studentService.updateCurrentStatus(api, postParams, accessToken)
				.then(function (data) {
					//console.log(data);
					let update = false;
					$scope.tableParams.data.forEach(function(leadDetail, tableKey){
						if(leadDetail.lead_id == index){
							$scope.agentslist.forEach(function(value){
								if(value.id == appValue.facilitator_id){
									$scope.tableParams.data[tableKey]['partner_name'] = value.first_name + ' ' + value.last_name;
									update = true;
								}
							})
						}
					});
					if(!update){
						$scope.followlisttoday.data.forEach(function(leadDetail, tableKey){
							if(leadDetail.lead_id == index){
								$scope.agentslist.forEach(function(value){
									if(value.id == appValue.facilitator_id){
										$scope.followlisttoday.data[tableKey]['partner_name'] = value.first_name + ' ' + value.last_name;
										update = true;
									}
								})
							}
						});
					}
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
			})
		};

		$scope.saveDataCalling = function(appValue){

			var postParams = {
				"id": appValue.id,
				"facilitator_id": appValue.assigned_to,
				"mobile": appValue.mobile_number,
				"email": appValue.email
			};

    		var api = ENV.apiEndpoint + '/lead/facupdatecalling';

    		return studentService.updateCurrentStatus(api, postParams, accessToken)
    		.then(function(data){
    			//$scope.agentslist = data;
					alert("Success...!!");
          //console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.editDataCalling = function (emp) {
      emp.isEditable = true;
        //$scope.isEditable = true;
    };

		$scope.cancelEditDataCAlling = function (emp) {
      emp.isEditable = false;
        //$scope.isEditable = true;
    };

		$scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		$scope.getMenu();
		$scope.agentsList();
    }]);
