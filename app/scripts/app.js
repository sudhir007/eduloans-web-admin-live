'use strict';

/**
 * @ngdoc overview
 * @name eduloansWebNewApp
 * @description
 * # eduloansWebNewApp
 *
 * Main module of the application.
 */
/*
angular
  .module('eduloansWebNewApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
  */

var app = angular.module('app', [
    'ngRoute',
    'ngResource',
    'config',
    'ngCookies',
    'ngTable',
    'oc.lazyLoad',
    'angularjs-dropdown-multiselect',
    '720kb.datepicker',
    'angucomplete-alt',
    'ngTableToCsv'
    //'ngCsv'
]);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/auth/login.html',
            controller: 'authController'
        })
        .when('/dashboard', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/index.html',
            filter: 'dashboard'
        })
        .when('/dashboard/intake/:year', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/intakedashboard.html',
            filter: 'intake-dashboard'
        })
        .when('/dashboard/imerialdata/:intakeYear', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/imperialDashboard.html',
            filter: 'imperialData'
        })
        .when('/dashboard/imperial/filter', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/imperialFilter.html',
            filter: 'imperial-student-filter'
        })
        .when('/dashboard/banks', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/banks.html',
            filter: 'banks'
        })
        .when('/dashboard/banks/intake/:year', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/banks_intake_wise.html',
            filter: 'banks-intake'
        })
        .when('/dashboard/studentbanks', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/studentbanks.html',
            filter: 'studentbanks'
        })
        .when('/dashboard/telesoftlogin', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/telesoftPage.html',
            filter: 'studentbanks'
        })
        .when('/dashboard/allstudent', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/smm/allStudentList.html',
            filter: 'smmAllLeads'
        })
        .when('/applications/collateral', {
            templateUrl: 'views/main.html',
            controller: 'applicationController',
            pageName: 'views/applications/index.html',
            filter: 'collateral'
        })
        .when('/applications/non-collateral', {
            templateUrl: 'views/main.html',
            controller: 'applicationController',
            pageName: 'views/applications/index.html',
            filter: 'non-collateral'
        })
        .when('/applications/bob', {
            templateUrl: 'views/main.html',
            controller: 'applicationController',
            pageName: 'views/applications/index.html',
            filter: 'bob'
        })
        .when('/applications/sbi', {
            templateUrl: 'views/main.html',
            controller: 'applicationController',
            pageName: 'views/applications/index.html',
            filter: 'sbi'
        })
        .when('/applications/us-cosignor', {
            templateUrl: 'views/main.html',
            controller: 'applicationController',
            pageName: 'views/applications/index.html',
            filter: 'us-cosignor'
        })
        .when('/applications/other-banks', {
            templateUrl: 'views/main.html',
            controller: 'applicationController',
            pageName: 'views/applications/index.html',
            filter: 'other-banks'
        })
        .when('/students/all', {
            templateUrl: 'views/main.html',
            controller: 'applicationController',
            pageName: 'views/applications/index.html',
            filter: 'all'
        })
        .when('/students/search', {
            templateUrl: 'views/main.html',
            controller: 'searchController',
            pageName: 'views/students/search.html',
        })
        .when('/students/partner-register', {
            templateUrl: 'views/main.html',
            controller: 'registerController',
            pageName: 'views/students/register.html',
        })
        .when('/student/:id/product/:pid', {
            templateUrl: 'views/main.html',
            controller: 'leadController',
            pageName: 'views/lead/index.html'
        })
        .when('/students/assigned-me', {
            templateUrl: 'views/main.html',
            controller: 'studentController',
            pageName: 'views/students/list.html',
            filter: 'assigned-me'
        })
        .when('/students/my/followuptoday', {
            templateUrl: 'views/main.html',
            controller: 'studentController',
            pageName: 'views/students/listtoday.html',
            filter: 'assigned-me'
        })
        .when('/students/my', {
            templateUrl: 'views/main.html',
            controller: 'studentController',
            pageName: 'views/students/list.html',
            filter: 'my-leads'
        })
        .when('/students/assigned-me/:type', {
            templateUrl: 'views/main.html',
            controller: 'studentController',
            pageName: 'views/students/list.html',
            filter: 'assigned-me'
        })
        .when('/students/offer', {
            templateUrl: 'views/main.html',
            controller: 'listController',
            pageName: 'views/list/offers.html'
        })
        .when('/profile/change-password', {
            templateUrl: 'views/main.html',
            controller: 'profileController',
            pageName: 'views/profile/change_password.html'
        })
        .when('/management/user/agents', {
            templateUrl: 'views/main.html',
            controller: 'userManagementController',
            pageName: 'views/management/user/agents.html'
        })
        .when('/student/create', {
            templateUrl: 'views/main.html',
            controller: 'studentController',
            pageName: 'views/students/create.html',
            filter: 'create'
        })
        .when('/student/create/calling/:calling_id', {
            templateUrl: 'views/main.html',
            controller: 'studentController',
            pageName: 'views/students/create.html',
            filter: 'create'
        })
        .when('/student/create/:lead_id', {
            templateUrl: 'views/main.html',
            controller: 'studentController',
            pageName: 'views/students/create.html',
            filter: 'create'
        })
        .when('/upload', {
            templateUrl: 'views/main.html',
            controller: 'bulkuploadController',
            pageName: 'views/bulkupload/bulkupload.html',
        })
        .when('/upload/imperialdata', {
            templateUrl: 'views/main.html',
            controller: 'bulkuploadController',
            pageName: 'views/bulkupload/imperialdataupload.html',
        })
        .when('/upload/bankresponse', {
            templateUrl: 'views/main.html',
            controller: 'bulkuploadController',
            pageName: 'views/bulkupload/uploadbankresponse.html',
        })
        .when('/assign/data', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/assigndata.html',
            filter: 'assign-list'
        })
        .when('/source/data', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/sourcedata.html',
            filter: 'source-data'
        })
        .when('/assign/mycalldata/:id/:leadstate', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/myassign.html',
            filter: 'myassign-list'
        })
        .when('/assign/mycalldata/todays', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/myassigntodays.html',
            filter: 'myassign-list-todays'
        })
        .when('/assign/mycalldatabysource', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/myassignbysource.html',
            filter: 'myassign-source-list'
        })
        .when('/assign/folloupListByDateRange', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/followupdatebyrange.html',
            filter: 'myassign-folloup-list'
        })
        .when('/masterdata/country/active', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/country.html',
            filter: 'country-active'
        })
        .when('/masterdata/country/in-active', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/country.html',
            filter: 'country-inactive'
        })
        .when('/masterdata/country/pending', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/country.html',
            filter: 'country-pending'
        })
        .when('/masterdata/country/rejected', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/country.html',
            filter: 'country-rejected'
        })
        .when('/masterdata/universities', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/university.html',
            filter: 'university-list'
        })
        .when('/masterdata/universities/bycountry', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/universitybycountry.html',
            filter: 'university-bycountry-list'
        })
        .when('/masterdata/courses', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/course.html',
            filter: 'course-list'
        })
        .when('/masterdata/courses/byuniversity', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/coursebyuniversity.html',
            filter: 'course-byuniversity-list'
        })
        .when('/active/university/mapping/:university_id', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/activeUniversityMapping.html',
            filter: 'active-university-mapping'
        })
        .when('/active/course/mapping/:course_id', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/activeCourseMapping.html',
            filter: 'active-course-mapping'
        })
        .when('/masterdata/statemaster', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/statemaster.html',
            filter: 'statemaster-list'
        })
        .when('/masterdata/citymaster', {
            templateUrl: 'views/main.html',
            controller: 'masterManagementController',
            pageName: 'views/masters/citymaster.html',
            filter: 'citymaster-list'
        })
        .when('/reports/callingdata', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/reports.html',
        })
        .when('/reports/callingdata/days', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/dayreports.html',
            filter: 'days-report-list'
        })
        .when('/reports/callingdata/sourcereport', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/sourcereports.html',
            filter: 'source-report-list'
        })
        .when('/reports/loginstat', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/loginstat.html',
            filter: 'login-stat'
        })
        .when('/reports/business-stats', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/business_stat.html',
            filter: 'business-stat'
        })
        .when('/reports/filter', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/filterreports.html',
            filter: 'filter-report'
        })
        .when('/reports/callingdata/overall', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/overallreport.html',
            filter: 'over-all-report'
        })
        .when('/offers/:lead_id', {
            templateUrl: 'views/main.html',
            controller: 'offerController',
            pageName: 'views/offers/index.html'
        })
        .when('/students/chat-with-student/:login_id/:entity_name', {
            templateUrl: 'views/main.html',
            controller: 'tokboxController',
            pageName: 'views/chatAndVideo/mainPage.html',
            filter: 'msg-video-calling'
        })
        .when('/comments/calling/:id', {
            templateUrl: 'views/main.html',
            controller: 'commentsController',
            pageName: 'views/comments/callingcomments.html',
            filter: 'calling-comments'
        })
        .when('/comments/lead/:id/:loanappid/:forexappid', {
            templateUrl: 'views/main.html',
            controller: 'commentsController',
            pageName: 'views/comments/leadcomments.html',
            filter: 'lead-app-comments'
        })
        .when('/banks/leads', {
            templateUrl: 'views/main.html',
            controller: 'bankManagementController',
            pageName: 'views/banksmanagement/myassign.html',
            filter: 'banks-all-leads'
        })
        .when('/educationloan/:id/product/:pid' ,{
            templateUrl: 'views/students/educationloan.html',
            controller: 'sbiController',
        })
        .when('/assign/rm' ,{
            templateUrl: 'views/main.html',
            pageName:'views/lead/partner.html',
           controller:'partnerController'
        })
        .when('/assign/rm/:id',{
            templateUrl: 'views/main.html',
            pageName:'views/lead/partner.html',
            controller:'partnerController'
        })
         .when('/assign/rm/:id/total',{
            templateUrl: 'views/main.html',
            pageName:'views/modal/list.html',
            controller: 'popUpController',
            filter: 'total'
        })

        .when('/assign/rm/:id/potential',{
            templateUrl: 'views/main.html',
            pageName:'views/modal/potential.html',
            controller: 'popUpController',
            filter: 'potential'
        })

        .when('/assign/rm/:id/disbursed',{
            templateUrl: 'views/main.html',
            pageName:'views/modal/disbursed.html',
            controller: 'popUpController',
            filter: 'disbursed'
        })

        .when('/assign/rm/:id/declined',{
            templateUrl: 'views/main.html',
            pageName:'views/modal/declined.html',
            controller: 'popUpController',
            filter: 'declined'
        })
        .when('/upload/status-report/bob', {
            templateUrl: 'views/main.html',
            pageName: 'views/lead/status_report_bob.html',
            controller: 'partnerController',
        })
        .when('/view1', {
            templateUrl: 'views/view1.html',
            controller: 'MyCtrl1'
        })
        .when('/counselor/alllist', {
            templateUrl: 'views/main.html',
            controller: 'counselorController',
            pageName: 'views/counsellor/allCounsellorList.html',
            filter: 'all-counselor-list'
        })
        .when('/counselor/list', {
            templateUrl: 'views/main.html',
            controller: 'counselorController',
            pageName: 'views/counsellor/currentCounsellorList.html',
            filter: 'my-counselor-list'
        })
        .when('/counselor/callinglist/:leadstate', {
            templateUrl: 'views/main.html',
            controller: 'counselorController',
            pageName: 'views/counsellor/CallingData.html',
            filter: 'myassign-list'
        })
        .when('/counselor/create', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/create.html',
            controller: 'counselorController',
            filter: 'create'
        })
        .when('/counselor/create/:id', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/create.html',
            controller: 'counselorController',
            filter: 'createCounselor'
        })
        .when('/counselor/edit/:id', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/edit.html',
            controller: 'counselorController',
            filter: 'edit'
        })
        .when('/counselor/:cid/team', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/team_add.html',
            controller: 'counselorTeamController',
            filter: 'add'
        })
        .when('/counselor/:cid/team/list', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/team_list.html',
            controller: 'counselorTeamController',
            filter: 'list'
        })
        .when('/counselor/:cid/team/edit/:tid', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/team_edit.html',
            controller: 'counselorTeamController',
            filter: 'edit'
        })
        .when('/counselor/upload', {
            templateUrl: 'views/main.html',
            controller: 'counselorController',
            pageName: 'views/counsellor/uploadData.html',
            filter: 'upload'
        })
        .when('/counselor/comments/:id', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/cunselorCallingComments.html',
            controller: 'counselorController',
            filter: 'counselor-comments'
        })
        .when('/counselor/callingcomments/:id', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/cunselorCallingComments.html',
            controller: 'counselorController',
            filter: 'counselor-calling-comments'
        })
        .when('/counselor/callingdata/add', {
            templateUrl: 'views/main.html',
            pageName: 'views/counsellor/createcallingdata.html',
            controller: 'counselorController',
            filter: 'counselor-callingdata-add'
        })
        .when('/bank/rmlist', {
            templateUrl: 'views/main.html',
            pageName: 'views/bulkupload/bankrmlist.html',
            controller: 'assignController',
            filter: 'bankrmlist'
        })
        .when('/university/alllist', {
            templateUrl: 'views/main.html',
            controller: 'universityController',
            pageName: 'views/university/allUniversityList.html',
            filter: 'all-university-list'
        })
        .when('/university/list', {
            templateUrl: 'views/main.html',
            controller: 'universityController',
            pageName: 'views/university/currentUniversityList.html',
            filter: 'my-university-list'
        })
        .when('/university/callinglist/:leadstate', {
            templateUrl: 'views/main.html',
            controller: 'universityController',
            pageName: 'views/university/CallingData.html',
            filter: 'myassign-list'
        })
        .when('/university/create', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/create.html',
            controller: 'universityController',
            filter: 'create'
        })
        .when('/university/create/:id', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/create.html',
            controller: 'universityController',
            filter: 'callingUniversity'
        })
        .when('/university/edit/:id', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/edit.html',
            controller: 'universityController',
            filter: 'edit'
        })
        .when('/university/:id/member', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/member.html',
            controller: 'universityController',
            filter: 'member'
        })
        .when('/university/:id/member/create', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/member_create.html',
            controller: 'universityController',
            filter: 'member-create'
        })
        .when('/university/:id/member/:member_id/edit', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/member_edit.html',
            controller: 'universityController',
            filter: 'member-edit'
        })
        .when('/university/upload', {
            templateUrl: 'views/main.html',
            controller: 'universityController',
            pageName: 'views/university/uploadData.html',
            filter: 'upload'
        })
        .when('/university/comments/:id', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/universityCallingComments.html',
            controller: 'universityController',
            filter: 'university-comments'
        })
        .when('/university/callingcomments/:id', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/universityCallingComments.html',
            controller: 'universityController',
            filter: 'university-calling-comments'
        })
        .when('/university/callingdata/add', {
            templateUrl: 'views/main.html',
            pageName: 'views/university/createcallingdata.html',
            controller: 'universityController',
            filter: 'university-callingdata-add'
        })
        .when('/reports/referred', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/referredreport.html',
            filter: 'referred-report'
        })
        .when('/finance/disbursedlist/:year', {
            templateUrl: 'views/main.html',
            controller: 'financeController',
            pageName: 'views/finance/finance.html',
            filter: 'disbursed-list'
        })
        .when('/confirmfinance/disbursedlist/:year', {
            templateUrl: 'views/main.html',
            controller: 'financeController',
            pageName: 'views/finance/confirmfinance.html',
            filter: 'confirm-disbursed-list'
        })
        .when('/finance/bankDisbursalInfo', {
            templateUrl: 'views/main.html',
            controller: 'financeController',
            pageName: 'views/finance/bankDisbursalInfo.html',
            filter: 'disbursed-info'
        })
        .when('/finance/calculateamount', {
            templateUrl: 'views/main.html',
            controller: 'financeController',
            pageName: 'views/finance/calculateamount.html',
            filter: 'calculate-amount'
        })
        .when('/finance/agentincome/:year', {
            templateUrl: 'views/main.html',
            controller: 'financeController',
            pageName: 'views/finance/agentIncome.html',
            filter: 'agent-income'
        })
        .when('/finance/disbursed/agentincome', {
            templateUrl: 'views/main.html',
            controller: 'financeController',
            pageName: 'views/finance/agentDisbursedIncome.html',
            filter: 'agent-disbursed-income'
        })
        .when('/finance/studentlist/byoffer/:year', {
            templateUrl: 'views/main.html',
            controller: 'financeController',
            pageName: 'views/finance/offerstudentlist.html',
            filter: 'offer-student-list'
        })
        .when('/offer/vendorlist', {
            templateUrl: 'views/main.html',
            controller: 'financeController',
            pageName: 'views/offers/offervendorlist.html',
            filter: 'offer-vendor-list'
        })
        .when('/bank/rmadd', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/bank_rm_add.html',
            filter: 'bank-rm-add'
        })
        .when('/bank/rmedit/:rmId', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/bank_rm_edit.html',
            filter: 'bank-rm-edit'
        })
        .when('/bank/newbankrmlist', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/newbankrmlist.html',
            filter: 'new-bank-rm-list'
        })
        .when('/bank/branchadd', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/bank_branch_add.html',
            filter: 'bank-branch-add'
        })
        .when('/bank/branchedit/:branchId', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/bank_branch_edit.html',
            filter: 'bank-branch-edit'
        })
        .when('/bank/branchlist', {
            templateUrl: 'views/main.html',
            controller: 'assignController',
            pageName: 'views/bulkupload/bank_branch_list.html',
            filter: 'bank-branch-list'
        })
        .when('/forex/rmadd', {
            templateUrl: 'views/main.html',
            controller: 'forexController',
            pageName: 'views/forex/forex_rm_add.html',
            filter: 'forex-rm-add'
        })
        .when('/forex/rmedit/:rmId', {
            templateUrl: 'views/main.html',
            controller: 'forexController',
            pageName: 'views/forex/forex_rm_edit.html',
            filter: 'forex-rm-edit'
        })
        .when('/forex/forexrmlist', {
            templateUrl: 'views/main.html',
            controller: 'forexController',
            pageName: 'views/forex/forexrmlist.html',
            filter: 'forex-rm-list'
        })
        .when('/dashboard/basicprofilereport', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/dashboard/basicprofilereport.html',
            filter: 'basic-profile-report'
        })
        .when('/leads/byfolloupdate', {
            templateUrl: 'views/main.html',
            controller: 'dashboardController',
            pageName: 'views/lead/leadsbyfollowupdate.html',
            filter: 'leads-by-folloupdate'
        })
        .when('/approval-list', {
            templateUrl: 'views/main.html',
            controller: 'approvalController',
            pageName: 'views/university/approval.html',
            filter: 'approval-list'
        })
        .when('/approval-page/university/:id', {
            templateUrl: 'views/main.html',
            controller: 'approvalController',
            pageName: 'views/university/approvalUniversity.html',
            filter: 'approval-page-university'
        })
        .when('/approval-page/course/:id', {
            templateUrl: 'views/main.html',
            controller: 'approvalController',
            pageName: 'views/university/approvalCourse.html',
            filter: 'approval-page-course'
        })
        .when('/approved-page/university/:id', {
            templateUrl: 'views/main.html',
            controller: 'approvalController',
            pageName: 'views/university/approvedUniversityMapping.html',
            filter: 'approved-page-university'
        })
        .when('/approved-page/course/:id/:aid', {
            templateUrl: 'views/main.html',
            controller: 'approvalController',
            pageName: 'views/university/approvedCourseMapping.html',
            filter: 'approved-page-course'
        })
        .when('/category/course', {
            templateUrl: 'views/main.html',
            controller: 'approvalController',
            pageName: 'views/university/courseCategoryMapping.html',
            filter: 'category-course'
        })
        .when('/counsellor/lead-transfer', {
            templateUrl: 'views/main.html',
            controller: 'counselorController',
            pageName: 'views/counsellor/leadtransfer.html',
            filter: 'leads-transfer-counsellor'
        })
        .when('/community/list', {
            templateUrl: 'views/main.html',
            controller: 'communityController',
            pageName: 'views/community/communitylist.html',
            filter: 'community-list'
        })
        .when('/community/posts/list/:community_id', {
            templateUrl: 'views/main.html',
            controller: 'communityController',
            pageName: 'views/community/communitypostlist.html',
            filter: 'community-post-list'
        })
        .when('/community/posts/action/:post_id', {
            templateUrl: 'views/main.html',
            controller: 'communityController',
            pageName: 'views/community/communitypostactionlist.html',
            filter: 'community-post-action-list'
        })
        .when('/community/student/list/:community_id', {
            templateUrl: 'views/main.html',
            controller: 'communityController',
            pageName: 'views/community/communitystudentlist.html',
            filter: 'community-student-list'
        })
        .when('/marketing/email/upload', {
            templateUrl: 'views/main.html',
            controller: 'bulkuploadController',
            pageName: 'views/bulkupload/marketingemailupload.html',
            filter: 'marketing-email-upload'
        })
        .when('/marketing/email/send', {
            templateUrl: 'views/main.html',
            controller: 'bulkuploadController',
            pageName: 'views/bulkupload/sendmarketingemail.html',
            filter: 'marketing-email-send'
        })
        .when('/forex/my', {
            templateUrl: 'views/main.html',
            controller: 'forexController',
            pageName: 'views/forex/all_current_student.html',
            filter: 'forex-my-student'
        })
        .when('/forex/my/followuptoday', {
            templateUrl: 'views/main.html',
            controller: 'forexController',
            pageName: 'views/forex/all_current_student.html',
            filter: 'forex-followuptoday-student'
        })
        .when('/forex/assigned-me', {
            templateUrl: 'views/main.html',
            controller: 'forexController',
            pageName: 'views/forex/all_student.html',
            filter: 'forex-assigned-me-student'
        })
        .when('/forex/assigned-me/:type', {
            templateUrl: 'views/main.html',
            controller: 'forexController',
            pageName: 'views/forex/all_student.html',
            filter: 'forex-assigned-me-student'
        })
        .when('/lead/chats', {
            templateUrl: 'views/main.html',
            controller: 'tokboxController',
            pageName: 'views/chatAndVideo/leadBulkChat.html',
            filter: 'lead-chat-student'
        })
        .when('/calling/chats', {
            templateUrl: 'views/main.html',
            controller: 'tokboxController',
            pageName: 'views/chatAndVideo/callingBulkChat.html',
            filter: 'calling-chat-student'
        })
        .when('/lead/logs', {
            templateUrl: 'views/main.html',
            controller: 'reportController',
            pageName: 'views/report/leadlogs.html',
            filter: 'lead-logs'
        })
        .otherwise({redirectTo : 'view1'});

    $locationProvider
        .html5Mode(true);
});
