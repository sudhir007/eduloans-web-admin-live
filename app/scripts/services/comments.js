'use strict';

angular.module('app')
.service('commentsService', function ($http, $q, ENV) {
    return {
        login : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        updateCommets : function(api, postParams, accessToken, applicationsList) {
            var def = $q.defer();
            var productTypes = postParams.product_type;
            var promises = [];
            productTypes.forEach(function(value){
                var postData = {};
                for(let key in postParams){
                    if(key == 'product_type'){
                        continue;
                    }
                    postData[key] = postParams[key];
                }
                if(value.product_id != undefined){
                    postData['product_type'] = value.product_id;
                    postData['application_id'] = value.id;
                }
                else{
                    postData['product_type'] = value;
                }
                //console.log(postData, "PostData");
                $http.post(api, postData, {
                    headers: {
                        'source': 4,
                        'access-token': accessToken
                    }
                })
                .then(function(data) {
                    def.resolve(data.data);
                },
                function(error) {
                    def.reject(error.data);
                });
                promises.push(def.promise);
            })
            return $q.all(promises);
        },
        updateForexCommets : function(api, postParams, accessToken, applicationsList) {
            var def = $q.defer();
            var productTypes = postParams.product_type;
            var promises = [];
            productTypes.forEach(function(value){
                var postData = {};
                for(let key in postParams){
                    if(key == 'product_type'){
                        continue;
                    }
                    postData[key] = postParams[key];
                }
                if(value.forex_id != undefined){
                    postData['product_type'] = value.forex_id;
                    postData['application_id'] = value.id;
                }
                else{
                    postData['product_type'] = value;
                }
                //console.log(postData, "PostData");
                $http.post(api, postData, {
                    headers: {
                        'source': 4,
                        'access-token': accessToken
                    }
                })
                .then(function(data) {
                    def.resolve(data.data);
                },
                function(error) {
                    def.reject(error.data);
                });
                promises.push(def.promise);
            })
            return $q.all(promises);
        },
        productsList : function(api, accessToken) {
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              //console.log("hellouuuuuuuuuu",JSON.parse(data.data[0].comments));
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        commentsById : function(api, accessToken) {
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              //console.log("hellouuuuuuuuuu",JSON.parse(data.data[0].comments));
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getBasicDetailCompleteness : function(accessToken, leadId, productId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lead/' + leadId + '/product/' + productId + '/basic-detail-completeness';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'Access-Token': accessToken
                },
                params: {
                    'visible_for': 2
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getDataByPost : function(apiEndPoint, accessToken, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
      };
    });
