'use strict';

angular.module('app')
.service('bankManagementService', function ($http, $q, ENV) {
    return {
        login : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        assignData : function(api, postParams, accessToken) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        unassigndata : function(api, postParams, accessToken) {
          //console.log(accessToken);
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              //console.log(data.data);
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        agentList : function(api, postParams, accessToken) {
          console.log(accessToken);
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              console.log(data.data);
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        assignList : function(api, accessToken) {
          console.log(accessToken);
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              console.log(data.data);
              //console.log("hellouuuuuuuuuu",JSON.parse(data.data[0].comments));
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        updateCallingLeadState : function(api, postParams, accessToken) {
          console.log(accessToken);
            var def = $q.defer(accessToken);
            $http.put(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              console.log(data.data);
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        updateCommets : function(api, postParams, accessToken) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        sourceRawList : function(api, accessToken) {
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              //console.log(data.data);
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        saveSourceData : function(api, postParams, accessToken) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        generateBOBApplicationPDF : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lead/' + leadId + '/product/2/bob/pdf';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        generateBOBFormPDF : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lead/' + leadId + '/product/2/bob/pdf/135';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
