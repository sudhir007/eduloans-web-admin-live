'use strict';

angular.module('app')
.service('bulkuploadService', function ($http, $q) {
    return {
        fileUpload : function(api, file, sourceId, sourceType, collOrAgentId, accessToken) {
          var fd = new FormData();
               fd.append('userfile', file);
               fd.append('source_id', sourceId);
               fd.append('data_type', sourceType);
               fd.append('coll_or_agent_id', collOrAgentId);

            var def = $q.defer();
            $http.post(api, fd, {
              transformRequest: angular.identity,
                headers: {
                    'source': 15,
                    'access-token': accessToken,
                    'Content-Type': undefined
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        fileImperialUpload : function(api, file, branchId, accessToken) {
          var fd = new FormData();
               fd.append('userfile', file);
               fd.append('branch_id', branchId);

            var def = $q.defer();
            $http.post(api, fd, {
              transformRequest: angular.identity,
                headers: {
                    'source': 15,
                    'access-token': accessToken,
                    'Content-Type': undefined
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        counselorFileUpload : function(api, file, sourceName, AgentId, accessToken) {
          var fd = new FormData();
               fd.append('userfile', file);
               fd.append('source_name', sourceName);
               fd.append('assigned_to', AgentId);

            var def = $q.defer();
            $http.post(api, fd, {
              transformRequest: angular.identity,
                headers: {
                    'source': 15,
                    'access-token': accessToken,
                    'Content-Type': undefined
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        fileUploadBankResponse : function(api, file, bankName, accessToken) {
          var fd = new FormData();
               fd.append('userfile', file);
               fd.append('bank_name', bankName);

            var def = $q.defer();
            $http.post(api, fd, {
              transformRequest: angular.identity,
                headers: {
                    'source': 15,
                    'access-token': accessToken,
                    'Content-Type': undefined
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        marketingEmailFileUpload : function(api, file, sourceName, accessToken) {
          var fd = new FormData();
               fd.append('userfile', file);
               fd.append('source_name', sourceName);

            var def = $q.defer();
            $http.post(api, fd, {
              transformRequest: angular.identity,
                headers: {
                    'source': 15,
                    'access-token': accessToken,
                    'Content-Type': undefined
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        logout : function(api, postParams, accessToken) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
