'use strict';

angular.module('app')
.service('offerService', function ($http, $q, ENV) {
    return {
        userOffer : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/offer/' + leadId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        applyProduct : function(postParams, accessToken) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/apply/product';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        applyForexProduct : function(postParams, accessToken) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/apply/forexproduct';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
