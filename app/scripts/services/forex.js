'use strict';

angular.module('app')
.service('forexService', function ($http, $q, ENV) {
    return {
        getData : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getList : function(queryParams, accessToken) {
            var api = ENV.apiEndpoint + "/forex/lead/list";
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'access-token': accessToken,
                    'source': 4
                },
                params: queryParams
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get courses');
            });
            return def.promise;
        }
    };
});
