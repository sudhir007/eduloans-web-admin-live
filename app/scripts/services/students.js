'use strict';

angular.module('app')
.service('studentService', function ($http, $q, ENV) {
    return {
        getData : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getAppliedProducts : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lead/' + leadId + '/product';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getLeadData : function(accessToken, leadId, productId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lead/' + leadId + '/product/' + productId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        update : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint.replace("/partner", "");
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.put(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        add : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint.replace("/partner", "");
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        postData : function(accessToken, api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getCourses : function(universityId) {
            var api = ENV.apiEndpoint.replace("/partner", "");
            api = api + "/course";
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4
                },
                params: {
                    'university_id': universityId
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get courses');
            });
            return def.promise;
        },
        existingUser : function(postParams) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/userid-check';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        updateCurrentStatus : function(api, postParams, accessToken) {

            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getList : function(queryParams, accessToken) {
            var api = ENV.apiEndpoint + "/lead/list";
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'access-token': accessToken,
                    'source': 4
                },
                params: queryParams
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get courses');
            });
            return def.promise;
        },
        getMinLeadData : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lead/' + leadId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'access-token': accessToken,
                    'source': 4
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get courses');
            });
            return def.promise;
        },
        getCallingData : function(accessToken, callingId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/callingdata/' + callingId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'access-token': accessToken,
                    'source': 4
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get courses');
            });
            return def.promise;
        },
        applyOffer : function(api, postParams, accessToken) {

            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
