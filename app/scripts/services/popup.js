'use strict';

angular.module('app')
.service('popUpService', function ($http, $q, ENV){
    return{
        leadList : function(api, accessToken, queryParams) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'access-token': accessToken,
                    'source': 4
                },
                params: queryParams
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get list');
            });
            return def.promise;
        }
    }

});