'use strict';

angular.module('app')
.service('listService', function ($http, $q, ENV){
    return{
        offersList : function(api, accessToken, queryParams) {
            var api = ENV.apiEndpoint + "/lead/offers";
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'access-token': accessToken,
                    'source': 4
                },
                params: queryParams
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get list');
            });
            return def.promise;
        }
    }

});
