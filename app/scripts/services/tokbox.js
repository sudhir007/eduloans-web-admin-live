'use strict';

angular.module('app')
.service('tokboxService', function ($http, $q) {
    return {
        getData : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getDataDirect : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        postData : function(accessToken, api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
    };
});
