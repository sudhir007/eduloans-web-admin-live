'use strict';

angular.module('app')
.service('approvalService', function ($http, $q, ENV) {
    return {
        updateApproval : function(approvalId, postParams, accessToken) {
            var api = ENV.apiEndpoint + '/university/approved/' + approvalId;
            var def = $q.defer(accessToken);
            $http.put(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getApprovals : function(accessToken) {
            var api = ENV.apiEndpoint + '/university/approved';
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getdatabypost : function(apiEndPoint, postParams, accessToken) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                  'source': 4,
                  'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getdatabyget : function(apiEndPoint, accessToken) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.get(api, {
              headers: {
                  'source': 4,
                  'access-token': accessToken
              }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
