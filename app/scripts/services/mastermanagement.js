'use strict';

angular.module('app')
.service('masterManagementService', function ($http, $q) {

  return {
    masterData : function(api, accessToken) {
      //console.log(accessToken);
        var def = $q.defer(accessToken);
        $http.get(api, {
            headers: {
                'source': 15,
                'access-token': accessToken
            }
        })
        .then(function(data) {
            def.resolve(data.data);
        },
        function(error) {
            def.reject(error.data);
        });
        return def.promise;
    },
    masterDataAdd : function(api, postParams, accessToken) {
        var def = $q.defer();
        $http.post(api, postParams, {
            headers: {
                'source': 15,
                'access-token': accessToken
            }
        })
        .then(function(data) {
            def.resolve(data.data);
        },
        function(error) {
            def.reject(error.data);
        });
        return def.promise;
    },
    masterDataUpdate : function(api, postParams, accessToken) {
        var def = $q.defer();
        $http.put(api, postParams, {
            headers: {
                'source': 15,
                'access-token': accessToken
            }
        })
        .then(function(data) {
            def.resolve(data.data);
        },
        function(error) {
            def.reject(error.data);
        });
        return def.promise;
    }
  };

});
