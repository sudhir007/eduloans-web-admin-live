'use strict';

angular.module('app')
.service('reportService', function ($http, $q) {
    return {
        getData : function(api, accessToken, params) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            },
                params
            )
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        reportsData : function(api, postParams, accessToken) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        overallreports : function(api, postParams, accessToken) {
          //console.log(accessToken);
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              console.log('hello',data.data);
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
        ,
        productsList : function(api, accessToken) {
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              console.log("hellouuuuuuuuuu",data.data);
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
