'use strict';

angular.module('app')
.service('commonService', function ($http, $q, ENV) {
    return {
        getMenu : function(accessToken) {
            var api = ENV.apiEndpoint + '/menu';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getTemplate : function(accessToken, visibleFor, productTypeId, leadId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/template';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                },
                params: {
                    'product_type_id': productTypeId,
                    'lead_id' : leadId,
                    'visible_for': visibleFor,
                    'r_type' : 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getListData : function(listId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lists/' + listId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        getLoginData : function(accessToken, loginId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/customer/' + loginId + '/login';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        getAllDataSources : function(accessToken) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/allsource/4';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        unassigndata : function(api, accessToken) {
            //console.log(accessToken);
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        uploadStatusReport : function(api, file, accessToken, partnerId) {
            var fd = new FormData();
            fd.append('userfile', file);
            fd.append('partner_id', partnerId);

            var def = $q.defer();
            $http.post(api, fd, {
                transformRequest: angular.identity,
                headers: {
                    'source': 4,
                    'access-token': accessToken,
                    'Content-Type': undefined
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },upload : function(accessToken, apiEndpoint, postData) {
              var endPoint = ENV.apiEndpoint;
              var api = endPoint + apiEndpoint;
              var def = $q.defer();
              $http({
                  method: 'POST',
                  url: api,
                  headers: {
                      'Content-Type': undefined,
                      'source': 4,
                      'Access-Token': accessToken
                  },
                  data: postData,
                  transformRequest: function (data, headersGetter) {
                      var formData = new FormData();
                      angular.forEach(data, function (value, key) {
                          formData.append(key, value);
                      });

                      var headers = headersGetter();
                      delete headers['Content-Type'];

                      return formData;
                  }
              })
              .then(function (data) {
                  def.resolve(data.data.data);
              })
              .catch(function (data, status) {
                  def.reject(data);
              });
              return def.promise;
          },
          getListData : function(listId) {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + 'lists/' + listId, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        add : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getPartnerData : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getPartnerListData : function(accessToken, partnerId) {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/list/' + partnerId, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        updatePartnerData : function(accessToken, apiEndPoint, putParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.put(api, putParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getdata : function(apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
