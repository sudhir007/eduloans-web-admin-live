'use strict';

angular.module('app')
.service('searchService', function ($http, $q, ENV) {
    return {
        authenticateUser : function(postParams, accessToken) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/leads/search';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        sendMailData : function(postParams, accessToken) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/partner/sendmail';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getRoles : function(accessToken, leadId, productId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/partner/get/roles';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getListData : function(listId) {
            // console.log(ENV.apiEndpoint,"------",);
            
            var def = $q.defer();
            $http.get(ENV.apiEndpoint.replace("/partner","") + '/lists/' + listId, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        add : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint.replace("/partner","");
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
